import React, {useEffect, useState} from 'react';

export default function ToDoList({token}) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/todolist", {
            method: 'GET',
            headers: {
                "Accept": "application/json",
                "Authorization": `Basic ${btoa(token.username + ':' + token.password)}`
            }
        })
            .then(res => res.json())
            .then(
                (json) => {
                    // if(status % 100 === 2) {
                        setIsLoaded(true);
                        setItems(json);
                    // } else {
                    //   setIsLoaded(true);
                    //   setError({message: "Status is " + status})
                    // }
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Загрузка...</div>;
    } else {
        return (<>
                <h2>ToDoList</h2>
                <ul>
                    {items.map(item => (
                        <li key={item.id}>
                            {item.title} {item.done}
                        </li>
                    ))}
                </ul>
            </>
        );
    }
}
