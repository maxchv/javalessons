package org.itstep.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import static org.itstep.utils.Logger.log;


public class Server {

    public static void main(String[] args) throws IOException {
        log("Start program");
        ServerSocket serverSocket = new ServerSocket(10_000, 100);
        log("Listen to " + serverSocket.getInetAddress() + ":10000");
        out: while (true) {
            Socket remoteClient = serverSocket.accept();
            log("Connected to: " + remoteClient.getRemoteSocketAddress());
            try (InputStream in = remoteClient.getInputStream();
                 InputStreamReader rdr = new InputStreamReader(in);
                 BufferedReader bufferedReader = new BufferedReader(rdr)) {
                String line;
                try {
                    while ((line = bufferedReader.readLine()) != null) {
                        log(">>> " + line);
                        if ("exit".equals(line)) {
                            break out;
                        }
                    }
                } catch (SocketException e) {
                    log("Client " + remoteClient.getRemoteSocketAddress()
                            + " " + e.getMessage());
                }
            }
        }
        serverSocket.close();
        log("End program");
    }
}
