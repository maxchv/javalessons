package org.itstep.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

import static org.itstep.utils.Logger.log;

public class Client {
    public static void main(String[] args) throws IOException {
        log("Start program");
        Socket client = new Socket();
        SocketAddress serverAddress =
                new InetSocketAddress("6.tcp.eu.ngrok.io", 16975);
        client.connect(serverAddress);
        Scanner scanner = new Scanner(System.in);
        try (OutputStream out = client.getOutputStream();
             PrintStream printStream = new PrintStream(out, true)) {
            String line;
            do {
                System.out.print(">>> ");
                line = scanner.nextLine();
                printStream.println(line);
            } while (!"exit".equals(line));
        }
        client.close();
        log("End program");
    }
}
