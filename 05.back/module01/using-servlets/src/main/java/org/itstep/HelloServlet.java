package org.itstep;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class HelloServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        System.out.println("Init HelloServlet");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Request
        String queryString = req.getQueryString();
        String method = req.getMethod();
        String remoteUser = req.getRemoteUser();
        String contextPath = req.getContextPath();
        String contentType = req.getContentType();
        int contentLength = req.getContentLength();
        String requestURI = req.getRequestURI();

        StringBuilder parametersBuilder = new StringBuilder();
        Enumeration<String> parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String parameterName = parameterNames.nextElement();
            String parameterValue = req.getParameter(parameterName);
            parametersBuilder
                    .append("<li>")
                    .append(parameterName).append(": ").append(parameterValue)
                    .append("</li>");
        }

        StringBuilder headersBuilder = new StringBuilder();
        Enumeration<String> headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = req.getHeader(headerName);
            headersBuilder
                    .append("<li>")
                    .append(headerName).append(": ").append(headerValue)
                    .append("</li>");
        }
        String serverName = req.getServerName();
        int serverPort = req.getServerPort();
        String scheme = req.getScheme();
        String protocol = req.getProtocol();
        StringBuffer requestURL = req.getRequestURL();

        String servletPath = req.getServletPath();

        PrintWriter writer = resp.getWriter();
        writer.println("<h2>Request info</h2>");
        writer.println("<h3>Test query String:</h3>");
        writer.println("<p><a href='?a=10&b=20'>a=10&b=20</a></p>");
        writer.println("<h4>GET Request</h4>");
        writer.println("<form method='GET'>");
        writer.println("<label>a = <input name='a'/></label>");
        writer.println("<input type='submit'/>");
        writer.println("</form>");
        writer.println("<h4>POST Request</h4>");
        writer.println("<form method='POST'>");
        writer.println("<label>b = <input name='b'/></label>");
        writer.println("<input type='submit'/>");
        writer.println("</form>");
        writer.println("<h3>General info:</h3>");
        writer.println("<p><strong>Context Path:</strong> " + contextPath + "</p>");
        writer.println("<p><strong>RequestURI:</strong> " + requestURI + "</p>");
        writer.println("<p><strong>RequestURL:</strong> " + requestURL + "</p>");
        writer.println("<p><strong>Servlet Path:</strong> " + servletPath + "</p>");
        writer.println("<p><strong>Scheme:</strong> " + scheme + "</p>");
        writer.println("<p><strong>Protocol:</strong> " + protocol + "</p>");
        writer.println("<p><strong>Server Name:</strong> " + serverName + "</p>");
        writer.println("<p><strong>Server Port:</strong> " + serverPort + "</p>");
        writer.println("<p><strong>Method:</strong> " + method + "</p>");
        writer.println("<p><strong>Content-Type:</strong> " + contentType + "</p>");
        writer.println("<p><strong>Content-Length:</strong> " + contentLength + "</p>");
        writer.println("<p><strong>Remote User:</strong> " + remoteUser + "</p>");
        writer.println("<p><strong>Query String:</strong> " + queryString + "</p>");
        writer.println("<h3>Headers: </h3>");
        writer.println("<ul>" + headersBuilder + "</ul>");
        writer.println("<h3>Parameters: </h3>");
        writer.println("<ul>" + parametersBuilder + "</ul>");
    }

    @Override
    public void destroy() {
        System.out.println("Destroy HelloServlet");
    }
}
