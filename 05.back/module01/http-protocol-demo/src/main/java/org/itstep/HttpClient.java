package org.itstep;


import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class HttpClient {
    private static final String CRLF = "\r\n";

    public static void main(String[] args) {
        String host = "127.0.0.1";
        int port = 5500;

        Socket socket = new Socket();
        InetSocketAddress endpoint = new InetSocketAddress(host, port);
        try {
            socket.connect(endpoint); // Make TCP connection
            try(InputStream in = socket.getInputStream();
                InputStreamReader rdr = new InputStreamReader(in);
                BufferedReader bufferedReader = new BufferedReader(rdr);
                OutputStream out = socket.getOutputStream();
                PrintStream printStream = new PrintStream(out, true)) {

                // Build HTTP Request
                String httpRequest = "GET /index.html HTTP/1.1" + CRLF + // Request-Line
                        // Add request headers
                        "Host: " + host + ":" + port + CRLF +
                        "Accept: text/html" + CRLF +
                        "Accept-Language: uk-UA,uk;q=0.8,en-US;q=0.5,en;q=0.3" + CRLF +
                        "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0" + CRLF +
                        "Connection: close" + CRLF +
                        CRLF;
                System.out.println("httpRequest:\n" + httpRequest);

                printStream.print(httpRequest); // Send HTTP Request

                StringBuilder httpResponseBuilder = new StringBuilder();
                String line;
                // Receive HTTP Response
                while ((line = bufferedReader.readLine()) != null) {
                    httpResponseBuilder.append(line).append("\n");
                }
                String httpResponse = httpResponseBuilder.toString();
                System.out.println("\nhttpResponse:\n" + httpResponse);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
