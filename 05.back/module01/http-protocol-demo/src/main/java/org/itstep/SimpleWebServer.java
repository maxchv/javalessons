package org.itstep;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleWebServer {

    static class Handler implements Runnable {

        private final Socket client;

        Handler(Socket client) {
            this.client = client;
        }

        private void handle() {
            try (OutputStream out = client.getOutputStream();
                 PrintStream printStream = new PrintStream(out, true);
                 InputStream in = client.getInputStream();
                 InputStreamReader rdr = new InputStreamReader(in);
                 BufferedReader reader = new BufferedReader(rdr)) {

                // Get Client's Http Request
                StringBuilder httpRequest = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    httpRequest.append(line).append(System.lineSeparator());
                    if (line.isEmpty()) {
                        break;
                    }
                }
                System.out.println(httpRequest);

                // Print Http Response
                String body = "<h1>Hello World</h1>" +
                        "<p>Now is " + LocalTime.now().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)) + "</p>";
                printStream.print("HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html\r\n" +
                        "Content-Length: "+body.length()+"\r\n" +
                        "Connection: close\r\n" +
                        "\r\n" +
                        body);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {
            handle();
        }
    }

    public static void main(String[] args) throws IOException {
        HttpServletRequest
        final int port = 8080;
        final ExecutorService executorService = Executors.newCachedThreadPool();
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                Socket client = serverSocket.accept();
                executorService.submit(new Handler(client));
            }
        }
    }

}
