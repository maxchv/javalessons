# Module 1. Introduction to network technologies

1. What is network and server-side programming?
2. The objectives and tasks of the network and server-side programming.
3. What is a network?
4. Types of networks.
5. The OSI model.
6. Basic terms.
7. Client and server interaction pattern
