package org.example;

import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws URISyntaxException, IOException {
        //demo01();
       //demo02();
        // localhost
        InetAddress addr = InetAddress.getByAddress(new byte[]{127,0,0,1});
        System.out.println(addr.getHostAddress());
        addr = InetAddress.getByName("itstep.dp.ua");
        int[] arr = new int[4];
        byte[] ip = addr.getAddress();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Byte.toUnsignedInt(ip[i]);
        }
        System.out.println(Arrays.toString((arr)));

        addr = InetAddress.getLocalHost();
        System.out.println(addr);
    }

    private static void demo02() throws IOException {
        URL itstepUrl = new URL("https://itstep.dp.ua");
        Files.copy(itstepUrl.openStream(), Paths.get("index.html"));
    }

    private static void demo01() throws MalformedURLException {
        URL url = new URL("http://user:password@localhost/index.html?user=root&password=qwerty#anchore");
        System.out.println("protocol: " + url.getProtocol());
        System.out.println("authority: " + url.getAuthority());
        System.out.println("host: " + url.getHost());
        System.out.println("path: " + url.getPath());
        System.out.println("query: " + url.getQuery());
    }
}
