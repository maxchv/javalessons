package org.itstep.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement // <tx:annotation-driven />
@EnableJpaRepositories(basePackages = "org.itstep") //  <jpa:repositories base-package="org.itstep.security" />
public class JpaConfigTest { // application-context.xml.old

    @Bean
    public DataSource getDataSourceDev() {
        /*
            <jdbc:embedded-database type="H2" id="dataSource">
                <jdbc:script location="classpath*:init.sql" />
            </jdbc:embedded-database>
         */
        return new EmbeddedDatabaseBuilder()
                .addScript("init.sql")
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }

    @Bean("entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean getEntityManagerFactory(DataSource dataSource) {
        /*
            <bean id="entityManagerFactory"
                class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean"
                p:dataSource-ref="dataSource"
                p:packagesToScan="org.itstep.security">
                <property name="jpaVendorAdapter">
                    <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter"
                            p:generateDdl="true"
                            p:showSql="true"/>
                </property>
                <property name="jpaProperties">
                    <props>
                        <prop key="format_sql">true</prop>
                    </props>
                </property>
            </bean>
         */
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("org.itstep");
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setGenerateDdl(true);
        jpaVendorAdapter.setShowSql(true);
        em.setJpaVendorAdapter(jpaVendorAdapter);
        Properties props = new Properties();
        props.setProperty("format_sql", "true");
        em.setJpaProperties(props);

        return em;
    }

    @Bean("transactionManager")
    public JpaTransactionManager getTransactionManager(EntityManagerFactory entityManagerFactory) {
        /*
            <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager"
                    p:entityManagerFactory-ref="entityManagerFactory"/>
        */
        return new JpaTransactionManager(entityManagerFactory);
    }
}
