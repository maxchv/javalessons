package org.itstep.config;

import org.h2.server.web.WebServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;


public class WebInit implements WebApplicationInitializer { /* web.xml.old */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext ctxt = new AnnotationConfigWebApplicationContext();
        ctxt.register(WebConfig.class); // <init-param><param-name>contextConfigLocation</param-name>
                                        // <param-value>/WEB-INF/config/dispatcher-servlet.xml.old</param-value></init-param>
        ctxt.setServletContext(servletContext);

        /*
        <listener>
            <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
        </listener>
         */
        servletContext.addListener(new ContextLoaderListener(ctxt));

        /*
         <servlet>
            <servlet-name>dispatcherServlet</servlet-name>
            <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
            <init-param>
                <param-name>contextConfigLocation</param-name>
                <param-value>/WEB-INF/config/dispatcher-servlet.xml.old</param-value>
            </init-param>
            <load-on-startup>1</load-on-startup>
        </servlet>
         */
        ServletRegistration.Dynamic dispatcherServlet =
                servletContext.addServlet("dispatcherServlet", new DispatcherServlet(ctxt));
        dispatcherServlet.setLoadOnStartup(1);
        /*
        <servlet-mapping>
            <servlet-name>dispatcherServlet</servlet-name>
            <url-pattern>/</url-pattern>
        </servlet-mapping>
         */
        dispatcherServlet.addMapping("/");

        /*
        <servlet>
            <servlet-name>h2console</servlet-name>
            <servlet-class>org.h2.server.web.WebServlet</servlet-class>
        </servlet>
         */
        ServletRegistration.Dynamic h2console = servletContext.addServlet("h2console", WebServlet.class);
        /*
            <servlet-mapping>
            <servlet-name>h2console</servlet-name>
            <url-pattern>/h2console/*</url-pattern>
        </servlet-mapping>
         */
        h2console.addMapping("/h2console/*");

        /*
            <filter>
                <filter-name>encodingFilter</filter-name>
                <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
                <init-param>
                    <param-name>encoding</param-name>
                    <param-value>UTF-8</param-value>
                </init-param>
                <init-param>
                    <param-name>forseRequestEncoding</param-name>
                    <param-value>true</param-value>
                </init-param>
                <init-param>
                    <param-name>forseResponseEncoding</param-name>
                    <param-value>true</param-value>
                </init-param>
            </filter>
         */
        FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encodingFilter",
                new CharacterEncodingFilter("UTF-8", true, true));
        EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST);
        /*
            <filter-mapping>
                <filter-name>encodingFilter</filter-name>
                <url-pattern>/*</url-pattern>
            </filter-mapping>
         */
        encodingFilter.addMappingForUrlPatterns(dispatcherTypes, true, "/*");
    }
}
