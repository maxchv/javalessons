package org.itstep.service;

import org.itstep.domain.entities.AutoUser;

public interface AutoUserService {
    AutoUser findByName(String name);
}
