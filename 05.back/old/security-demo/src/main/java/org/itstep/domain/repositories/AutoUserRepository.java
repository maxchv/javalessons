package org.itstep.domain.repositories;

import org.itstep.domain.entities.AutoUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoUserRepository extends JpaRepository<AutoUser, Long> {
    AutoUser findUserByUsername(String username);
}
