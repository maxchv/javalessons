package org.itstep.config;

import org.itstep.domain.repositories.AutoUserRepository;
import org.itstep.security.CustomAuthenticationProvider;
import org.itstep.security.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@ComponentScan("org.itstep")
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

    final DataSource dataSource;

    final AutoUserRepository autoUserRepository;

    public WebSecurity(AutoUserRepository autoUserRepository, DataSource dataSource) {
        this.autoUserRepository = autoUserRepository;
        this.dataSource = dataSource;
    }

    @Override
    public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
            <sec:http auto-config="true" use-expressions="true">
            <sec:intercept-url pattern="/appointments/**" access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"/>
            <sec:intercept-url pattern="/schedule/**" access="hasAnyRole('ROLE_USER','ROLE_ADMIN')"/>
            <sec:intercept-url pattern="/**" access="permitAll()"/>
            <sec:form-login login-page="/login"
                            login-processing-url="/login"
                            username-parameter="custom_username"
                            password-parameter="custom_password"
                            authentication-failure-url="/login?error=credentials"/>
            <sec:logout logout-url="/logout"
                        logout-success-url="/"
                        invalidate-session="true"
                        delete-cookies="true"/>
            <sec:remember-me key="rememberMe" remember-me-parameter="remember-me" />
        </sec:http>
         */
        http.
                authorizeRequests(authorizeRequests ->
                        authorizeRequests
                                .antMatchers("/schedule/**").access("hasAnyRole('ADMIN', 'USER')")
                                .antMatchers("/appointment/**").hasAnyRole("ADMIN", "USER")
                                .antMatchers("/**").permitAll()

                )
                .formLogin(loginForm ->
                        loginForm
                                .loginPage("/login")
                                .loginProcessingUrl("/login")
                                .usernameParameter("custom_username")
                                .passwordParameter("custom_password")
                                .failureForwardUrl("/login?error=credentials")
                                .permitAll()
                )
                .logout(logout ->
                        logout
                                .logoutUrl("/logout")
                                .logoutSuccessUrl("/")
                                .invalidateHttpSession(true)
                                .deleteCookies()
                                .permitAll()
                )
                .rememberMe(rememberMe ->
                        rememberMe
                                .key("rememberMy").rememberMeParameter("remember-me")
                                .tokenRepository(tokenRepository())
                )
                .oauth2Login(ouaht2Login ->
                        ouaht2Login
                                .clientRegistrationRepository(clientRegistrationRepository())
                                .authorizedClientService(authorizedClientService())
                                .defaultSuccessUrl("/success")
                                .loginPage("/login")
                );
    }

    @Bean
    public OAuth2AuthorizedClientService authorizedClientService() {
        return new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository());
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(
                CommonOAuth2Provider
                        .GOOGLE.getBuilder("google")
                        .clientId("623209115479-neslrh1pkdon40n7te9s2bmicm7j6jkg.apps.googleusercontent.com")
                        .clientSecret("76gvYwFan3wxg4CDnB3MMqVv")
                        .build()
        );
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.inMemoryAuthentication()
                .withUser(new User("admin", "admin", AuthorityUtils.createAuthorityList("ROLE_ADMIN")));
         */
        auth.authenticationProvider(customAuthenticationProvider());
    }

    @Bean
    public AuthenticationProvider customAuthenticationProvider() {
        return new CustomAuthenticationProvider(getUserDetailsService(), passwordEncoder());
    }

    @Bean
    public UserDetailsService getUserDetailsService() {
        return new CustomUserDetailsService(autoUserRepository);
    }

    @Bean
    public PersistentTokenRepository tokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        jdbcTokenRepositoryImpl.setDataSource(dataSource);
        return jdbcTokenRepositoryImpl;
    }
}
