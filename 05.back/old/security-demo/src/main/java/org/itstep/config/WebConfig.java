package org.itstep.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc //  <mvc:annotation-driven />
@Configuration
@ComponentScan(basePackages = "org.itstep") // <context:component-scan base-package="org.itstep.security" />
public class WebConfig implements WebMvcConfigurer { /* dsipatcher-servlet.xml */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // <mvc:resources mapping="/resources/**" location="/resources/" />
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("/resources/");
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        // <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
        //          p:prefix="/WEB-INF/views/" p:suffix=".jsp" />
        registry.viewResolver(new InternalResourceViewResolver("/WEB-INF/views/", ".jsp"));
    }
}
