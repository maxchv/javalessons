package org.itstep.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.itstep.domain.entities.AutoUser;
import org.itstep.domain.repositories.AutoUserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@RequestMapping("/")
public class HomeController {

    final AutoUserRepository autoUserRepository;
    final PasswordEncoder passwordEncoder;

    public HomeController(AutoUserRepository autoUserRepository, PasswordEncoder passwordEncoder) {
        this.autoUserRepository = autoUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public String goHome() {
        log.info("home");
        return "home";
    }

    @GetMapping("/services")
    public String goServices() {
        return "services";
    }

    @GetMapping("/login")
    public String goLogin() {
        return "login";
    }

    @GetMapping("/schedule")
    public String goSchedule() {
        return "schedule";
    }

    @GetMapping("/register")
    public String goRegister(){
        return "register";
    }

    @GetMapping("/success")
    public String successGoogleSingIn(Authentication authentication) {
        log.info("{}", authentication);

        if(authentication instanceof OAuth2AuthenticationToken) {
            OAuth2User oAuth2User = ((OAuth2AuthenticationToken) authentication).getPrincipal();
            String given_name = oAuth2User.getAttribute("given_name");
            String family_name = oAuth2User.getAttribute("family_name");
            String password = oAuth2User.getAttribute("name");
            String email = oAuth2User.getAttribute("email");

            AutoUser autoUser = new AutoUser(given_name, family_name, email, password, "ROLE_USER", email);
            autoUser.setGoogleAccount(true);

            SecurityContextHolder.clearContext(); // logout
            registerAndSingIn(autoUser);
        }

        return "redirect:/";
    }

    @PostMapping("/register")
    public String doRegister(@ModelAttribute AutoUser autoUser) {
        log.info("Register user: {}", autoUser);
        registerAndSingIn(autoUser);
        return "redirect:/";
    }

    @Transactional
    public void registerAndSingIn(@ModelAttribute AutoUser autoUser) {
        autoUser.setPassword(passwordEncoder.encode(autoUser.getPassword()));
        autoUser.setRole("ROLE_USER");
        autoUserRepository.save(autoUser);
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        Authentication authentication =
                new UsernamePasswordAuthenticationToken(autoUser, autoUser.getPassword(), autoUser.getAuthorities());
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @ResponseBody
    @GetMapping("/hashing")
    public String passwordHash(@RequestParam String pass) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(pass);
    }

}
