package org.itstep.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.itstep.domain.entities.Appointment;
import org.itstep.service.AppointmentService;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;


@Slf4j
@Controller
@RolesAllowed({"ROLE_ADMIN", "ROLE_USER"}) // @Secured
@RequestMapping("/appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @ModelAttribute
    public Appointment getAppointment() {
        return new Appointment();
    }

    @GetMapping
    public String index() {
        return "appointments";
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @PostFilter("filterObject.user.autoUserId == principal.autoUserId")
    public List<Appointment> saveAppointment(@ModelAttribute Appointment appointment) {
        log.info("Save appointment {}", appointment);
        appointmentService.saveForCurrentUser(appointment);
        return appointmentService.findAllByCurrentUser();
    }

    @ResponseBody
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostFilter("filterObject.user.autoUserId == principal.autoUserId")
    public List<Appointment> getAppointments() {
        log.info("Get all appointments");
        return appointmentService.findAllByCurrentUser();
    }

    @PostAuthorize("principal.autoUserId == #model['appointment'].user.autoUserId")
    @GetMapping(value = "/{appointmentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAppointment(@PathVariable("appointmentId") Long appointmentId, Model model) {
        Appointment appointment = appointmentService.findById(appointmentId);
        log.info("Get appointment {} by id {}", appointment, appointmentId);
        model.addAttribute("appointment", appointment);
        return "appointment";
    }

    @ResponseBody
    @GetMapping("/{appointmentId}/confirm")
    public String confirm(@PathVariable("appointmentId") Long appointmentId) {
        log.info("Confirmed");
        appointmentService.setStatus(appointmentId, "Confirmed");
        return "Confirmed";
    }

    @ResponseBody
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{appointmentId}/cancel")
    public String cancel(@PathVariable("appointmentId") Long appointmentId) {
        log.info("Cancelled");
        appointmentService.setStatus(appointmentId, "Cancelled");
        return "Cancelled";
    }

    @ResponseBody
    @GetMapping("/{appointmentId}/complete")
    public String complete(@PathVariable("appointmentId") Long appointmentId) {
        log.info("Completed");
        appointmentService.setStatus(appointmentId, "Completed");
        return "Completed";
    }
}
