package org.itstep.resttemplatedemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import okhttp3.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;

@SpringBootApplication
public class RestTemplateDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestTemplateDemoApplication.class, args);
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<Response> response = restTemplate.getForEntity(url, Response.class);
//        if(response.getStatusCode().is2xxSuccessful()) {
//            System.out.println(response.getBody());
//        }

    }

    @Component
    class Runner implements CommandLineRunner {

        @Override
        public void run(String... args) throws Exception {
            String url = "https://api.telegram.org/bot1233640978:AAEwWhcwlrBm-ka3s-CklfzBZrpeaQlRiKM/getUpdates?offset=-1";

            Request request = new Request.Builder()
                    .url(url)
                    .build();
            OkHttpClient client = new OkHttpClient();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ObjectMapper objectMapper = new ObjectMapper();
                    org.itstep.resttemplatedemo.entity.Response response1 = objectMapper.readValue(response.body().string(), org.itstep.resttemplatedemo.entity.Response.class);
                    System.out.println(response1);
                }
            });
        }
    }

}
