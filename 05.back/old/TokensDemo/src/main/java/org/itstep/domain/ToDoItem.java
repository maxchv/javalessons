package org.itstep.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ToDoItem {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @NonNull
    private String title;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Date created;

    @ManyToOne
    @JsonIgnore
    private User user;

    private Boolean done = false;

    private Boolean deleted = false;
}
