package org.itstep.init;

import org.itstep.domain.Role;
import org.itstep.domain.User;
import org.itstep.repository.RoleRepository;
import org.itstep.repository.ToDoItemRepository;
import org.itstep.repository.UserRepository;
import org.itstep.security.AuthoritiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class InitDatabase {

    private static boolean inited;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ToDoItemRepository toDoItemRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @PostConstruct
    @Transactional
    public void init() {
        if (inited) return;

        Role roleAdmin = new Role(AuthoritiesConstants.ADMIN);
        Role roleUser = new Role(AuthoritiesConstants.USER);
        roleRepository.saveAll(List.of(roleAdmin, roleUser));

        User admin = new User("admin", passwordEncoder.encode("admin"));
        admin.addRole(roleAdmin);
        admin.addRole(roleUser);
        userRepository.save(admin);

        User user = new User("user", passwordEncoder.encode("user"));
        user.addRole(roleUser);
        userRepository.save(user);

        inited = true;
    }
}
