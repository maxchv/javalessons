package org.itstep.service;

import org.itstep.domain.User;

public interface UserService {
    void save(User user);
}
