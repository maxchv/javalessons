package org.itstep.web.rest;

import org.itstep.domain.ToDoItem;
import org.itstep.repository.ToDoItemRepository;
import org.itstep.security.util.SecurityUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/todo")
public class ToDoItemResource {

    final ToDoItemRepository toDoItemRepository;

    public ToDoItemResource(ToDoItemRepository toDoItemRepository) {
        this.toDoItemRepository = toDoItemRepository;
    }

    @GetMapping
    public List<ToDoItem> all() {
        return toDoItemRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ToDoItem> find(@PathVariable Long id) {
        return ResponseEntity.of(toDoItemRepository.findById(id));
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Void> save(@Validated @RequestBody ToDoItem toDoItem, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().build();
        }
        toDoItem.setUser(SecurityUtils.getCurrentUser().orElse(null));
        toDoItemRepository.save(toDoItem);
        return ResponseEntity.created(URI.create("/api/v1/todo/"+toDoItem.getId())).build();
    }
}
