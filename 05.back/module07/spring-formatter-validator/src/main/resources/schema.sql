drop table if exists students;
drop table if exists groups;
create table groups
(
    id   integer generated by default as identity,
    name varchar(255),
    primary key (id)
);
create table students
(
    id          integer generated by default as identity,
    dateOfBirth date,
    firstName   varchar(255),
    lastName    varchar(255),
    phone       varchar(255),
    group_id    integer,
    primary key (id)
);
alter table if exists groups add unique (name);
alter table if exists students add foreign key (group_id) references groups;