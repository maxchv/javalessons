<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <style>
        label {
            display: inline-block;
            width: 100px;
        }
        .error {
            color: red;
        }
    </style>
</head>
<body>
    <%--@elvariable id="${student}" type="org.itstep.controller.domain.entity.Student`"--%>
    <form:form modelAttribute="student" method="post">
        <form:hidden path="id"/>
        <div>
            <form:label path="firstName"><fmt:message key="FirstName"/> </form:label>
            <form:input path="firstName" required="required"/>
            <form:errors path="firstName" cssClass="error"/>
        </div>
        <div>
            <form:label path="lastName">Last Name</form:label>
            <form:input path="lastName"/>
            <form:errors path="lastName" cssClass="error"/>
        </div>
        <div>
            <form:label path="dateOfBirth">Date of birth</form:label>
            <form:input path="dateOfBirth"/>
            <form:errors path="dateOfBirth" cssClass="error"/>
        </div>
        <div>
            <form:label path="phone">Phone</form:label>
            <form:input path="phone" type="tel"/>
            <form:errors path="phone" cssClass="error"/>
        </div>
        <div>
            <form:label path="group">Group</form:label>
            <form:select path="group" items="${groups}" itemLabel="name" itemValue="name"/>
        </div>
        <input type="submit"/>
    </form:form>

</body>
</html>
