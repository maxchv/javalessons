package org.itstep.controller.domain.dao;

import org.itstep.controller.domain.entity.Group;

public interface GroupDao extends Dao<Group, Integer> {
}
