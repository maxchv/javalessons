package org.itstep.controller.domain.dao.impl;

import org.itstep.controller.domain.dao.StudentDao;
import org.itstep.controller.domain.entity.Group;
import org.itstep.controller.domain.entity.Student;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class StudentDaoImpl implements StudentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Student entity) {
        if(entity.getGroup() != null) {
            Group g = entityManager.createQuery("from Group g where g.name=?1", Group.class)
                    .setParameter(1, entity.getGroup().getName())
                    .getSingleResult();
            entity.setGroup(g);
        }
        entityManager.persist(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Student findById(Integer id) {
        return entityManager.find(Student.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Student> findAll() {
        return entityManager.createQuery("from Student", Student.class).getResultList();
    }

    @Override
    public Student update(Student entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(Student entity) {
        entityManager.remove(entityManager.find(Student.class, entity.getId()));
    }
}
