package org.itstep.controller.validator;

import org.itstep.controller.domain.entity.Student;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class StudentValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Student.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        System.out.println("Validate...");
        Student student = (Student) o;
        if(student.getFirstName() == null || "".equals(student.getFirstName().trim())){
            errors.rejectValue("firstName", "EmptyOrNull");
        }
        ValidationUtils
                .rejectIfEmptyOrWhitespace(errors,"lastName", "EmptyOrNull");

    }
}
