package org.itstep.controller;

import org.itstep.controller.domain.dao.GroupDao;
import org.itstep.controller.domain.dao.StudentDao;
import org.itstep.controller.domain.entity.Group;
import org.itstep.controller.domain.entity.Student;
import org.itstep.controller.validator.StudentValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class HomeController {

    private final StudentDao studentDao;
    private final GroupDao groupDao;

    public HomeController(StudentDao studentDao, GroupDao groupDao) {
        this.studentDao = studentDao;
        this.groupDao = groupDao;
    }

    @InitBinder
    public void init(WebDataBinder dataBinder) {
        dataBinder.addValidators(new StudentValidator());
    }

    @ModelAttribute(name = "groups")
    public List<Group> getGroups() {
        return groupDao.findAll();
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("student", new Student());
        return "index";
    }

    @PostMapping
    public String create(@Validated @ModelAttribute Student student,
                         BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "index";
        }
        System.out.println(student);
        studentDao.save(student);
        return "redirect:/";
    }

}
