package org.itstep.controller.domain.dao.impl;

import org.itstep.controller.domain.dao.GroupDao;
import org.itstep.controller.domain.entity.Group;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class GroupDaoImpl implements GroupDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Group entity) {
        entityManager.persist(entity);
    }

    @Override
    public Group findById(Integer id) {
        return entityManager.find(Group.class, id);
    }

    @Override
    public List<Group> findAll() {
        return entityManager.createQuery("from Group", Group.class).getResultList();
    }

    @Override
    public Group update(Group entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(Group entity) {
        entityManager.remove(entityManager.find(Group.class, entity.getId()));
    }
}
