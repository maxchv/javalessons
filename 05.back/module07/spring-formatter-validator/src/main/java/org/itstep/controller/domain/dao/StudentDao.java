package org.itstep.controller.domain.dao;

import org.itstep.controller.domain.entity.Student;

public interface StudentDao extends Dao<Student, Integer>{
}
