package org.itstep.controller;

import org.hibernate.Hibernate;
import org.itstep.domain.entity.Car;
import org.itstep.repository.CarRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.matching;

@Controller
public class HomeController {

    private final CarRepository carRepository;

    public HomeController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("car", new Car());
        return "index";
    }

    @PostMapping
    public String create(@Validated @ModelAttribute Car car, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            return "index";
        }
        car = carRepository.save(car);
        System.out.println("car = " + car);
        System.out.println("State: " + Hibernate.isInitialized(car));
        return "redirect:/";
    }

    @GetMapping("/all")
    public String all(Model model) {
        model.addAttribute("cars", carRepository.findAll());
        return "all";
    }

    @GetMapping("/color")
    public String findByColor(@RequestParam(required = false) String color, Model model) {
        System.out.println("color = " + color);
        if(color != null) {
//            Car car = null; //carRepository.carByColor(color);
//            Car probe = new Car();
//            probe.setColor(color);
//            ExampleMatcher colorMatcher = matching().withMatcher("color", contains());
//            car = carRepository.findOne(Example.of(probe, colorMatcher)).orElse(null);
            model.addAttribute("car", carRepository.findCarByColorContains(color));
        }
        return "one";
    }
}
