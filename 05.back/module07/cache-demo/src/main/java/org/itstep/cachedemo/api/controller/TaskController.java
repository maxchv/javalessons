package org.itstep.cachedemo.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.itstep.cachedemo.domain.entity.Task;
import org.itstep.cachedemo.service.TaskService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/v1/tasks")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> createTask(@RequestBody @Validated Task task, BindingResult bindingResult) {
        log.info("createTask({})", task);
        if(bindingResult.hasErrors()) {
            log.error("hasErrors: {}", bindingResult.getAllErrors());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Task newTask = taskService.save(task);
        log.info("newTask: {}", newTask);
        return ResponseEntity.created(URI.create("/api/v1/tasks/" + task.getId())).body(newTask);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Task>> getTasks(@RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size) {
        page = page == null ? 0 : page;
        size = size == null ? 10 : size;
        return ResponseEntity.ok(taskService.findAllTasks(PageRequest.of(page, size)));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> getTaskById(@PathVariable Long id) {
        log.info("Find task by id: {}", id);
        return ResponseEntity.of(taskService.findTaskById(id));
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> updateTask(@PathVariable Long id, Task task) {
        return ResponseEntity.ok(taskService.update(task));
    }
}
