package org.itstep.cachedemo.domain.repository;

import org.itstep.cachedemo.domain.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
