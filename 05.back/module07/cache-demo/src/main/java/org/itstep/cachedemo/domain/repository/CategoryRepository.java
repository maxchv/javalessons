package org.itstep.cachedemo.domain.repository;

import org.itstep.cachedemo.domain.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
