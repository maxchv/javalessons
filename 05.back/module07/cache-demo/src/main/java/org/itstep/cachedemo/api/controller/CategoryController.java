package org.itstep.cachedemo.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.itstep.cachedemo.domain.entity.Category;
import org.itstep.cachedemo.domain.entity.Task;
import org.itstep.cachedemo.service.TaskService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final TaskService taskService;

    public CategoryController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> createCategory(@RequestBody @Validated Category category, BindingResult bindingResult) {
        log.info("createCategory({})", category);
        if(bindingResult.hasErrors()) {
            log.error("hasErrors: {}", bindingResult.getAllErrors());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Category newCategory = taskService.save(category);
        log.info("newCategory: {}", newCategory);
        return ResponseEntity.created(URI.create("/api/v1/categories/" + newCategory.getId())).body(newCategory);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Category>> getCategories(@RequestParam(required = false) Integer page,
                                               @RequestParam(required = false) Integer size) {
        page = page == null ? 0 : page;
        size = size == null ? 10 : size;
        return ResponseEntity.ok(taskService.findAllCategories(PageRequest.of(page, size)));
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> getCategoryById(@PathVariable Long id) {
        return ResponseEntity.of(taskService.findCategoryById(id));
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Category> updateTask(@PathVariable Long id, Category category) {
        return ResponseEntity.ok(taskService.update(category));
    }
}
