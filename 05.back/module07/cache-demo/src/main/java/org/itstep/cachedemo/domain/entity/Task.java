package org.itstep.cachedemo.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Length(max = 100)
    @JsonProperty(value = "title", required = true)
    private String title;

    @Column(columnDefinition = "text")
    private String description;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created = new Date();

    @ManyToOne
    private Category category;
}
