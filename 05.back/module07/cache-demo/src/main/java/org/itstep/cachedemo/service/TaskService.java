package org.itstep.cachedemo.service;

import org.itstep.cachedemo.domain.entity.Category;
import org.itstep.cachedemo.domain.entity.Task;
import org.itstep.cachedemo.domain.repository.CategoryRepository;
import org.itstep.cachedemo.domain.repository.TaskRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class TaskService {
    private final TaskRepository taskRepository;
    private final CategoryRepository categoryRepository;

    public TaskService(TaskRepository taskRepository, CategoryRepository categoryRepository) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
    }

    public Task save(Task task) {
        return taskRepository.save(task);
    }

    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Cacheable(value = "task", key = "#id")
    @Transactional(readOnly = true)
    public Optional<Task> findTaskById(Long id) {
        return taskRepository.findById(id);
    }

    @Cacheable(value = "category", key = "#id")
    @Transactional(readOnly = true)
    public Optional<Category> findCategoryById(Long id) {
        return categoryRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Page<Task> findAllTasks(Pageable pageable) {
        return taskRepository.findAll(pageable);
    }

    public Task update(Task task) {
        return taskRepository.saveAndFlush(task);
    }

    public Page<Category> findAllCategories(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }

    public Category update(Category category) {
        return categoryRepository.saveAndFlush(category);
    }
}
