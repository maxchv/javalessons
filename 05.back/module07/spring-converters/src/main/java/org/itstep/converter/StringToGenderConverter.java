package org.itstep.converter;

import org.itstep.domain.enitity.Gender;
import org.springframework.core.convert.converter.Converter;

import java.util.Map;

public class StringToGenderConverter implements Converter<String, Gender> {
    @Override
    public Gender convert(String s) {
        Map<String, Gender> conv = Map.of(
                "m", Gender.MALE,
                "f", Gender.FEMALE,
                "u", Gender.UNKNOWN
        );
        return conv.getOrDefault(s, Gender.UNKNOWN);
    }
}
