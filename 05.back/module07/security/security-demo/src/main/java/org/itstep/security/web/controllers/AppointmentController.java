package org.itstep.security.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.itstep.security.domain.entities.Appointment;
import org.itstep.security.domain.entities.AutoUser;
import org.itstep.security.domain.repositories.AppointmentRepository;
import org.itstep.security.domain.repositories.AutoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@Controller
@RequestMapping("/appointments")
public class AppointmentController {

    @Autowired
    AutoUserRepository autoUserRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @ModelAttribute
    public Appointment getAppointment() {
        return new Appointment();
    }

    @GetMapping
    public String index() {
        return "appointments";
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    //@RolesAllowed("ROLE_ADMIN")
    @ResponseBody
    @GetMapping("/admin")
    public String onlyForAdmin() {
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "You are admin";
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public List<Appointment> saveAppointment(@ModelAttribute Appointment appointment) {
        log.info("Save appointment {}", appointment);
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        AutoUser user = autoUserRepository.findUserByUsername(principal.getUsername()).orElse(null); // new AutoUser();
//        user.setEmail("test@email.com");
//        user.setFirstName("Joe");
//        user.setLastName("Doe");
        appointment.setUser(user);
        appointment.setStatus("Initial");
        appointmentRepository.save(appointment);
        return this.appointmentRepository.findAll();
    }

    @ResponseBody
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> getAppointments() {
        log.info("Get all appointments");
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return this.appointmentRepository.findAllByUserUsername(principal.getUsername());
    }

    @PostAuthorize("#principal.username=#model['appointment'].user.username")
    @GetMapping(value = "/{appointmentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAppointment(@PathVariable("appointmentId") Long appointmentId, Model model) {
        Appointment appointment = appointmentRepository.findById(appointmentId).orElse(null);
        log.info("Get appointment {} by id {}", appointment, appointmentId);
        model.addAttribute("appointment", appointment);
        return "appointment";
    }

}
