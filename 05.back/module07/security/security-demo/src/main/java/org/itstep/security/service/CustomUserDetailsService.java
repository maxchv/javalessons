package org.itstep.security.service;

import org.itstep.security.domain.entities.AutoUser;
import org.itstep.security.domain.repositories.AutoUserRepository;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final AutoUserRepository autoUserRepository;

    public CustomUserDetailsService(AutoUserRepository autoUserRepository) {
        this.autoUserRepository = autoUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AutoUser> autoUserOptional = autoUserRepository.findUserByUsername(username);
        if(autoUserOptional.isEmpty()) throw new UsernameNotFoundException("Not found by " + username);
        AutoUser autoUser = autoUserOptional.get();
        return new User(autoUser.getUsername(), autoUser.getPassword(),
                AuthorityUtils.createAuthorityList(autoUser.getRole()));
    }
}
