package org.itstep.security.domain.dto;

import lombok.Data;

@Data
public class UserDto {
    private String email;
    private String lastName;
    private String firstName;
    private String username;
    private String password;
}
