package org.itstep.security.domain.repositories;

import org.itstep.security.domain.entities.AutoUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AutoUserRepository extends JpaRepository<AutoUser, Long> {

    Optional<AutoUser> findUserByUsername(String username);

}
