package org.itstep.security.web.controllers;

import org.itstep.security.domain.dto.UserDto;
import org.itstep.security.domain.entities.AutoUser;
import org.itstep.security.domain.repositories.AutoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    AutoUserRepository autoUserRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/encode")
    @ResponseBody
    public String getEncodedText(@RequestParam String text) {
        return passwordEncoder.encode(text);
    }

    @GetMapping
    public String goHome() {
        return "home";
    }

    @GetMapping("/services")
    public String goServices() {
        return "services";
    }

    @GetMapping("/login")
    public String goLogin() {
        return "login";
    }

    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute UserDto user) {
        String password = passwordEncoder.encode(user.getPassword());
        AutoUser autoUser = new AutoUser(user.getFirstName(), user.getLastName(),
                user.getUsername(), password, "ROLE_USER" ,user.getEmail());
        autoUserRepository.save(autoUser);
        SecurityContext securityContext =  SecurityContextHolder.getContext(); //SecurityContextHolder.createEmptyContext();
        User principal = new User(autoUser.getUsername(), autoUser.getPassword(),
                AuthorityUtils.createAuthorityList(autoUser.getRole()));
        Authentication authentication = new UsernamePasswordAuthenticationToken(principal, principal.getAuthorities());
        securityContext.setAuthentication(authentication);
        return "redirect:/";
    }

    @GetMapping("/schedule")
    public String goSchedule() {
        return "schedule";
    }
}
