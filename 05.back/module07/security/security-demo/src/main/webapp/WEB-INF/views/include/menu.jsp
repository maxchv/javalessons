<%@page isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a href="<spring:url value="/"/>" class="navbar-brand">Kevin's Auto Service Center</a>
        </div>
        <sec:authentication property="name" var="username"/>
        <sec:authorize access="authenticated" var="auth"/>
        <ul class="nav navbar-nav">
            <li><a href="<spring:url value="/services"/>">Services</a></li>
            <c:if test="${auth}">
                <li><a href="<spring:url value="/appointments"/>">Appointments</a></li>
            </c:if>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <c:choose>
                <c:when test="${auth}">
                    <li><p class="navbar-text"> Welcome ${username}</p> </li>
                    <li><a href="<spring:url value="/h2console"/>" target="_blank">H2 console</a></li>
                    <li><a id="logout" href="<spring:url value="/logout"/>">Sing Out</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<spring:url value="/register"/>">Register</a></li>
                    <li><a href="<spring:url value="/login"/>">Sign In</a></li>
                </c:otherwise>
            </c:choose>


        </ul>
    </div>
</nav>
<form id="form-logout" method="post" action="<spring:url value="/logout"/>">
    <sec:csrfInput/>
</form>
<script>
    $("#logout").click((e) => {
        e.preventDefault();
        $("#form-logout").submit();
    })
</script>