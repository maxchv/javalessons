<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Kevin's Auto Service Center</title>

    <%@include file="include/head.jsp"%>

</head>
<body>
<jsp:include page="include/menu.jsp"/>
<div class="container">
    <div class="row">
        <h1>Services</h1>
    </div>
    <form id="appointment-form" method="post" action="<spring:url value="/register"/>">
        <sec:csrfInput/>
        <div class="form-group">
            <label for="firstName">FirstName</label>
            <input name="firstName" id="firstName" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="lastName">Last Name</label>
            <input name="lastName" id="lastName" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input name="email" id="email" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input name="username" id="username" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control"/>
        </div>
        <button type="submit" id="btn-save" class="btn btn-primary">Register</button>
    </form>
</div>
</body>
</html>