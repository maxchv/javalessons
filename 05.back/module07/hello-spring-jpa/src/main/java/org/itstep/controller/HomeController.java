package org.itstep.controller;

import org.itstep.controller.domain.dao.TaskDao;
import org.itstep.controller.domain.entity.Task;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    private final TaskDao taskDao;

    public HomeController(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @PostMapping
    public String create(Task task) {
        taskDao.save(task);
        return "redirect:/";
    }

}
