package org.itstep.controller.domain.dao.impl;

import org.itstep.controller.domain.dao.TaskDao;
import org.itstep.controller.domain.entity.Task;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TaskDaoImpl implements TaskDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    @Transactional
    public void save(Task data) {
//        try {
            //entityManager.getTransaction().begin();
            entityManager.persist(data);
//            entityManager.getTransaction().commit();
//        } catch (Throwable ex) {
//            entityManager.getTransaction().rollback();
//        }
    }

    @Override
    public Task findById(Integer integer) {
        return null;
    }

    @Override
    public List<Task> findAll() {
        return null;
    }

    @Override
    public void delete(Task data) {

    }

    @Override
    public Task update(Task data) {
        return null;
    }
}
