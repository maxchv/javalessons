package org.itstep.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@ToString(exclude = {"user", "posts"})
@Table(name = "authors")
public class Author {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    @Column(name = "first_name")
    private String firstName;

    @NonNull
    @Column(name = "last_name")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "id", foreignKey = @ForeignKey(name = "id"))
    private User user;

    @OneToMany(mappedBy = "author")
    private List<Post> posts;
}
