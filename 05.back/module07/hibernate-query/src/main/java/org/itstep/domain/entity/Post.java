package org.itstep.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "posts")
@EqualsAndHashCode(exclude = "tags")
public class Post {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String title;

    @NonNull
    @Column(columnDefinition = "text")
    private String body;

    @NonNull
    private Date published;

    @ManyToOne(cascade = CascadeType.ALL)
    //@JoinColumn(name = "author_id", foreignKey = @ForeignKey(name="author_fk"))
    private Author author;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name ="posts_tags",
            foreignKey = @ForeignKey(name = "post_fk"),
            inverseForeignKey = @ForeignKey(name = "tag_fk")
    )
    private Set<Tag> tags = new HashSet<>();

    public void addTag(Tag tag) {
        tags.add(tag);
        tag.getPosts().add(this);
    }
}
