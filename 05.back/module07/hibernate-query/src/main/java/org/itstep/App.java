package org.itstep;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.itstep.domain.entity.Author;
import org.itstep.domain.entity.Post;
import org.itstep.domain.entity.Tag;
import org.itstep.domain.entity.User;

import java.util.Date;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();

        Session session = null;
        Transaction tx = null;
        Author author = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
//            savePost(session);
//            Post post = session.get(Post.class, 1);
//            System.out.println(post);

            // SQL - HQL (Hibernate Query Language)
            // SQL: SELECT * FROM posts
            // HQL: FROM Post
//            retrivePosts(session);
//            User user = session.get(User.class, 1); //new User("vasja", "qwerty");
//            //session.save(user);
//            System.out.println("user = " + user);
//            Author author = new Author("Василий", "Пупкин");
//            author.setUser(user);
//            session.save(author);
//            System.out.println("author = " + author);
////
//            Author copy = session.get(Author.class, 1);
//            System.out.println(copy);
//            System.out.println(copy.getUser());
//
//            Post post = new Post("First post", "First body", new Date());
//            post.setAuthor(author);
//            session.save(post);

            Post post = session.get(Post.class, 1);
            Tag tag = new Tag("test");
            session.saveOrUpdate(tag);
            post.addTag(tag);
            session.update(post);
            System.out.println(post);
            // persistence
            author = session.get(Author.class, 1);
            //System.out.println(author);
            System.out.println("---");
//            Hibernate.initialize(author.getPosts());
            tx.commit();
        } catch (Throwable ex) {
            if(tx!= null) tx.rollback();
            ex.printStackTrace();
        } finally {
            if(session != null) session.close();
        }
        // detached
        session = sessionFactory.openSession();
        if(author != null) {
            session.refresh(author);
            List<Post> posts = author.getPosts();
            for (Post p: posts) {
                System.out.println(p);
            }
        }
        session.close();
        sessionFactory.close();
    }

    private static void retrivePosts(Session session) {
        Query<Post> query = session.createQuery("from Post p where p.title like :title", Post.class);
        query.setParameter("title", "First post");
        List<Post> list = query.list();
        list.forEach(System.out::println);
    }

    private static void savePost(Session session) {
        Post post = new Post("First post", "First body", new Date());
        session.persist(post);
    }
}
