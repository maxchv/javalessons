package org.itstep.api;

import org.itstep.entity.Car;
import org.itstep.reposotory.CarRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cars/")
public class CarController {

    private final CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping
    public List<Car> getAll() {
        return carRepository.findAll();
    }

    @PostMapping
    public ResponseEntity<Car> createCar(@ModelAttribute Car car, BindingResult bindingResult) {
        System.out.println("car = " + car);
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(car);
        }
        return ResponseEntity.ok(carRepository.save(car));
    }

}
