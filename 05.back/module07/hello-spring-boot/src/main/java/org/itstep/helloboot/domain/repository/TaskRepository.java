package org.itstep.helloboot.domain.repository;

import org.itstep.helloboot.domain.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {
}
