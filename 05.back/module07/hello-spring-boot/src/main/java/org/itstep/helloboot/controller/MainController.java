package org.itstep.helloboot.controller;

import org.itstep.helloboot.domain.entity.Task;
import org.itstep.helloboot.domain.repository.TaskRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;

@Controller
public class MainController {

    private final TaskRepository taskRepository;

    public MainController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("title", "Hello Spring Boot!");
        model.addAttribute("time", LocalDateTime.now().toLocalTime());
        model.addAttribute("tasks", taskRepository.findAll());
        return "index";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("task", new Task());
        return "create";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute @Validated Task task, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "create";
        }
        taskRepository.save(task);
        return "redirect:/";
    }
}
