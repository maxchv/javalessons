package org.itstep.domain.dao;

import org.itstep.domain.entity.Book;

import java.util.List;

public interface BookDao {
    void save(Book... book);
    Book find(int id);
    List<Book> findAll();
    void update(Book book);
    void delete(int id);
}
