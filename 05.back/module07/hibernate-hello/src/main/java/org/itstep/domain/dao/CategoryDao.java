package org.itstep.domain.dao;

import org.itstep.domain.entity.Book;
import org.itstep.domain.entity.Category;

import java.util.List;

public interface CategoryDao {
    void save(Category... book);
    Category find(int id);
    List<Category> findAll();
    void update(Category book);
    void delete(int id);
}
