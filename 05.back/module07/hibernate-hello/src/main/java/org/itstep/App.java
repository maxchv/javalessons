package org.itstep;

import lombok.var;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.itstep.domain.dao.BookDao;
import org.itstep.domain.dao.CategoryDao;
import org.itstep.domain.entity.Book;
import org.itstep.domain.entity.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application.xml");
        BookDao bookDao = ctx.getBean(BookDao.class);
        CategoryDao categoryDao = ctx.getBean(CategoryDao.class);

//        Category category = new Category();
//        category.setName("Фантастика");
//
//        Book book = new Book("Молох", "Станислав Лем");
//        category.addBook(book);
//        categoryDao.save(category);
//        System.out.println(category);
//        System.out.println(category.getClass());
//        book.setCategory(category);
//        bookDao.save(book);
//        System.out.println(book);
//        System.out.println(book.getClass());

        Book book = bookDao.find(1);
//        var sessionFactory = new Configuration()
//                .configure()
//                .buildSessionFactory();
//        Session session = sessionFactory.openSession();
//        session.beginTransaction();

        System.out.println(book.getCategory());
//        session.getTransaction().commit();
//        session.close();
        System.out.println(book.getClass());

        categoryDao.find(1).getBooks().forEach(System.out::println);
    }
}
