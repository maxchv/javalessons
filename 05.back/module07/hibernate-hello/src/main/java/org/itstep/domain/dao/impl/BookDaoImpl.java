package org.itstep.domain.dao.impl;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.itstep.domain.dao.BookDao;
import org.itstep.domain.entity.Book;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class BookDaoImpl implements BookDao {

    private final SessionFactory sessionFactory;

    public BookDaoImpl() {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
    }

    @Override
    public void save(Book... book) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            for (Book b : book) {
                Integer id = (Integer) session.save(b);
                System.out.println("Id: " + id);
            }
            tx.commit();
        } catch (Throwable ex) {
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public Book find(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Book book = null;
        try {
            book = session.get(Book.class, id);
            Hibernate.initialize(book.getCategory());
            tx.commit();
        } catch (Throwable ex) {
            tx.rollback();
        } finally {
            session.close();
        }
        return book;
    }

    @Override
    public List<Book> findAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Book> books = null;
        try {
//            session.createNativeQuery("SELECT * FROM books", Book.class).list();

            books = session.createQuery("from Book", Book.class).list();
            tx.commit();
        } catch (Throwable ex) {
            tx.rollback();
        } finally {
            session.close();
        }
        return books;
    }

    @Override
    public void update(Book book) {
        // session.update()
        // session.merge() JPA
    }

    @Override
    public void delete(int id) {
        // session.delete(entity)
    }
}
