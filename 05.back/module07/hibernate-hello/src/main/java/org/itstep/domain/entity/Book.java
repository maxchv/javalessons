package org.itstep.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "books")
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Book {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title", length = 100, nullable = false, unique = true)
    @NonNull
    private String title;
    @NonNull
    private String author;
    private String publisher;
    @Column(columnDefinition = "text")
    private String description;
    private int pages;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date created;
    private int year;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
}
