package org.itstep.controller.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PhoneValidation implements ConstraintValidator<Phone, String> {

    public static final String PHONE_PATTERN = "(\\+3)?(\\s*8)?\\s*\\(?0\\d{2}\\)?\\s*\\d{3}\\s*(-?\\s*\\d{2}){2}";

    @Override
    public void initialize(Phone phone) {

    }

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext constraintValidatorContext) {
        System.out.println("Validation: " + phone);
        return Pattern.matches(PHONE_PATTERN, phone);
    }
}
