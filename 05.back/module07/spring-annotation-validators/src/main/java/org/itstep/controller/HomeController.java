package org.itstep.controller;

import org.itstep.controller.dto.CustomerDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @GetMapping
    public String index(Model model) {
        model.addAttribute("customerDto", new CustomerDto());
        return "index";
    }

    @PostMapping
    public String create(@Validated @ModelAttribute CustomerDto customerDto, BindingResult bindingResult) {
        System.out.println("customerDto = " + customerDto);
        System.out.println("bindingResult = " + bindingResult);
        return "index";
    }

}
