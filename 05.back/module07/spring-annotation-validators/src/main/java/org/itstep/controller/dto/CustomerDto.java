package org.itstep.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.itstep.controller.validator.Phone;

import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
public class CustomerDto {
    // Не пустой! Длинна до 30
    @NotBlank
    @Length(max = 30)
    private String firstName;
    // Не пустой! Длинна до 50
    @NotBlank
    @Length(max = 30)
    private String lastName;
    // Может быть пустым, но соответствовать требованиям к Email
    @Email
    private String email;
    // Проверка по шаблону
    //@Pattern(regexp = "(\\+3)?(\\s*8)?\\s*\\(?0\\d{2}\\)?\\s*\\d{3}\\s*(-?\\s*\\d{2}){2}", message = "{customerDto.phone}")
    @Phone(message = "{customerDto.phone}")
    private String phone;
    // проверка на url
    @URL
    private String facebook;
}
