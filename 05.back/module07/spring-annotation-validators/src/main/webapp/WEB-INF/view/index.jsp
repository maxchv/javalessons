<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <style>
        label {
            display: inline-block;
            width: 100px;
        }

        .error {
            color: red;
        }
    </style>
</head>
<body>
<h1>Annotation driven Validators</h1>
<form:form modelAttribute="customerDto">
    <div>
        <form:label path="firstName">First name: </form:label>
        <form:input path="firstName"/>
        <form:errors path="firstName" cssClass="error"/>
    </div>
    <div>
        <form:label path="lastName">Last name: </form:label>
        <form:input path="lastName"/>
        <form:errors path="lastName" cssClass="error"/>
    </div>
    <div>
        <form:label path="email">Email: </form:label>
        <form:input path="email"/>
        <form:errors path="email" cssClass="error"/>
    </div>
    <div>
        <form:label path="phone">Phone: </form:label>
        <form:input path="phone" type="tel"/>
        <form:errors path="phone" cssClass="error"/>
    </div>
    <div>
        <form:label path="facebook">Facebook: </form:label>
        <form:input path="facebook"/>
        <form:errors path="facebook" cssClass="error"/>
    </div>
    <input type="submit">
</form:form>

<ul>
    <li><a href="https://beanvalidation.org/1.0/spec/">JSR 303</a></li>
    <li><a href="https://beanvalidation.org/1.1/spec/">JSR 349</a></li>
    <li><a href="https://hibernate.org/validator/documentation/getting-started/">Hibernate validator</a></li>
</ul>

</body>
</html>
