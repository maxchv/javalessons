package org.itstep.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Hibernate;
import org.itstep.domain.entity.Car;
import org.itstep.repository.CarRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Controller
public class HomeController {

    private final CarRepository carRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public HomeController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping(value = "/demo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String demo() {
        return "{\"id\":null,\"model\":\"Mercedes\",\"color\":\"red\",\"price\":1000}";
    }

    @GetMapping
    public String index(Model model) throws JsonProcessingException {
        // сериализация
        String json = objectMapper.writerFor(Car.class)
                .writeValueAsString(new Car("Mercedes", "red", new BigDecimal(1000)));
        model.addAttribute("json", json);
        // десериализация
        Car car = objectMapper.readerFor(Car.class)
                .readValue("{\"id\":null,\"model\":\"Mercedes\",\"color\":\"red\",\"price\":1000}");
        model.addAttribute("car", car);

        return "index";
    }

    @PostMapping
    public String create(@Validated @ModelAttribute Car car, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            return "index";
        }
        car = carRepository.save(car);
        System.out.println("car = " + car);
        System.out.println("State: " + Hibernate.isInitialized(car));
        return "redirect:/";
    }

    @GetMapping("/all")
    public String all(Model model) {
        model.addAttribute("cars", carRepository.findAll());
        return "all";
    }

    @GetMapping("/color")
    public String findByColor(@RequestParam(required = false) String color, Model model) {
        System.out.println("color = " + color);
        if(color != null) {
//            Car car = null; //carRepository.carByColor(color);
//            Car probe = new Car();
//            probe.setColor(color);
//            ExampleMatcher colorMatcher = matching().withMatcher("color", contains());
//            car = carRepository.findOne(Example.of(probe, colorMatcher)).orElse(null);
            model.addAttribute("car", carRepository.findCarByColorContains(color));
        }
        return "one";
    }
}
