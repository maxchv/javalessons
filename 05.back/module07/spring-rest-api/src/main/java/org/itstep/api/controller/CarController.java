package org.itstep.api.controller;

import org.itstep.domain.entity.Car;
import org.itstep.repository.CarRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

//@ResponseBody
//@Controller
@RestController
@RequestMapping("/api/v1/cars")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class CarController {

    private final CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Car>> allCars() {
        return ResponseEntity.ok(carRepository.findAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Car> carById(@PathVariable int id) {
        Car car = carRepository.findById(id).orElse(null);
        if (car != null) {
            return ResponseEntity.ok(car); // 200
        } else {
            return ResponseEntity.notFound().build(); // 404
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody Car car) {
        try {
            car = carRepository.save(car);
            return ResponseEntity.created(URI.create("/api/v1/cars/" + car.getId())).body(car); // 201
        } catch (Throwable ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getLocalizedMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable int id, @RequestBody Car car) {

        try {
            car = carRepository.saveAndFlush(car);
            return ResponseEntity.ok(car); // 200
        } catch (Throwable ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        try {
            carRepository.deleteById(id);
            return ResponseEntity.noContent().build(); // 204
        } catch (Throwable ex) {
            return ResponseEntity.notFound().build(); // 404
        }
    }

}
