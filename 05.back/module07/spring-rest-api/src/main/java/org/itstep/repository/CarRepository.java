package org.itstep.repository;

import org.itstep.domain.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CarRepository extends JpaRepository<Car, Integer> {
//    @Query(value = "SELECT * from Car where color=?0", nativeQuery = true)
    @Query("from Car c where c.color like :color")
    Car carByColor(@Param("color") String color);

    Car findCarByColorContains(String color);
}
