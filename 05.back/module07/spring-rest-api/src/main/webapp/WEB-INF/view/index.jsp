<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <style>
        label {
            display: inline-block;
            width: 150px;
        }

        .error {
            color: red;
        }
    </style>
</head>
<body>
<h1>REST API</h1>

<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Model</th>
        <th>Color</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    <tr>
        <td></td>
        <td><input id="model" type="text" placeholder="Model"/></td>
        <td><input id="color" type="color" placeholder="Color"/></td>
        <td><input id="price" type="number" min="0" step="0.01" placeholder="Price"/></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">
            <button id="add">Add</button>
        </td>
    </tr>
    </tfoot>
</table>

<%--<div>--%>
<%--    <button id="load">Load</button>--%>
<%--</div>--%>

<%--<div>--%>
<%--    <div>--%>
<%--        <label for="model">Model</label>--%>
<%--        <input name="model" id="model">--%>
<%--    </div>--%>
<%--    <div>--%>
<%--        <label for="color">Color</label>--%>
<%--        <input name="color" id="color" type="color">--%>
<%--    </div>--%>
<%--    <div>--%>
<%--        <label for="price">Price</label>--%>
<%--        <input name="price" id="price" type="number" min="0" step="0.01">--%>
<%--    </div>--%>
<%--    <button id="save">Save</button>--%>
<%--</div>--%>

<script>
    function displayCars(cars) {
        console.log(cars);
        const tbody = document.querySelector('tbody');
        tbody.innerHTML = '';
        cars.forEach(car => {
            let tr = document.createElement('tr');
            for (const prop in car) {
                let td = document.createElement('td');
                td.append(document.createTextNode(car[prop]));
                tr.append(td);
            }
            tbody.append(tr);
        });
    }

    const baseUrl = "http://localhost:8080/api/v1/cars";
    //document.querySelector("#load").addEventListener('click', event => {
    fetch(baseUrl)
        .then(response => response.json())
        .then(json => {
            displayCars(json);
        });

    //})

    function addCar(car) {
        let tr = document.createElement('tr');
        for (const prop in car) {
            let td = document.createElement('td');
            td.append(document.createTextNode(car[prop]));
            tr.append(td);
        }
        document.querySelector('tbody').append(tr);
    }

    document.querySelector("#add")
        .addEventListener('click', event => {
            let modelInput = document.querySelector('#model');
            let colorInput = document.querySelector('#color');
            let priceInput = document.querySelector('#price');
            const car = {
                model: modelInput.value,
                color: colorInput.value,
                price: priceInput.value
            };
            fetch(baseUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(car)
            }).then(response => {
                modelInput.value = priceInput.value = colorInput.value = '';
                return response.json();
            }).then(addCar);
        });

</script>

</body>
</html>
