import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class VideoStreamer {
    public static void main(String[] args) {
        try {
            Robot robot = new Robot();
            String format = "jpg";
            String fileName = "screenshot." + format;

            // получить скриншот
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);

            // сохраняем в файл (для примера)
            ImageIO.write(screenFullImage, format, new File(fileName));
            System.out.println("Сохранен!");

            // получаем мессив байт для отравки по сети
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(screenFullImage, format, out);

            byte[] bytes = out.toByteArray();
            System.out.println("Image size: " + bytes.length);
            out.close();

            // TODO: здесь код для отправки изображения по сети

        } catch (AWTException | IOException ex) {
            System.err.println(ex);
        }
    }
}
