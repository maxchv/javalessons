import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class VideoClient {

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    public static void main(String[] args) throws IOException {
        // Создаем окно нужного размера
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(WIDTH, HEIGHT);

        // Добавляем метку
        JLabel lbl = new JLabel();
        // Считываем изображение из файла
        // TODO: считать из сети используя перегруженный конструктор ImageIcon
        ImageIcon icon = new ImageIcon("screenshot.jpg");
        int h = icon.getIconHeight();
        int w = icon.getIconWidth();
        float scale = (float)WIDTH/w; // масштабируем
        icon = new ImageIcon(icon.getImage().getScaledInstance((int) (w*scale), (int) (h*scale), Image.SCALE_SMOOTH));
        lbl.setIcon(icon); // устанавливаем изображение для метки

        // Добавляем метку в окно
        frame.add(lbl);
        frame.setVisible(true);
        // Зкарывать окно при выходе
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
