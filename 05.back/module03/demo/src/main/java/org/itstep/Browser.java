package org.itstep;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Base64;

public class Browser {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket();
        String encoded = Base64.getEncoder().encodeToString(new byte[]{1,2,3,4});
        System.out.println(encoded);
        socket.connect(new InetSocketAddress("localhost", 80));
        System.out.println("Connected");
        String request = "GET / HTTP/1.1\r\n" +    // Строка запроса
                         "Host: localhost\r\n" +   // Заголовки запроса
                         "User-Agent: Console-Browser. It is not Mozilla\r\n" +
                         "Connection: close\r\n" + //
                         "\r\n";                   // Пустая строка - признак окончания заголовков
        try(OutputStream out = socket.getOutputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            out.write(request.getBytes());
            String line;
            System.out.println("Response:");
            /*
            HTTP/1.1 200 OK
            Date: Tue, 10 Nov 2020 18:22:14 GMT
            Server: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.4.4
            X-Powered-By: PHP/7.4.4
            Content-Length: 18
            Connection: close
            Content-Type: text/html; charset=UTF-8

            <h1>Hello Web</h1>
             */
            while((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
        socket.close();
    }
}
