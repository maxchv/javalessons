package org.itstep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.service(req, resp);
        log("Invoked");

        if ("GET".equals(req.getMethod())) {
            PrintWriter writer = resp.getWriter();
            writer.println("<h1>Hello World</h1>");
            String url = String.format("%s://%s:%s%s", req.getScheme(), req.getServerName(), req.getServerPort(), req.getContextPath());
            writer.println("<p><a href='" + url + "/about'>About</a></p>");
        }
    }
}
