package org.itstep;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
//import java.util.concurrent.atomic.AtomicInteger;

public class HomeServlet extends HttpServlet {

//    private AtomicInteger counter  = new AtomicInteger();

    @Override
    public void init(ServletConfig config) {
        System.out.println("Init HomeServlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet() method invoked");
        // Заголовки ответа
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");

        PrintWriter writer = resp.getWriter();
//        writer.println("Count: " + counter.addAndGet(1));
//        writer.println("</br>");
//        writer.println("Thread: "+ Thread.currentThread().getName());
        String login = req.getParameter("login");
        if(login != null) {
            writer.println("Login: " + login);
        }
        writer.println("<form method='post'>" +
                "<label>Name: <input name='login' /></label>" +
                "<input type='submit'/>" +
                "</form>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost() method invoked");
        doGet(req, resp);
//        resp.setContentType("text/html");
//        resp.setCharacterEncoding("utf-8");
//        PrintWriter writer = resp.getWriter();
////        writer.println("Count: " + counter.addAndGet(1));
////        writer.println("</br>");
////        writer.println("Thread: "+ Thread.currentThread().getName());
//        String login = req.getParameter("login");
//        if(login != null) {
//            writer.println("Your login: " + login);
//        }
    }

    @Override
    public void destroy() {
        System.out.println("Destroy HomeServlet");
    }
}
