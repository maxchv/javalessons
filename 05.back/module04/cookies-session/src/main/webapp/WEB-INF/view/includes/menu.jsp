<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<menu>
    <li><a href="<%=request.getContextPath()%>/secret">Admin page</a></li>
    <% if(request.getAttribute("admin") == null)  {%>
        <li><a href="<%=request.getContextPath()%>/login">Login</a></li>
    <% } else { %>
        <li><a href="<%=request.getContextPath()%>/logout">Logout</a></li>
    <% } %>
</menu>

