package org.itstep;

import org.itstep.security.SecurityUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = LogoutServlet.PATH)
public class LogoutServlet extends BaseServlet {
    public static final String PATH = "/logout";

    @Override
    public String getViewName() {
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SecurityUtils.logout(req, resp);
        resp.sendRedirect(req.getContextPath() + SecretServlet.PATH);
    }
}
