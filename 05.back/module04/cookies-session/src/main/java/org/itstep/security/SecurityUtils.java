package org.itstep.security;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class SecurityUtils {

    public static final String LOGIN_PARAMETER = "login";
    public static final String PASSWORD_PARAMETER = "password";
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "123";

    public static boolean login(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter(LOGIN_PARAMETER) != null && req.getParameter(PASSWORD_PARAMETER) != null) {
            if (LOGIN.equals(req.getParameter(LOGIN_PARAMETER)) && PASSWORD.equals(req.getParameter(PASSWORD_PARAMETER))) {
                // Response with header 'Set-Cookie: admin=true; HttpOnly'
                Cookie cookie = new Cookie("admin", "true");
                cookie.setMaxAge(3600 * 24 * 30);
                cookie.setHttpOnly(true);
                resp.addCookie(cookie);
                return true;
            }
        }
        return false;
    }

    public static boolean logout(HttpServletRequest req, HttpServletResponse resp) {
        if (isAuthorized(req)) {
            Cookie cookie = new Cookie("admin", "");
            cookie.setValue("");
            cookie.setMaxAge(0);
            resp.addCookie(cookie);
            return true;
        }
        return false;
    }

    public static boolean isAuthorized(HttpServletRequest req) {
        return Arrays.stream(req.getCookies())
                .anyMatch(c -> "admin".equals(c.getName()) && "true".equals(c.getValue()));
    }
}
