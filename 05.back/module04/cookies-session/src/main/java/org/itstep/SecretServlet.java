package org.itstep;

import org.itstep.security.SecurityUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = SecretServlet.PATH)
public class SecretServlet extends BaseServlet {
    public static final String PATH = "/secret";

    @Override
    public String getViewName() {
        return "secret.jsp";
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(SecurityUtils.isAuthorized(req)) {
            super.doGet(req, resp);
        } else {
            // response with code 302
            resp.sendRedirect(req.getContextPath() + LoginServlet.PATH);
        }
    }
}
