package org.itstep;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = HomeServlet.PATH)
public class HomeServlet extends BaseServlet {
    public static final String PATH = "/";

    @Override
    public String getViewName() {
        return "index.jsp";
    }
}
