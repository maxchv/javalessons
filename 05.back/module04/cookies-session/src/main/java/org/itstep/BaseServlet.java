package org.itstep;

import org.itstep.security.SecurityUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {
    private String prefix;
    private String username;
    private String password;
    private String url;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServletContext servletContext = config.getServletContext();
        prefix = servletContext.getInitParameter("prefix");
        username = servletContext.getInitParameter("username");
        password = servletContext.getInitParameter("password");
        url = servletContext.getInitParameter("url");
    }

    public abstract String getViewName();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(SecurityUtils.isAuthorized(req)) {
            req.setAttribute("admin", true);
        }
        req.getRequestDispatcher(prefix + getViewName())
                .forward(req, resp);
    }
}
