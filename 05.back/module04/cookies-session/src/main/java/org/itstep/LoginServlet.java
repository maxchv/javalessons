package org.itstep;

import org.itstep.security.SecurityUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = LoginServlet.PATH)
public class LoginServlet extends BaseServlet {

    public static final String PATH = "/login";

    @Override
    public String getViewName() {
        return "login.jsp";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(SecurityUtils.isAuthorized(req) || SecurityUtils.login(req, resp)) {
            resp.sendRedirect(req.getContextPath() + SecretServlet.PATH);
        } else {
            resp.sendRedirect(req.getContextPath() + LoginServlet.PATH);
        }
    }
}
