package org.itstep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet(urlPatterns = "/")
public class HomeServlet extends HttpServlet {
    //private final List<String> tasks = new CopyOnWriteArrayList<>();

    private static final Logger logger = LoggerFactory.getLogger(HttpServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET Request to servlet");
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        //req.setAttribute("tasks", tasks);
        HttpSession session = req.getSession();
        List<String> tasks = new ArrayList<>();
        if(session.isNew() || session.getAttribute("tasks") == null) {
            session.setAttribute("tasks", tasks);
        }

        req.getRequestDispatcher( "/WEB-INF/pages/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST Request to servlet");
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        if(req.getParameter("task") != null) {
            HttpSession session = req.getSession();
            List<String> tasks = (List<String>) session.getAttribute("tasks");
            tasks.add(req.getParameter("task"));
            //tasks.add(req.getParameter("task"));
        }
        //req.setAttribute("tasks", tasks);
        resp.sendRedirect(getServletContext().getContextPath());
    }
}
