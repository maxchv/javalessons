<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%!
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
%>
<%= request.getAttribute("time")%>
<form method="post">
    Задача: <input name="task"> <input type="submit">
</form>

<table>
    <%
        if (session.getAttribute("tasks") != null) {
            for (String row : (List<String>) session.getAttribute("tasks")) {
                out.println("<tr>");
                out.println("<td>" + row + "</td>");
                out.println("</tr>");
            }
        } else {
            out.println("<tr><td>Tasks not found</td></tr>");
        }
    %>
</table>

<div>
<p>Session Id: <%=session.getId()%> </p>
<p>Session is New?: <%=session.isNew()%> </p>
<p>Session created: <%=dateFormat.format(new Date(session.getCreationTime()))%> </p>
<p>Session last access: <%=dateFormat.format(new Date(session.getLastAccessedTime()))%> </p>
</div>
</body>
</html>
