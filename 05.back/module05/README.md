# Module 5. JavaServer Pages

1. What is JSP?
2. Objectives and tasks of JSP.
3. Origin of JSP.
4. The concept of directive.
5. Error handling in JSP.
6. JSP and Model View Controller.
7. Expression Language in JSP.
8. JavaBean.
9. Java Standard Tag Library.
10. Different types of Tags.
11. The use of Conditional Tags.
12. The use of Iteration Tags.
13. The example use of different Tags.
14. What is Custom Tags?
15. What is Tag Files?
16. What is JSP Fragment?
17. The examples use.
