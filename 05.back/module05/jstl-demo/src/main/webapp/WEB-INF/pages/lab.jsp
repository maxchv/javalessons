<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form method="post">
    <label>Value <input name="value" value="${param.value}"/></label>
    <select name="scope">
        <c:choose>
            <c:when test="${param.scope == 'applicationScope'}">
                <option selected>applicationScope</option>
            </c:when>
            <c:otherwise>
                <option>applicationScope</option>
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${param.scope == 'sessionScope'}">
                <option selected>sessionScope</option>
            </c:when>
            <c:otherwise>
                <option>sessionScope</option>
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${param.scope == 'requestScope'}">
                <option selected>requestScope</option>
            </c:when>
            <c:otherwise>
                <option>requestScope</option>
            </c:otherwise>
        </c:choose>
    </select>
    <label>
        <c:choose>
            <c:when test="${param.remove != null}">
                Remove: <input type="checkbox" checked name="remove" />
            </c:when>
            <c:otherwise>
                Remove: <input type="checkbox" name="remove" />
            </c:otherwise>
        </c:choose>
    </label>
    <br/>
    <input type="submit">
</form>
<p>Default: ${value}</p>
<p>Application: ${applicationScope.value}</p>
<p>Session: ${sessionScope.value}</p>
<p>Request: ${requestScope.value}</p>

</body>
</html>
