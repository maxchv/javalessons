<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Title</title>
    <script src="https://cdn.tiny.cloud/1/koolxfdub4sj7wf52rr59pgggri7b8sk2kvi9tf352z3b1yf/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
</head>
<body>

<form method="post" enctype="multipart/form-data">
    <label>Название:<br/><input name="title"></label>
    <br/>
    <label>Описание:<br/><textarea name="description" id="description"></textarea></label>
    <br/>
    <label>Файл: <input name="file" type="file"/></label>
    <br/>
    <input type="submit" value="Сохранить">
</form>

<%--enctype="application/x-www-form-urlencoded"--%>
<%--title=%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0&description=%D0%9C%D0%BE%D0%B5%20%D0%BE%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5--%>
<%--title=%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0&description=%3Cp%3E%D0%9C%D0%BE%D0%B5+%D0%BE%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%3C%2Fp%3E--%>

<table border="1">
    <%--@elvariable id="tasks" type="java.util.List<org.itstep.domain.entity.Task>"--%>
    <c:forEach items="${tasks}" var="task">
        <tr>
            <td>
                    ${task.id}
            </td>
            <td>
                    ${task.title}
            </td>
            <td>
                    ${task.description}
            </td>
            <td>
                <c:if test="${task.img != null}">
                    <a href="<%=request.getContextPath()+"/resources/uploads/"%>${task.img}" download="${task.img}">Загрузить</a>
                </c:if>
            </td>
            <td>
                <c:url value="index.jsp" var="link">
                    <c:param name="delete" value="${task.id}"/>
                </c:url>
                <a href="${link}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<script>
    tinymce.init({
        selector: '#description',
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
    });
</script>
</body>
</html>
