package org.itstep.listener;

import org.itstep.DynamicServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;

public class GlobalListener implements ServletContextListener {

    public  static final Logger logger = LoggerFactory.getLogger(GlobalListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("Init GlobalListener");
        ServletContext servletContext = sce.getServletContext();
        try {
            DynamicServlet servlet = servletContext.createServlet(DynamicServlet.class);
            ServletRegistration.Dynamic dynamic = servletContext.addServlet("dynamic", servlet);
            dynamic.addMapping("/dynamic");
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
