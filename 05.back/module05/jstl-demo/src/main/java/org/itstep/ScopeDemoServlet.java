package org.itstep;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/demo")
public class ScopeDemoServlet extends HomeServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/lab.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String value = req.getParameter("value");
        String scope = req.getParameter("scope");
        String remove = req.getParameter("remove");
        if(scope != null) {
            if(remove == null) {
                switch (scope) {
                    case "applicationScope":
                        getServletContext().setAttribute("value", value);
                        break;
                    case "sessionScope":
                        req.getSession().setAttribute("value", value);
                        break;
                    case "requestScope":
                        req.setAttribute("value", value);
                        break;
                }
            } else {
                switch (scope) {
                    case "applicationScope":
                        getServletContext().removeAttribute("value");
                        break;
                    case "sessionScope":
                        req.getSession().removeAttribute("value");
                        break;
                    case "requestScope":
                        req.removeAttribute("value");
                        break;
                }
            }
        }
        req.getRequestDispatcher("/WEB-INF/pages/lab.jsp").forward(req, resp);
    }
}
