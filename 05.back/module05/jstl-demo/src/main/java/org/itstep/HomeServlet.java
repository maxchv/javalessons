package org.itstep;

import org.itstep.domain.entity.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@MultipartConfig
@WebServlet(urlPatterns = "/", loadOnStartup = 1)
public class HomeServlet extends HttpServlet {
    //private final List<String> tasks = new CopyOnWriteArrayList<>();

    private static final Logger logger = LoggerFactory.getLogger(HttpServlet.class);
    public static final String UPLOADS = "resources/uploads";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // applicationScope
        getServletContext().setAttribute("key", "value");
        // sessionScope
        req.getSession().setAttribute("key", "value");
        // requestScope
        req.setAttribute("key", "value");

        logger.info("GET Request to servlet");
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        //req.setAttribute("tasks", tasks);
        HttpSession session = req.getSession();
        if(session.isNew() || session.getAttribute("tasks") == null) {
            session.setAttribute("tasks", new ArrayList<Task>());
        }

        req.getRequestDispatcher( "/WEB-INF/pages/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST Request to servlet");
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        if(req.getParameter("title") != null) {
            Optional<String> file = saveFile(req.getPart("file"));
            HttpSession session = req.getSession();
            List<Task> tasks = (List<Task>) session.getAttribute("tasks");
            if(tasks != null) {
                Task task = new Task(req.getParameter("title"), req.getParameter("description"));
                file.ifPresent(task::setImg);
                tasks.add(task);
            }
        }
        resp.sendRedirect(getServletContext().getContextPath());
    }

    private Optional<String> saveFile(Part part) throws IOException {
        // Content-Disposition: form-data; name="file"; filename="controls.png"
        if(part == null) return Optional.empty();
        String contentDisposition = part.getHeader("Content-Disposition");
        int start = contentDisposition.indexOf("filename=");
        start += "filename=".length();
        int end = contentDisposition.lastIndexOf("\"");
        String filename = contentDisposition.substring(start + 1, end);
        logger.info("filename: {}", filename);
        String uploadsDirUrl = getServletContext().getRealPath(UPLOADS);
        String absolutePathToFile = uploadsDirUrl + "/" + filename;
        logger.info("File path: {}", absolutePathToFile);
        part.write(absolutePathToFile);
        return Optional.of(filename);
    }
}
