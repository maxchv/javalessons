package org.itstep.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalTime;

@WebFilter(urlPatterns = "*")
public class CurrentTimeFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(CurrentTimeFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Init filter " + filterConfig.getFilterName());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("Filter start");
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        request.setAttribute("time", LocalTime.now().toString());
        chain.doFilter(request, response);
        log.info("Filter end");
    }

    @Override
    public void destroy() {
        log.info("Destroy filter");
    }
}
