<%-- Это директива! --%>
<!-- html комментарий -->
<%@ page pageEncoding="utf-8" contentType="text/html;utf-8" import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head>
    <title>Hello JSP</title>
    <meta charset="utf-8"/>
    <style>
        <jsp:include page="css/style.css"/>
    </style>
    <%--    <link href="css/style.css" rel="stylesheet"/>--%>
</head>
<body>
<%!
    // Блок объявления переменных
    int count = 0;

    static class Test {

    }
%>

<%
    String title = "It is simple jsp file";
%>
<%@include file="include/header.jsp" %>
<h2>Hello World!</h2>
<ul>
    <%
        // Скриплеты - способ добавить java в "html"
        String parameter = request.getParameter("count");
        int count = 10;
        if (parameter != null) {
            count = Integer.parseInt(parameter);
        }
        for (int i = 0; i < count; i++) {
            out.println("<li> item" + (i + 1) + "</li>");
        }
    %>
</ul>

<p>Current date:
    <%
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy HH:mm");
        out.println(dateFormat.format(date));
    %>
</p>

<p>Counter: <%= this.count++ %></p>

</body>
</html>
