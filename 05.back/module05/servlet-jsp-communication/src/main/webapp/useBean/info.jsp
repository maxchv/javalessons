<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>User info</h1>
<jsp:useBean id="user" class="org.itstep.domain.User" scope="session"/>
<p>
    Login: <jsp:getProperty name="user" property="login"/>
</p>
<p>
    Password: <jsp:getProperty name="user" property="password"/>
</p>
</body>
</html>
