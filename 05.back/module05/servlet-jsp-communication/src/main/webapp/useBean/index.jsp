<%@ page import="org.itstep.domain.User" %>
<%@page pageEncoding="UTF-8" %>
<html>
<body>
<h2>Hello World!</h2>
<%--<%--%>
<%--    User user = new User();--%>
<%--    user.setLogin("admin");--%>
<%--    user.setPassword("qwerty");--%>
<%--%>--%>
<jsp:useBean id="user" class="org.itstep.domain.User" scope="session"/>
<% if ("GET".equals(request.getMethod())) { %>
<form method="post">
    <label>Login: <input name="login"></label>
    <label>Password: <input name="password" type="password"></label>
    <input type="submit">
</form>
<% } %>

<% if ("POST".equals(request.getMethod())) { %>

<jsp:setProperty name="user" property="login" param="login"/>
<jsp:setProperty name="user" property="password" param="password"/>

<p>
    <a href="info.jsp">Info</a>
</p>
<% } %>
</body>
</html>
