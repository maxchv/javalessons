package org.itstep;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;

@WebServlet(urlPatterns = "/")
public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("HomeServlet doGet()");

        req.setAttribute("title", "Current time: " + LocalTime.now());

//        req.getRequestDispatcher("/WEB-INF/pages/header.jsp").include(req,resp);
//        req.getRequestDispatcher("/WEB-INF/pages/index.jsp").include(req,resp);
        req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req,resp);
    }
}
