# Development of Back-End Solutions Using Java

Version 2.0.0

Course duration: 58 double-classes

##Course objective

To teach them to pick appropriate mechanisms and structures to solve
a particular real-world problem.

Upon the completion of the course, the student will be able to:

* understand fundamentals of creating back-end solutions using Java;
* create, compile, and debug web applications;
* interact with data sources;
* use network mechanisms;
* create servlets;
* understand and apply the MVC pattern;
* create JSP solutions;
* use Spring framework;
* use Hibernate library.

Upon the completion of this course, the student submits practical
task and takes test of knowledge on course materials. In order to
be admitted for the test, all home tasks and practical tasks must be
submitted. Practical task must cover a maximum of the material from
various sections of the course.

## Topic Plan

Module 1. Introduction to network technologies. 2 double-classes

Module 2. Network interaction. 4 double-classes

Module 3. Introduction to developing back-end solutions using Java. 6 double-classes

Module 4. Interaction with data sources. 6 double-classes

Module 5. JavaServer Pages, Tags in JSP. 8 double-classes

Module 6. Introduction to Spring. 20 double-classes

Module 7. Introduction to Hibernate, Spring Data. 10 double-classes

Module 8. Exam. 2 double-classes