package org.itstep.multicast;

import java.io.IOException;
import java.net.*;

public class Server {
    public static void main(String[] args) throws IOException {

        var server = new MulticastSocket(Client.PORT); //new DatagramSocket(10_001);
        var mcastaddr = InetAddress.getByName(Client.MULTICAST_ADDR);
        InetSocketAddress mcSocketAddr = new InetSocketAddress(mcastaddr, Client.PORT);
        server.joinGroup(mcSocketAddr, NetworkInterface.getByIndex(0));
        byte[] buff = new byte[100];
        while(true) {
            var packet = new DatagramPacket(buff, buff.length);
            server.receive(packet);
            System.out.println("Receive from: " + packet.getAddress());
            System.out.println("Server time: "
            + new String(buff, 0, packet.getLength()));
        }
        //server.leaveGroup(mcSocketAddr, NetworkInterface.getByIndex(0));
    }
}
