package org.itstep.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    static class Worker implements Runnable {

        private final Socket socket;

        public Worker(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (var printStream = new PrintStream(socket.getOutputStream());
                 var inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {
                // Считывание данных
                var input = inputStream.readLine();
                log("Request from " + socket.getRemoteSocketAddress() + ": ", input);
                printStream.println(input);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket();

        ExecutorService executor = Executors.newCachedThreadPool();

        server.bind(new InetSocketAddress(10_000), 10);

        while (true) {
            log("Wait for clients...");

            var remoteClient = server.accept();

            log("Connected: " + remoteClient.getRemoteSocketAddress());

            executor.submit(new Worker(remoteClient));

//            var printStream = new PrintStream(remoteClient.getOutputStream());
//            var inputStream = new BufferedReader(new InputStreamReader(remoteClient.getInputStream()));
//
//            // Считывание данных
//            var input = inputStream.readLine();
//
//            log("Request from client: ", input);
//
//            printStream.println(input);
//
//            inputStream.close();
//            printStream.close();
//
//            remoteClient.close();

//            if ("exit".equals(input)) {
//                log("Shutdown server");
//                server.close();
//                break;
//            }
        }
    }

    static void log(String... messages) {
        System.out.println("[" + LocalDateTime.now() +
                "] " + String.join("", messages));
    }
}
