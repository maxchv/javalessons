package org.itstep.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket client = new Socket();

        client.connect(new InetSocketAddress(InetAddress.getLocalHost(), 10_000));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Your message: ");
        String msg = scanner.nextLine();

        PrintStream printStream = new PrintStream(client.getOutputStream());
        BufferedReader inputStream =
                new BufferedReader(new InputStreamReader(client.getInputStream()));

        printStream.println(msg);
        printStream.flush();
        System.out.println("Response: " + inputStream.readLine());

        printStream.close();
        inputStream.close();
        client.close();

    }
}
