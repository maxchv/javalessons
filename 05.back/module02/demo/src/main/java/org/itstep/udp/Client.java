package org.itstep.udp;

import java.io.IOException;
import java.net.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        DatagramSocket client = new DatagramSocket();
        System.out.println("Start time server");
        while(true) {
            LocalTime currentTime = LocalTime.now();
            System.out.println("Now: " + currentTime);
            var now = currentTime.toString().getBytes();
            client.send(new DatagramPacket(now, now.length,
                    new InetSocketAddress("192.168.56.255", 10_001)));
            Thread.sleep(1000);
        }
    }
}
