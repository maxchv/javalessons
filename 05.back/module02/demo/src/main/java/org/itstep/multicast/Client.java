package org.itstep.multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.time.LocalTime;

public class Client {

    public static final String MULTICAST_ADDR = "230.0.0.1";
    public static final int PORT = 10_001;

    public static void main(String[] args) throws IOException, InterruptedException {
        DatagramSocket client = new DatagramSocket();
        System.out.println("Start time server");
        while(true) {
            LocalTime currentTime = LocalTime.now();
            System.out.println("Now: " + currentTime);
            var now = currentTime.toString().getBytes();
            client.send(new DatagramPacket(now, now.length, new InetSocketAddress(MULTICAST_ADDR, PORT)));
            Thread.sleep(1000);
        }
    }
}
