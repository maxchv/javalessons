package org.itstep.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Server {
    public static void main(String[] args) throws IOException {

        var server = new DatagramSocket(10_001);
        byte[] buff = new byte[100];
        while(true) {
            var packet = new DatagramPacket(buff, buff.length);
            server.receive(packet);
            System.out.println("Receive from: " + packet.getAddress());
            System.out.println("Server time: "
            + new String(buff, 0, packet.getLength()));
        }
    }
}
