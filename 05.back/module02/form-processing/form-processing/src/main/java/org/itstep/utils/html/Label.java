package org.itstep.utils.html;

public class Label extends Tag {
    public Label() {
        super("label");
    }

    public Label(String text) {
        this();
        text(text);
    }
}
