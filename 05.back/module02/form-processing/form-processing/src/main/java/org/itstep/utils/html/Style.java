package org.itstep.utils.html;

public class Style extends Tag {
    public Style() {
        super("style");
    }

    public Style(String text) {
        this();
        text(text);
    }
}
