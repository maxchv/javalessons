package org.itstep.utils.html;

public interface Node {
    String render();
}
