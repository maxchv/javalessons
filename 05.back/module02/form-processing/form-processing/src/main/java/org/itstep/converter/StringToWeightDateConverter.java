package org.itstep.converter;

import org.itstep.domain.WeightData;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringToWeightDateConverter {
    private static final SimpleDateFormat dateFormat;

    static {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    public static WeightData parse(String date, String weight) throws ParseException {
        return new WeightData(dateFormat.parse(date), Float.parseFloat(weight));
    }
}
