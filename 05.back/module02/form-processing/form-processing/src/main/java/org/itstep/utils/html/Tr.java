package org.itstep.utils.html;

public class Tr extends Tag {
    public Tr() {
        super("tr");
    }

    public Tr th(Tag th) {
        add(th);
        return this;
    }
}
