package org.itstep.utils.html;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Tag implements Node {
    private final String name;
    private final Map<String, String> attrs = new HashMap<>();
    private final List<Node> children = new ArrayList<>();
    private String text = "";

    public Tag(String name) {
        this.name = name;
    }

    public Tag attr(String name, String value) {
        attrs.put(name, value);
        return this;
    }

    public Tag add(Node... tag) {
        children.addAll(Arrays.stream(tag).collect(Collectors.toList()));
        return this;
    }

    public String render() {
        return "<"
                + name
                + " "
                + attrs.entrySet().stream().map(attr -> attr.getKey() + "='" + attr.getValue() + "' ").collect(Collectors.joining())
                + ">"
                + (children.size() > 0 ? children.stream().map(Node::render).collect(Collectors.joining()) : "")
                + " "
                + text
                + "</" + name + ">";
    }
}
