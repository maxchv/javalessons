package org.itstep.utils.html;

public class H1 extends Tag {
    public H1() {
        super("h1");
    }

    public H1(String text) {
        this();
        text(text);
    }
}
