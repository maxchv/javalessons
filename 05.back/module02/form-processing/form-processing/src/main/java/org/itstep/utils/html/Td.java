package org.itstep.utils.html;

public class Td extends Tag {
    public Td() {
        super("td");
    }

    public Td(String text) {
        this();
        text(text);
    }
}
