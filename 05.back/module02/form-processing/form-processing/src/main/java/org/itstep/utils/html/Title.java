package org.itstep.utils.html;

public class Title extends Tag {
    public Title() {
        super("title");
    }

    public Title(String text) {
        this();
        text(text);
    }
}
