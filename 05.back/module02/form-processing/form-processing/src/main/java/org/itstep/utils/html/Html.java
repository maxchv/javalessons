package org.itstep.utils.html;

public class Html extends Tag {
    public Html() {
        super("html");
    }

    public Html head(Tag head) {
        add(head);
        return this;
    }

    public Html body(Tag body) {
        add(body);
        return this;
    }

    @Override
    public String render() {
        return "<!DOCTYPE html>\n" + super.render();
    }
}
