package org.itstep.utils.html;

public class Th extends Tag {
    public Th() {
        super("th");
    }

    public Th(String text) {
        this();
        text(text);
    }
}
