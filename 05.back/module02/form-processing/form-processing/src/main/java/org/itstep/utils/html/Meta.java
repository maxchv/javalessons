package org.itstep.utils.html;

public class Meta extends Tag {
    public Meta() {
        super("meta");
    }

    public Tag charset(String charset) {
        attr("charset", charset);
        return this;
    }
}
