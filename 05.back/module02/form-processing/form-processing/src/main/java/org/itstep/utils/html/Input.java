package org.itstep.utils.html;

public class Input extends Tag {
    public Input() {
        super("input");
    }

    @Override
    public String render() {
        String string = super.render();
        int i = string.indexOf("</input>");
        return string.substring(0, i);
    }
}
