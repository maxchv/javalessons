package org.itstep.utils.html;

public class Table extends Tag {
    public Table() {
        super("table");
    }

    public Table thead(Tag thead) {
        add(thead);
        return this;
    }

    public Table tbody(Tag tbody) {
        add(tbody);
        return this;
    }
}
