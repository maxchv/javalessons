package org.itstep;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.itstep.converter.StringToWeightDateConverter;
import org.itstep.domain.WeightData;
import org.itstep.utils.html.Tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import static org.itstep.utils.html.Tags.*;

public class HomeServlet extends HttpServlet {

//    @Override
//    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        if("GET".equals(req.getMethod())) {
//            // process get request
//        } else if("POST".equals(req.getMethod())) {
//            // process post request
//        }
//        // ...
//    }

    private final List<WeightData> weightDataList = new CopyOnWriteArrayList<>();
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("It is GET request!");
        // set headers
        resp.setContentType("text/html");   // Content-Type: text/html; utf-8
        resp.setCharacterEncoding("UTF-8");

        // set body
        printFormUsingObject(resp);
    }

    private void printFormUsingObject(HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        String css = "h1 { text-align: center; }\n" +
                "form { border: 1px solid black; width: 350px; margin: 20px auto; padding: 20px; border-radius: 10px }\n" +
                ".control-group { display: flex; flex-wrap: nowrap; margin-bottom: 10px; }\n" +
                "label { display: block; width: 200px; }\n" +
                "table { width: 100%; border-collapse: collapse; border: 1px solid black; }\n" +
                "td, th { border: 1px solid black; }";

        writer.println(
                html()
                        .head(
                                head()
                                        .title(title()
                                                .add(text("Weight control application")))
                                        .meta(meta().charset("UTF-8"))
                                        .style(style()
                                                .add(text(css)))
                        )
                        .body(
                                body()
                                        .add(h1().add(text("Weight control application")))
                                        .add(main()
                                                .add(form().attr("method", "post")
                                                        .add(div().attr("class", "control-group")
                                                                .add(label().attr("for", "date")
                                                                        .add(text("Date")))
                                                                .add(input()
                                                                        .attr("id", "date")
                                                                        .attr("name", "date")
                                                                        .attr("type", "date")
                                                                        .attr("required", "required"))
                                                        )
                                                        .add(div().attr("class", "control-group")
                                                                .add(label().attr("for", "weight")
                                                                        .add(text("Weight")))
                                                                .add(input()
                                                                        .attr("id", "weight")
                                                                        .attr("name", "weight")
                                                                        .attr("type", "number")
                                                                        .attr("min", "0")
                                                                        .attr("required", "required"))
                                                        )
                                                        .add(input().attr("type", "submit"))
                                                )
                                                .add(table()
                                                        .thead(thead()
                                                                .add(tr()
                                                                        .th(th().add(text("Date")))
                                                                        .th(th()).add(text("Weight")))
                                                        )
                                                        .tbody(
                                                                weightDataList
                                                                        .stream()
                                                                        .map(wd -> tr()
                                                                                .add(td(dateFormatter.format(wd.getDate())))
                                                                                .add(td(String.valueOf(wd.getWeight()))))
                                                                        .reduce(tbody(), (Tag::add))
                                                        ))
                                        )
                        )
                        .render()
        );
    }

    private void printForm(HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println("<!DOCTYPE html>");
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>Weight control application</title>");
        writer.println("<meta charset='utf-8'/>");
        writer.println("<style>");
        writer.println(" h1 { text-align: center; }");
        writer.println(" form { border: 1px solid black; width: 350px; margin: 20px auto; padding: 20px; border-radius: 10px }");
        writer.println(" .control-group { display: flex; flex-wrap: nowrap; margin-bottom: 10px; }");
        writer.println(" label { display: block; width: 200px; }");
        writer.println(" table { width: 100%; border-collapse: collapse; border: 1px solid black; }");
        writer.println(" td, th { border: 1px solid black; }");
        writer.println("</style>");
        writer.println("</head>");
        writer.println("<body>");
        writer.println("<header>");
        writer.println("<h1>Weight control application</h1>");
        writer.println("</header>");
        writer.println("<main>");

        writer.println("<form method='post'>");
        writer.println("<div class='control-group'>");
        writer.println("<label for='date'>Select date</label>");
        writer.println("<input type='date' id='date' name='date' required />");
        writer.println("</div>");
        writer.println("<div class='control-group'>");
        writer.println("<label for='weight'>Set weight</label>");
        writer.println("<input type='number' min='0' id='weight' name='weight' required />");
        writer.println("</div>");
        writer.println("<input type='submit' />");
        writer.println("</form>");

        writer.println("<table>");
        writer.println("<thead>");
        writer.println("<tr>");
        writer.println("<th>Date</th>");
        writer.println("<th>Weight</th>");
        writer.println("</tr>");
        writer.println("</thead>");
        writer.println("<tbody>");
        writer.println(weightDataList.stream()
                .map(weightData -> String.format("<tr><td>%s</td><td>%s</td></tr>", dateFormatter.format(weightData.getDate()), weightData.getWeight()))
                .collect(Collectors.joining()));
        writer.println("</tbody>");
        writer.println("</table>");

        writer.println("</main>");
        writer.println("</body>");
        writer.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("It is POST request!");

        String date = req.getParameter("date");
        String weight = req.getParameter("weight");

        System.out.println("date = " + date);
        System.out.println("weight = " + weight);
        try {
            weightDataList.add(StringToWeightDateConverter.parse(date, weight));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        doGet(req, resp);
    }
}
