package org.itstep.utils.html;

public class Text implements Node {
    private final String text;

    public Text(String text) {
        this.text = text;
    }

    @Override
    public String render() {
        return text;
    }
}
