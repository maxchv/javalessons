package org.itstep.domain;

import java.util.Date;
import java.util.Objects;

public class WeightData {
    private final Date date;
    private final float weight;

    public WeightData(Date date, float weight) {
        this.date = date;
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeightData that = (WeightData) o;
        return Float.compare(that.weight, weight) == 0 && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, weight);
    }

    @Override
    public String toString() {
        return "WeightData{" +
                "date=" + date +
                ", weight=" + weight +
                '}';
    }
}
