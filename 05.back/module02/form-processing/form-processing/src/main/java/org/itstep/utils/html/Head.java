package org.itstep.utils.html;

public class Head extends Tag {
    public Head() {
        super("head");
    }

    public Head title(Tag title) {
        add(title);
        return this;
    }

    public Head meta(Tag title) {
        add(title);
        return this;
    }

    public Head style(Tag title) {
        add(title);
        return this;
    }
}
