package org.itstep.utils.html;

public class Tags {
    public static Html html() {
        return new Html();
    }

    public static Head head() {
        return new Head();
    }

    public static Main main() {
        return new Main();
    }

    public static Div div() {
        return new Div();
    }

    public static Input input() {
        return new Input();
    }

    public static Table table() {
        return new Table();
    }

    public static Thead thead() {
        return new Thead();
    }

    public static Tbody tbody() {
        return new Tbody();
    }

    public static Th th() {
        return new Th();
    }

    public static Tr tr() {
        return new Tr();
    }

    public static Td td(String text) {
        return new Td(text);
    }

    public static Title title() {
        return new Title();
    }

    public static Text text(String text) {
        return new Text(text);
    }

    public static Style style() {
        return new Style();
    }

    public static Meta meta() {
        return new Meta();
    }

    public static Form form() {
        return new Form();
    }

    public static Label label() {
        return new Label();
    }

    public static Body body() {
        return new Body();
    }

    public static H1 h1() {
        return new H1();
    }

    public static Header header() {
        return new Header();
    }
}
