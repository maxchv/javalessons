# Module 2. Network interaction

1. The overview of the java.net package.
2. The InetAddress class.
3. The Socket class.
4. The ServerSocket class.
5. The DatagramSocket class.
6. The DatagramPacket class.
7. Practical project. Creating a file server
