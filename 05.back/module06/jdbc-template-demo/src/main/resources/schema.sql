create table if not exists `groups`
(
    id int auto_increment
        primary key,
    name varchar(255) not null,
    constraint groups_name_uindex
        unique (name)
);

create table if not exists students
(
    id int auto_increment
        primary key,
    first_name varchar(100) not null,
    last_name varchar(255) not null,
    birth date null,
    group_id int null,
    constraint students_groups_id_fk
        foreign key (group_id) references `groups` (id)
);