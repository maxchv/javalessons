package org.itstep.domain.entity;

import lombok.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class Group {
    private int id;
    @NonNull
    private String name;
}
