package org.itstep;

import org.itstep.config.AppConfig;
import org.itstep.domain.dao.Dao;
import org.itstep.domain.entity.Group;
import org.itstep.domain.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class App {
    final Dao<Student> studentDao;

    @Autowired
    public App(Dao<Student> studentDao) {
        this.studentDao = studentDao;
    }

    public static void main(String[] args) {

        System.out.println(System.getenv("username"));
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        App bean = context.getBean(App.class);
        Student student = Student.builder()
                .firstName("Вася")
                .lastName("Пупкин")
                .birth(LocalDate.now())
                .group(new Group("Java01"))
                .build();
        Student student2 = Student.builder()
                .firstName("Маша")
                .lastName("Пупкина")
                .group(new Group("Java01"))
                .build();
        bean.studentDao.saveAll(new Student[]{student, student2});

        for (Student s : bean.studentDao.findAll()) {
            System.out.println(s);
        }
    }
}
