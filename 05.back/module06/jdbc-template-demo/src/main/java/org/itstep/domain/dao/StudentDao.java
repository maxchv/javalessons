package org.itstep.domain.dao;

import lombok.val;
import org.itstep.domain.entity.Group;
import org.itstep.domain.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Component
public class StudentDao implements Dao<Student> {
    private final JdbcTemplate jdbcTemplate;
    private final PlatformTransactionManager txManager;
    private final SimpleJdbcInsert simpleJdbcInsert;
    private final Dao<Group> groupDao;

    @Autowired
    public StudentDao(@Qualifier("dataSource") DataSource dataSource,
                      PlatformTransactionManager txManager, Dao<Group> groupDao) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert
                .withTableName("students")
                .usingColumns("first_name", "last_name", "birth", "group_id")
                .usingGeneratedKeyColumns("id");
        this.txManager = txManager;
        this.groupDao = groupDao;
    }

    @Override
    public boolean exists(Student data) {
        return false;
    }

    @Transactional
    @Override
    public void saveAll(Student... students) {
//        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
//        transactionDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
//        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
//        System.out.println(transactionDefinition);
//        TransactionStatus status = txManager.getTransaction(transactionDefinition);
////        System.out.println("New transaction: " + status.isNewTransaction());
////        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_NOT_SUPPORTED);
////        TransactionStatus status1 = txManager.getTransaction(transactionDefinition);
////        System.out.println("New transaction: " + status1.isNewTransaction());
//        try {
        Arrays.stream(students).forEach(this::save);
//            txManager.commit(status);
//        } catch (Exception ex) {
//            txManager.rollback(status);
//            ex.printStackTrace();
//        }
    }

    @Transactional
    @Override
    public Integer save(Student student) {
//        DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
//        TransactionStatus status = txManager.getTransaction(txDef);
        Integer id = null;
//        try {
//            if(student.getGroup() != null) {
        Integer groupId = groupDao.save(student.getGroup());
//            }
        String format = null;
        if(student.getBirth() != null) {
            format = student.getBirth().format(DateTimeFormatter.ISO_LOCAL_DATE);
        }
        student.getGroup().setId(groupId);
        id = simpleJdbcInsert
                .executeAndReturnKey(
//                        new BeanPropertySqlParameterSource(student)
                        new MapSqlParameterSource()
                                .addValue("first_name", student.getFirstName())
                                .addValue("last_name", student.getLastName())
                                .addValue("birth", format)
                                .addValue("group_id", groupId)
                )
                .intValue();
//            txManager.commit(status);
//        } catch (Exception ex) {
//            txManager.rollback(status);
//        }
        return id;
    }

    static RowMapper<Student> studentRowMapper() {
        return (resultSet, i) -> {
            LocalDate birth = null;
            if(resultSet.getString("birth") != null) {
                birth = LocalDate.parse(resultSet.getString("birth"), DateTimeFormatter.ISO_LOCAL_DATE);
            }
            return Student.builder()
                    .id(resultSet.getInt("id"))
                    .firstName(resultSet.getString("first_name"))
                    .lastName(resultSet.getString("last_name"))
                    .birth(birth)
                    .group(new Group(resultSet.getInt("group_id"), "null"))
                    .build();
        };
    }

    @Transactional(readOnly = true)
    @Override
    public Student findById(Integer id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, first_name, last_name, birth, group_id from students where id=?",
                studentRowMapper(), id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Student> findAll() {
        return jdbcTemplate.query("SELECT id, first_name, last_name, birth, group_id from students",
                studentRowMapper());
    }

    static PreparedStatementCreator updateStudent(Student student) {
        return conn -> {
            val stmt = conn.prepareStatement(
                    "UPDATE students set first_name=?, last_name=?, birth=? where id=?");
            stmt.setString(1, student.getFirstName());
            stmt.setString(2, student.getLastName());
            stmt.setString(3, student.getBirth().format(DateTimeFormatter.ISO_LOCAL_DATE));
            stmt.setInt(4, student.getId());
            return stmt;
        };
    }

    @Override
    @Transactional
    public void update(Student student) {
        jdbcTemplate.update(updateStudent(student));
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        jdbcTemplate.update("DELETE FROM students where id=?", id);
    }
}
