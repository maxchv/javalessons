package org.itstep.domain.dao;

import org.itstep.domain.entity.Student;

import java.util.List;

public interface Dao<T> {

    boolean exists(T data);

    void saveAll(T[] data);

    Integer save(T data);

    T findById(Integer id);

    List<T> findAll();

    void update(T data);

    void delete(Integer id);
}
