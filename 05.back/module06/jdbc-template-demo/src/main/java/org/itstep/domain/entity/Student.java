package org.itstep.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Student {
    private int id;
    private String firstName;
    private String lastName;
    private LocalDate birth;
    private Group group;
}
