package org.itstep.domain.dao;

import org.itstep.domain.entity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Component
public class GroupDao implements Dao<Group> {

    private final DataSource dataSource;
    private final SimpleJdbcInsert jdbcInsert;
    private final PlatformTransactionManager txManager;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public GroupDao(@Qualifier("dataSource") DataSource dataSource, PlatformTransactionManager txManager) {
        this.dataSource = dataSource;
        this.jdbcInsert = new SimpleJdbcInsert(dataSource);
        jdbcInsert.withTableName("`groups`")
                .usingColumns("name")
                .usingGeneratedKeyColumns("id");
        this.txManager = txManager;
        jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Transactional(propagation =  Propagation.REQUIRES_NEW, readOnly = true)
    @Override
    public boolean exists(Group data) {
//        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
////        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
////        transactionDefinition.setReadOnly(true);
//        TransactionStatus status = txManager.getTransaction(transactionDefinition);
        Integer count = null;
//        try {
            String sql = "SELECT count(*) from `groups` where name=?";
            Object[] args = {data.getId()};
            count = jdbcTemplate.queryForObject(sql, args, Integer.class);
//            txManager.commit(status);
//        }catch (Exception ex) {
//            txManager.rollback(status);
//        }
        return count != null && count != 0;
    }

    @Override
    public void saveAll(Group... groups) {

    }

    @Transactional
    @Override
    public Integer save(Group group) {
        if(group == null) return null;
        if(exists(group)) return findIdByName(group.getName());

//        TransactionStatus status = txManager.getTransaction(new DefaultTransactionDefinition());
        Integer id = null;
//        try{
            id=jdbcInsert.executeAndReturnKey(new BeanPropertySqlParameterSource(group)).intValue();
//            txManager.commit(status);
//        }catch (Exception ex) {
//            txManager.rollback(status);
//        }
        return id;
    }

    @Transactional(readOnly = true)
    public Integer findIdByName(String name) {
        return jdbcTemplate.queryForObject("SELECT id from `groups` where name=?", Integer.class, name);
    }

    @Override
    public Group findById(Integer id) {
        return jdbcTemplate.queryForObject("SELECT id, name from `groups` where id=?",
                (resultSet, i) -> new Group(resultSet.getInt("id"), resultSet.getString("name")),
                id);
    }

    @Override
    public List<Group> findAll() {
        return null;
    }

    @Override
    public void update(Group student) {

    }

    @Override
    public void delete(Integer id) {

    }
}
