package org.itstep;

import org.itstep.config.AppConfig;
import org.itstep.service.BasicMessageOfTheDayImpl;
import org.itstep.service.MessagePrinter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

    public static void main(final String[] args) {

        // POJO vs EJB
        // Plain Old Java Object
        // EJB Enterprise Java Been
        // TODO: 2. Конфигурация в xml
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application.xml");
        MessagePrinter bean = context.getBean(MessagePrinter.class);
        bean.printMessage();

        BasicMessageOfTheDayImpl basicMessageOfTheDay
                = context.getBean(BasicMessageOfTheDayImpl.class);
        basicMessageOfTheDay.setMessage("Здоровенькі були");
        bean = context.getBean(MessagePrinter.class);
        bean.printMessage();

        // TODO: 3. Конфигурация через аннотации
        // @Component
        // @Autowire | @Inject
        // @Configuration @ComponentScan
        context = new AnnotationConfigApplicationContext(AppConfig.class);
        bean = context.getBean(MessagePrinter.class);
        bean.printMessage();
    }
}
