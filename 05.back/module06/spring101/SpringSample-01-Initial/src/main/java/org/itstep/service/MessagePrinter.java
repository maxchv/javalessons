package org.itstep.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MessagePrinter {

    private MessageOfTheDayService service;

    @Autowired
    public MessagePrinter(@Qualifier("basicMsg") MessageOfTheDayService service) {
        this.service = service;
    }

    public void printMessage() {
        System.out.println(service.getMessage());
    }

    public void setService(MessageOfTheDayService service) {
        this.service = service;
    }

}
