package org.itstep.service;

import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.GregorianCalendar;

@Component("dynamicMsg")
public class DynamicMessageOfTheDayImpl implements MessageOfTheDayService {

    private String[] messages = new String[]{
            "Воскресенье",
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота"
    };

    public String getMessage() {

        int day = GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK);

        return messages[day - 1];
    }

	public DynamicMessageOfTheDayImpl() {
	}

	public DynamicMessageOfTheDayImpl(String[] messages) {
        this.messages = messages;
    }
}
