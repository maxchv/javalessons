package org.itstep.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component("basicMsg")
@PropertySource("config.properties")
public class BasicMessageOfTheDayImpl implements MessageOfTheDayService {

	@Value("${message}")
	private String message;
	
	public String getMessage() {		
		return message;
	}

	public void setMessage(String message){
		this.message = message;
	}
}
