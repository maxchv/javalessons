package org.itstep;

import org.itstep.service.BasicMessageOfTheDayImpl;
import org.itstep.service.DynamicMessageOfTheDayImpl;
import org.itstep.service.MessagePrinterService;
import org.itstep.service.ObjectFactory;


public class Client {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {

        // В последствии можно заменить DynamicMessageOfTheDayImpl.class на другой класс реализующий
        // интерфейс MessageOfTheDayService
        Client client = Client.class.newInstance(); //new Client();
        MessagePrinterService messagePrinter = ObjectFactory
                .getInstance(MessagePrinterService.class, BasicMessageOfTheDayImpl.class);

        messagePrinter.printMessage();
    }
}
