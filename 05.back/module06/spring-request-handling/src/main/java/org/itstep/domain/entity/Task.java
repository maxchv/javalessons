package org.itstep.domain.entity;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class Task {
    private int id;
    @NonNull
    private String title;
    @NonNull
    private String description;
}
