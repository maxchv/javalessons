package org.itstep.domain.dao.impl;

import lombok.val;
import org.itstep.domain.dao.TaskDao;
import org.itstep.domain.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskDaoImpl implements TaskDao {

    private final DataSource dataSource;

    @Autowired
    public TaskDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Task> findAll() {
        List<Task> tasks = new ArrayList<>();
        try (Connection conn = getConnection()) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, title, description from tasks");
            while (rs.next()) {
                tasks.add(new Task(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("description")
                ));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tasks;
    }

    @Override
    public Task findById(Integer integer) {
        return null;
    }

    @Override
    public void save(Task data) {
        try (Connection conn = getConnection()) {
            val stmt = conn.prepareStatement("INSERT INTO tasks(title, description) values(?,?)");
            stmt.setString(1, data.getTitle());
            stmt.setString(2, data.getDescription());
            stmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Task update(Task data) {
        return null;
    }

    @Override
    public void delete(Integer id) {
        try (Connection conn = getConnection()) {
            val stmt = conn.prepareStatement("delete from tasks where id=?");
            stmt.setInt(1, id);
            stmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
//        DriverManager.getConnection()
        return dataSource.getConnection();
    }
}
