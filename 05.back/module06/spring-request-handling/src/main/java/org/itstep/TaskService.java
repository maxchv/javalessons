package org.itstep;

import org.itstep.domain.dao.TaskDao;
import org.itstep.domain.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    private final TaskDao taskDao;

    @Autowired
    public TaskService(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    public List<Task> findAll() {
        return taskDao.findAll();
    }

    public void save(Task task) {
        taskDao.save(task);
    }

    public void delete(Integer id) {
        taskDao.delete(id);
    }
}
