package org.itstep.domain.dao;

import org.itstep.domain.entity.Task;

import java.sql.Connection;
import java.sql.SQLException;

public interface TaskDao extends Dao<Task, Integer> {
    Connection getConnection() throws SQLException;
}
