package org.itstep.controller;

import org.itstep.TaskService;
import org.itstep.domain.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("tasks", taskService.findAll());
        return "tasks/index";
    }

    @GetMapping("/add")
    public String add(Model model) {
        return "tasks/add";
    }

    @PostMapping("/add")
    public String add(Task task, RedirectAttributes redirectAttributes) {
        System.out.println(task);
        taskService.save(task);
        redirectAttributes.addFlashAttribute("msg", "Successfully added");
        // перенаправление на страницу /tasks/add
        return "redirect:/tasks/add";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
        taskService.delete(id);
        redirectAttributes.addFlashAttribute("msg", "Deleted by id " + id);
        return "redirect:/tasks";
    }
}
