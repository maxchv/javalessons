<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>Add new task</h1>
    <nav><menu><li><a href="<c:url value="/tasks/all"/>">All tasks</a></li></menu></nav>

<main>
    <div>
        <form action="<c:url value="/tasks/add"/>" method="post">
            <label for="title">Title</label>
            <input name="title" id="title"/>
            <br/>
            <label for="description">Description</label>
            <textarea name="description" id="description"></textarea>
            <br/>
            <input type="submit">
        </form>
        <div>${msg}</div>
    </div>
</main>
</body>
</html>
