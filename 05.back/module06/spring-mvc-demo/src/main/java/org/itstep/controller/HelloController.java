package org.itstep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Locale;

@Controller
@SessionAttributes
public class HelloController {

    @Autowired
    WebApplicationContext webApplicationContext;

    //@RequestMapping(method = RequestMethod.GET)
    //@ResponseBody
    @GetMapping
    public String index() {
        //return "Hello World";
        //model.addAttribute("time", LocalTime.now());
        return "index";
    }

    @ModelAttribute(name = "time")
    public LocalTime getTime() {
        return LocalTime.now();
    }

    @RequestMapping(method = RequestMethod.POST)
    public String info(String info) {
        System.out.println("Info: " + info);
        return "index";
    }

}
