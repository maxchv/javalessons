<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <link href="${pageContext.request.contextPath}css/style.css" rel="stylesheet"/>
</head>
<body>
    <h1>Hello Spring Web MVC Framework</h1>
<div>
    <form method="post">
        <label><input type="text" name="info"/></label>
        <input type="submit">
    </form>
</div>
<footer>
    <p>Time: ${time}</p>
</footer>
</body>
</html>
