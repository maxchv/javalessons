package org.itstep.service;

import org.itstep.Cached;
import org.itstep.Post;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    @Cached
    @Override
    public void save(Post post) {
        System.out.println("Save post: " + post);
    }
}
