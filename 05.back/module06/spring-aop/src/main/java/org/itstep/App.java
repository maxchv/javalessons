package org.itstep;

import org.itstep.service.PostService;
import org.itstep.service.PostServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.Duration;
import java.time.Instant;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        //demo01();
        Instant start = Instant.now();
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application.xml");
        PostService postService = ctx.getBean(PostService.class);
        Post post = new Post(2, "Hi", "It is second post");
        postService.save(post);
        System.out.println(postService);
        Duration between = Duration.between(start, Instant.now());
        System.out.println(between.toMillis());

    }

    private static void demo01() {
        PostService postService = new PostServiceImpl();
        Post post = new Post(1, "Hello World", "It is first post");
        //postService.save(post);
        InvocationHandler handler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object retVal = null;
                System.out.println("Begin invoke");
                retVal = method.invoke(postService, args);
                System.out.println("End invoke");
                return retVal;
            }
        };
        PostService proxy = (PostService) Proxy.newProxyInstance(App.class.getClassLoader(), new Class[]{PostService.class}, handler);
        proxy.save(post);
    }
}
