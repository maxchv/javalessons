package org.itstep.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
// aspect - класс, в котором реализуется сквозная функциональность
public class PostServiceAspect {
    // pointcut - срез, набор точек соединения
    @Pointcut("execution(* org.itstep.service.*.*(..))")
    private void savePointCut(){
    }

    @Pointcut("@annotation(org.itstep.Cached)")
    private void cachedPointCut() {
    }

    // advice - совет т.е. функционал, который будет внедрен (wave)
//    @Before("execution(* org.itstep.service.PostService.save(..))")
    @Before("savePointCut()")
    public void before(JoinPoint joinPoint) {
        //System.out.println("JoinPoint: " + joinPoint);
        System.out.println("Begin invoke");
    }

//    @After("execution(* org.itstep.service.PostService.save(..))")
    @After("savePointCut()")
    public void after() {
        System.out.println("After invoke");
    }

    ///@Around("execution(* org.itstep.service.PostService.save(..))")
    @Around("savePointCut()")
    public Object around(ProceedingJoinPoint jp) throws Throwable {
        //System.out.println("JoinPoint: " + jp.getSignature());
        System.out.println("Before around");
        Object retVal = jp.proceed();
        System.out.println("After around");
        return retVal;
    }

//    @AfterReturning(value = "execution(* org.itstep.service.PostService.save(..))", returning = "result")
    @AfterReturning(value = "savePointCut()", returning = "result")
    public void afterReturning(JoinPoint jp, Object result) {
        System.out.println("Result: " + result);
    }

//    @AfterThrowing(value = "execution(* org.itstep.service.PostService.save(..))", throwing = "ex")
    @AfterThrowing(value = "savePointCut()", throwing = "ex")
    public void afterThrowing(JoinPoint jp, Throwable ex) {
        System.err.println(ex.getMessage());
    }

}
