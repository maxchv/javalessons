package org.itstep.service;

import org.itstep.Post;

public interface PostService {
    void save(Post post);
}
