# Module 6. Introduction to Spring

1.	What is Spring?
2.	Objectives and tasks of Spring.
3.	Origin.
4.	The Spring architecture.
5.	REST and SOAP
6.	Spring MVC.
7.	The Spring MVC architecture.
8.	The examples use.
9.	Spring Boot.
10.	Spring Security.
11.	Spring Data.
12.	Microservice architecture.
