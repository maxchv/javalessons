insert into users (username, password, enabled)
values ('admin', '$2a$10$u.08TYiy0UtSc3w87JtpDeDxwZyxlwrExbKbH6z1WDfjRkHSzhkny', true);

insert into authorities (username, authority)
values ('admin', 'ROLE_ADMIN');