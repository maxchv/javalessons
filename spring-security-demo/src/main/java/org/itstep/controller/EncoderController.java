package org.itstep.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/encoder")
@RequiredArgsConstructor
@Slf4j
public class EncoderController {

    private final PasswordEncoder passwordEncoder;

    @GetMapping
    String index() {
        return "encoder";
    }

    @PostMapping
    String encode(String password, Model model) {
        log.info("encoding password {}", password);
        String encodedPassword = passwordEncoder.encode(password);
        model.addAttribute("encodedPassword", encodedPassword);
        model.addAttribute("password", password);
        return "encoder";
    }

    @PostMapping("/match")
    String match(String rawPassword, String encodedPassword, Model model){
        boolean matches = passwordEncoder.matches(rawPassword, encodedPassword);
        model.addAttribute("matches", matches);
        model.addAttribute("rawPassword", rawPassword);
        model.addAttribute("encodedPassword", encodedPassword);
        return "match";
    }

}
