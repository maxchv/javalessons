package org.itstep.controller;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/singup")
public class SingUpController {

    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    public SingUpController(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = (UserDetailsManager) userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    String index() {
        return "singup";
    }

    @PostMapping
    String singUp(String login, String password) {
        UserDetails user = new User(login, passwordEncoder.encode(password),
                AuthorityUtils.createAuthorityList("ROLE_USER"));
        userDetailsManager.createUser(user);
        return "redirect:/";
    }
}
