package ua.itstep;

import java.util.Scanner;

public class Application {

	public static final int MB = 1024 * 1024;
	public static final int DEFAULT_SIZE = 10;

	private static class List {
		Object[] obj;
		int size = -1;
		int count = -1;
		
		List(int size) {
			count = 0;
			this.size = size;
			obj = new Object[size];
		}
		
		void add(Object item) {
			if(!this.full()) {
				obj[count++] = item;
			}
		}

		void removeLast() {
			if(!this.empty()) {
				obj[count--] = null;
			}
		}
		
		boolean empty() {
			return count < 0;
		}

		boolean full() {
			return count >= size;
		}
	}
	
	static List list = new List(512);

	/**
	 * По умолчанию - создание объекта размером 10 Мб
	 */
	public static void addObjects() {
		// 10 Мб
		list.add(new byte[DEFAULT_SIZE * MB]);
	}
	
	public static void removeObjects() {
		list.removeLast();
	}
	
	static Scanner in = new Scanner(System.in);
	
	public static void start() {
		boolean done = false;
		while(!done) {
			System.out.println("Создано: " + list.count + " объектов и около " + list.count * DEFAULT_SIZE + " МБ");
			System.out.println("\n\nВаши действия:\n"
					+ "1. Создать объекты\n"
					+ "2. Удалить объекты\n"
					+ "3. Вызвать сборщик мусора (GC)\n"
					+ "0. Выйти\n"
					+ ">>> ");
			String line = in.nextLine();
			switch (line) {
			case "1":
				addObjects();
				break;
			case "2":
				removeObjects();
				break;
			case "3":
				System.gc();
				break;
			case "0":
				done = true;
				break;
			default:
				break;
			}
		}
	}

	static class Demo {
		@Override
		protected void finalize() throws Throwable {
			System.out.println("finalize");
		}
	}

	public static void main(String[] args) {
		// Запускать с параметрами:
		// -Xms512m -Xmx512m -XX:NewRatio=3 -XX:+PrintGCTimeStamps
		// -XX:+PrintGCDetails -Xloggc:gc.log
//		start();
		Demo demo = new Demo();
		demo =  null;
		System.gc();
	}

}
