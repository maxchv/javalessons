package org.example;


import java.util.Arrays;
import java.util.Collection;

public class App {

    public static void main(String[] args) {
        Vegitable a = new Apple(10);
        Box<Apple> appleBox = new Box<Apple>(new Apple(30));
        Box<Vegitable> emptyBox = new Box<>();
        swap(appleBox, emptyBox);
        System.out.println(appleBox.getData());
        System.out.println(emptyBox.getData());

        //printApple(a);
        printVegitalbe(a);

        Double[] arr = new Double[10];
        fill(arr, 0.0);
        System.out.println(Arrays.toString(arr));

        Apple[] apples = {new Apple(30), new Apple(10), new Apple(15)};
        Arrays.sort(apples);
        System.out.println(Arrays.toString(apples));
//
//        MinMax minMax = new MinMax(apples);
//        System.out.println(minMax.min()); // Apple{weight=10}
//        System.out.println(minMax.max()); // Apple{weight=30}

    }

    private static <T> void fill(T[] arr, T value) {
        //T[] mas = (T[]) new Object[10];
        for(int i = 0; i<arr.length; i++) {
            arr[i] = value;
        }
    }

    static void printVegitalbe(Vegitable vegitable) {
        System.out.println(vegitable);
    }

    static void printApple(Apple apple) {
        System.out.println(apple);
    }

    static void swap(Box<? extends Vegitable> producer, Box<? super Vegitable> consumer) {
        consumer.setData(producer.getData());
        producer.setData(null);
    }
}

class Vegitable {
    private String name;

    public Vegitable(String name) {
        this.name = name;
    }
}

class Apple extends Vegitable implements Comparable<Apple> {

    private int weight;

    public Apple(int weight) {
        super("Apple");
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Apple apple) {
        return weight - apple.weight;
    }
}

class Banana extends Vegitable {

    public Banana(String name) {
        super(name);
    }
}

class Box<T> {
    private T data;

    public Box(T data) {
        this.data = data;
    }

    public Box() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}