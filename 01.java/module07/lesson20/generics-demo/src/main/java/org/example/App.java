package org.example;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        //demo01();
        //demo02();

        Person[] people = {
                new Person("Вася", 10),
                new Person("Маша", 30),
                new Person("Даша", 41),
                new Person("Рома", 66),
                new Person("Рома", 16),
        };

        Arrays.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        //demo03(people);
        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("----------");

        // найти всех Person age > 30 используя локальный класс
        // Person[] res = filter(people, ???);

        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("----------");

        // найти всех Person age < 30 используя анонимный класс
        // res = filter(people, ???);

        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("----------");

        // найти всех Person имена которых не Рома используя лямбду
        // res = filter(people, ???);

        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("----------");
    }

    private static void demo03(Person[] people) {
        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("----------");

//        class CmpByAge implements PersonCompare {
//            @Override
//            public int Compare(Person p1, Person p2) {
//                return p1.getAge() - p2.getAge();
//            }
//        }
        // Анонимный класс
        PersonCompare cmp = new PersonCompare(){
            @Override
            public int Compare(Person p1, Person p2) {
                return p1.getAge() - p2.getAge();
            }
        };

        sort(people, cmp);

        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("---");

//        class CmpByName implements PersonCompare {
//            @Override
//            public int Compare(Person p1, Person p2) {
//                return p1.getName().compareTo(p2.getName());
//            }
//        }
//        cmp = new CmpByName();

        sort(people, new PersonCompare() { // Анонимный класс
            @Override
            public int Compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        System.out.println("---");

        for (Person p : people) {
            System.out.println(p);
        }

        System.out.println("---");
        // Лямбда выражения - анонимные методы
        sort(people, (Person p1, Person p2) -> p2.getAge() - p1.getAge());

        for (Person p : people) {
            System.out.println(p);
        }
    }

    interface PersonCompare {
        // Если p1 > p2 - возвращается число больше 0
        // Если p1 < p2 - возвращается число меньше 0
        // Если p1 == p2 - возвращается 0
        int Compare(Person p1, Person p2);
    }

    interface Predicate {
        boolean test(Person p);
    }

    private static Person[] filter(Person[] arr, Predicate predicate) {
        Person[] res = new Person[0];
        for(Person p : arr) {
            if(predicate.test(p)) {
                res = Arrays.copyOf(res, res.length + 1);
                res[res.length - 1] = p;
            }
        }
        return res;
    }

    private static void sort(Person[] arr, PersonCompare cmp) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 1; j < arr.length; j++) {
                //if (arr[j - 1].getAge() < arr[j].getAge()) {
                if(cmp.Compare(arr[j-1], arr[j]) > 0){
                    Person tmp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

    private static void demo02() {
        Country ukraine = new Country("Украина", "UA");
        System.out.println(ukraine);
    }

    private static void demo01() {
        char[] sutes = {'♠', '♥', '♦', '♣'};
        String[] ranks = {"6", "7", "8", "9", "10", "В", "Д", "К", "Т"};
        Card[] cards = new Card[36];
        int i = 0;
        for (Suits sute : Suits.values()) {
            for (String rank : ranks) {
                cards[i++] = new Card(sute, rank);
            }
        }
        for (Card card : cards) {
            System.out.printf("%4s", card);
        }
    }
}

class Person {
    private static int count;
    private int id;
    private String name;
    private int age;

    public Person(String name, int age) {
        this.id = ++count;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

class Country {
    private String code;
    private String name;

    abstract class City {
        String name;

        public City(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Город: " + this.name + " Страна: " + Country.this.name + " Код страны: " + code;
        }
    }

    City[] cities;

    public Country(String name, String code) {
        this.name = name;
        this.code = code;
        cities = new City[]{buildCity("Днепр", 1000_000)};
    }

    public City buildCity(String name, final int peoples) {
        class InnerCity extends City { // локальный класс
            int people;

            public InnerCity(String name) {
                super(name);
                people = peoples;
            }
        }
        //peoples = 10000;

        return new InnerCity(name);
    }

    @Override
    public String toString() {
        StringBuilder allCities = new StringBuilder();
        for (City c : cities) {
            allCities.append(c).append("\n");
        }
        return allCities.toString();
    }
}

enum Suits {
    Heart('♥'), Diamond('♦'), Club('♣'), Spade('♠');
    final char suit;

    Suits(char suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return String.valueOf(suit);
    }
};

enum Ranks {
    Ace(11), King(4), Queen(3), Jack(2), Ten(10), Nine(9), Eight(8), Seven(7), Six(6);
    final int value;

    int alternativeValue() {
        if (this == Ranks.Ace) {
            return 1;
        }
        return value;
    }

    Ranks(int value) {
        this.value = value;
    }
}

class Card {
    private Suits suite; // рубашка
    private String rank;

    public Card(Suits suite, String rank) {
        this.suite = suite;
        this.rank = rank;
    }

    @Override
    public String toString() {
        return suite + rank;
    }
}