package org.itstep;

import java.util.Scanner;

// package
public class App {

    /**
     * Соединяет массив строк в одну строку разделенную пробелом
     *
     * @param strings - массив строк
     * @return строка состоящая из элементов массив
     */
    public static String join(String[] strings) {
        return join(strings, " ");
    }

    /**
     * Соединяет массив строк в одну строку разделенную соединителем glue
     *
     * @param strings - массив строк
     * @param glue    - соединитель
     * @return строка состоящая из элементов массива
     */
    public static String join(String[] strings, String glue) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : strings) {
            stringBuilder.append(s).append(glue);
        }
        return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
    }

    public static void main(String[] args) {
        //demo1();

//         Point p1 = new Point();
//         p1.show(); // x = 0; y = 0; z = 0
//         Point p2 = new Point(10);
//         p2.show(); // x = 10; y = 0; z = 0
//         Point p3 = new Point(10, 20);
//         p3.show(); // x = 10; y = 20; z = 0
//         Point p4 = new Point(10, 20, 30);
//         p4.show(); //x = 10; y = 20; z = 30

//        CustomException customException = new CustomException("test message");
//        System.out.println(customException.getMessage());

        /*
        Vehicle bike = new Vehicle();
        bike.addTire(new Tire("Pirelli", 15));
        bike.addTire(new Tire("Pirelli", 15));
        System.out.println("Bike has " + bike.tiresCount() + " tires");

        Vehicle car = new Vehicle();
        for (int i = 0; i < 4; i++) {
            car.addTire(new Tire("Belshina", 17));
        }

        System.out.println("Car has " + car.tiresCount() + " tires");
        */

        Vehicle car = new Vehicle("Unknown");
        car.model = "VAZ 2010";
        car.tires = new Tire[4];

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        if (str.matches("[+-]?\\d+")) {
            long l = Long.parseLong(str);
            if (l <= Integer.MAX_VALUE) {
                System.out.println("Integer");
            } else {
                System.out.println("Long");
            }
        }
    }

    private static void demo1() {
        String[] strings = {"one", "two", "tree"};
        String first = join(strings);
        System.out.println(first);
        String second = join(strings, ",");
        System.out.println(second);
    }
}

