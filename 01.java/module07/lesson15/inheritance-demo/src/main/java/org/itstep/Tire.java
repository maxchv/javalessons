package org.itstep;

public class Tire {
    private String made;
    private double diameter;

    public Tire(String made, double diameter) {
        this.made = made;
        this.diameter = diameter;
    }

    public String getMade() {
        return made;
    }

    public void setMade(String made) {
        this.made = made;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }
}
