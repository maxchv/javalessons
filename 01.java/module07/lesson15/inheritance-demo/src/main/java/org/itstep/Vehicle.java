package org.itstep;

import java.util.Arrays;

public class Vehicle extends Object {
    // package-private (default)
    String model; // null
    protected Tire[] tires; // null <- агрегация

    public Vehicle() {
//        this.model = "Unknown";
//        setModel("Unknow");
//        tires = new Tire[1];
        this("Unknown");
    }

    public Vehicle(String model) {
//        this.model = model;
        setModel(model);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void addTire(Tire tire) {
        if (tires == null) {
            tires = new Tire[1];
        } else {
            tires = Arrays.copyOf(tires, tires.length + 1);
        }
        tires[tires.length - 1] = tire;
    }

    public int tiresCount() {
        return tires.length;
    }

    public void show() {
        System.out.print(model + " has " + tiresCount() + " tires");
    }

    @Override
    public String toString() {
        return model + " has " + tiresCount() + " tires";
    }
}
