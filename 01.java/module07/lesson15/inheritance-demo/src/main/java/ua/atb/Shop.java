package ua.atb;

import org.itstep.Tire;
import org.itstep.Vehicle;

// WORLD
public class Shop {
    public static void main(String[] args) {
        //Vehicle bike = new Vehicle();
        //bike.model = "Java";
        //bike.tires = new Tire[2];

        Bike java = new Bike("Java", false);
        //System.out.println(java.getModel());
//        java.show();

        System.out.println(java);
    }
}


class Bike extends Vehicle {
    private boolean hasSideCar;

    public Bike(String model, boolean hasSideCar) {
        super(model);
        this.hasSideCar = hasSideCar;
        tires = new Tire[2];
//        this.model = model;
//        setModel(model);
    }

    @Override // Аннотация
    public void show() { // переопределение
        super.show();
        System.out.println(" has side car? " + hasSideCar);
    }

    @Override
    public String toString() {
        return super.toString() + " has side car? " + hasSideCar;
    }
}