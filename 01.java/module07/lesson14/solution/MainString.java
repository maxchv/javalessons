package org.itstep.task06;

import java.util.Arrays;

/**
 * Задание
 * Описать  базовый  класс  MainString  (Строка).
 * <p>
 * Обязательные поля класса:
 * - массив символов (chars);
 * - значение типа int хранит длину строки в символах (len).
 * <p>
 * Реализовать  обязательные  методы  следующего  назначения:
 * - конструктор без параметров;
 * - конструктор, принимающий в качестве параметра строковый литерал;
 * - конструктор, принимающий в качестве параметра символ и длину строки;
 * - метод получения длины строки (length());
 * - метод очистки строки (делает строку пустой) (clear());
 * - метод конкатенации (соединяет две строки типа MainString) (concat);
 * - метод поиска символа в строке (indexOf()).
 * - метод toString(), который возвращает строковое представление объекта
 * <p>
 * Класс должен находиться в отдельном файле в этом же пакете
 */
public class MainString {
    private char[] chars;
    private int len;

    public MainString() {
        chars = new char[100];
    }

    public MainString(String str) {
        chars = str.toCharArray();
        len = chars.length;
    }

    public MainString(char ch, int len) {
        chars = new char[len];
        Arrays.fill(chars, ch);
        this.len = chars.length;
    }

    public int length() {
        return len;
    }

    public void clean() {
        len = 0;
    }

    public MainString concat(MainString other) {
        return new MainString(toString() + other.toString());
    }

    public int indexOf(int ch) {
        return new String(Arrays.copyOfRange(chars, 0, len)).indexOf(ch);
    }

    @Override
    public String toString() {
        return new String(Arrays.copyOfRange(chars, 0, len));
    }
}
