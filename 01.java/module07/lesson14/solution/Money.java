package org.itstep.task05;

/**
 * Создать  класс  Money  (Деньги)  для  работы  с  денежными  суммами.
 * <p>
 * Число должно быть представлено двумя полями:
 * - типа long (hryvnia)  – для гривен;
 * - типа byte (kopecks) – для копеек.
 * <p>
 * Добавить сеттеры и геттеры для полей
 * <p>
 * Добавить три конструктора: по умолчанию и два с параметрами (первый принимает только гривны, второй - гривны и копейки)
 * <p>
 * Реализовать вывод значения на экран, при этом дробная часть должна быть отделена от целой части запятой.
 * <p>
 * Реализовать:
 * - сложение (addition).
 * - вычитание (subtraction),
 * - умножение (multiplication),
 * - деление на дробное число (division),
 * - умножение на дробное (метод multiply) число
 * - операции сравнения (метод equals) возвращает boolean.
 * <p>
 * Методы сложения, вычитания, умножения должны принимать другой объект Money
 * <p>
 * Все методы, кроме equals, возвращают результат типа Money
 * <p>
 * Класс должен находиться в отдельном файле в этом же пакете
 */
public class Money {
    private long hryvnia;
    private byte kopecks;

    public Money() {
        this(0L, (byte) 0);
    }

    public Money(long hryvnia) {
        this(hryvnia, (byte) 0);
    }

    public Money(long hryvnia, byte kopecks) {
        this.hryvnia = hryvnia;
        this.kopecks = kopecks;
    }

    public long getHryvnia() {
        return hryvnia;
    }

    public void setHryvnia(long hryvnia) {
        this.hryvnia = hryvnia;
    }

    public byte getKopecks() {
        return kopecks;
    }

    public void setKopecks(byte kopecks) {
        this.kopecks = kopecks;
    }

    public Money addition(Money money) {
        return new Money(hryvnia + money.hryvnia, (byte) (kopecks + money.kopecks));
    }

    public Money subtraction(Money money) {
        return new Money(hryvnia - money.hryvnia, (byte) (kopecks - money.kopecks));
    }

    public Money multiply(double value) {
        return new Money((long)(hryvnia*value), (byte)(kopecks*value));
    }

    public Money division(double value) {
        return new Money((long)(hryvnia/value), (byte)(kopecks/value));
    }

    public boolean equals(Money money) {
        return hryvnia == money.hryvnia && kopecks == money.kopecks;
    }
}
