package org.itstep;

import java.util.Scanner;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Car.count = " + Car.count);

        // ООП
        // 1. Инкапусляция - ограничение доступа к аттрибутам и операциям объекта: public, private
        // 2. Наследование
        // 3. Полиморфизм
        // 4. Абстракция

        Car car = new Car("White", "Mercedes", 100_000); // car - экземпляр класса (объект) типа Car
        /*car.color = "White";
        car.model = "Mersedes";
        car.price = 100_000;*/
        car.show();

//        int[] arr = new int[10];
//        System.out.println(arr.length);

        Car car2 = new Car();
        car2.show();
        car2.setColor("Black");
        car2.setModel("BMW");
        car2.setPrice(-150_000);
        car2.show();

        Car copy = car2.clone();
        copy.show();
    }
}


class Car implements Cloneable {
    public static int count;

    // поля класса
    private String color;
    private String model;
    private double price;

    // геттер + сеттер = аксессоры (access)
    // getters - геттеры - методы, которые возвращают значение заданного поля
    public String getColor() {
        return color;
    }

    // Alt+Insert
    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    // сеттер
    public void setColor(String color) {
        this.color = color;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Password: ");
        String password = scanner.nextLine();
        if("qwerty".equals(password)) {
            this.price = price;
        }
    }

    // Конструктор класса (конструктор по умолчанию)
    public Car() {
        System.out.println("Default constructor");
        color = "Baklagan";
        model = "Lada Sedan";
        price = 500_000;
    }

    // Перегруженный конструктор с тремя параметрами
    public Car(String color, String model, double price) {
        System.out.println("Constructor with parameters: color = " + color + ", model = " + model + ", price = " + price);
        this.color = color;
        this.model = model;
        this.price = price;
    }

    public void show() {
        System.out.printf("%s %s $%g%n", model, color, price);
    }

    @Override
    public Car clone() {
        return new Car(color, model, price);
    }
}
