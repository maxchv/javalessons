package org.itstep;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        String input = "Vasilij Pupkin, 23 года, тел. (091) 123-45-67, pukin@mail.com";
        // 1. Поиск в строке - парсинг (разбор) строки

        // 2. Замена строк
        String str = "I Love C++ and C#";
        System.out.println(str.replaceFirst("C.{0,2}", "Java"));
        System.out.println("\\test");
        System.out.println("\\\\*");

        System.out.println("***".replaceAll("\\*", "\\\\*"));

        System.out.println(input.replaceFirst("(\\w+)\\s(\\w+),(.*)", "$3, $2 $1"));

        // 3. Разделение строк
        System.out.println(Arrays.toString("test;test:test$test%test".split("[;:$%]")));

        // 4. Валидация строки
        System.out.println("user@mail.com".matches("\\w+@\\w+(\\.\\w+)+"));
        System.out.println("usermail.com".matches("\\w+@\\w+(\\.\\w+)+"));

        // @formant off
        // Регулярное выражение. Perl
        //   Символ              Сопоставление
        //   a                   a
        //   ab                  ab
        //   .                   любой символ, кроме перевода строки
        //  \d                   любая арабская цифра [0-9]
        //  \w                   любая буква, цифра, знак _ [a-zA-Z0-9_]
        //  \s                   любой пробельный символ [ \t\v\r\n\f]
        //
        //  Экранирование спец. символов
        //  \(                   (
        //  \)                   )
        //  \[                   [
        //  \]                   ]
        //  \.                   .
        //
        //  Классы символов
        //  [abcd]                    любой символ из a,b,c,d
        //  [a-z]                     любая буква (в нижнем регистре) латинского алфавита
        //  [0-9]                     любая арабская цифра
        //  [0-9a-fA-F]               любая цифра шестнадцатиричного числа
        //  0x[0-9a-fA-F][0-9a-fA-F]  0xff
        //
        // Квантификаторов (повторителей символов) - всегда после символов
        // *                     повторение от 0 до ∞
        // +                     повторяется от 1 до ∞
        // ?                     от 0 до 1 раза
        // {n}                   повторение n раз
        // {n, m}                повторение от n до m раз (включительно)
        // {n,}                  повторение от n до ∞
        // {,m}                  повторение от 0 до m раз (включительно)

        // @format on


    }
}
