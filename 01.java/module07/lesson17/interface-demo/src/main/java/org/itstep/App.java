package org.itstep;


import java.util.Arrays;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        // demo01();
        // demo02();
        // demo03();
//        Car car = new Car();
//        System.out.println(car.UP);
        List arrayList = new ArrayList();

        arrayList.add(10);
        arrayList.add(20);
        arrayList.stream();
        System.out.println(arrayList.size());
    }

    private static void demo03() {
        System.out.println("Start program");
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        System.out.print("b = ");
        int b = scanner.nextInt();
        try {
            System.out.println(a / b);
        } catch (Throwable ex) {
            System.err.println(ex.getLocalizedMessage());
        }
        System.out.println("End program");
    }

    private static void demo02() {
        byte b;
        int i = 10;
        b = (byte) i; // casting
        i = b;

        Lorry car = new Car(); // upcasting - up - вверх, casting - приведение типа
        car.setMove(true);

        Animal[] animals = {new Dog(), new Cat(), new Lion()};
        for (Animal a : animals) {
            feed(a);
        }
        Horse horse = new Horse();
        feed(horse);

        boolean isDog = animals[0] instanceof Dog;
        if (isDog) {
            Dog d = (Dog) animals[0];// downcasting
            feed(d);
        } else {
            System.out.println("Это не собака!!!");
        }

        System.out.println(animals[0].getClass().getSimpleName());
        System.out.println(animals[1].getClass().getSimpleName());
        System.out.println(animals[2].getClass().getSimpleName());
    }

    public static void feed(Animal animal) {
        animal.eat();
    }

    private static void demo01() {
        Employee[] employees = new Employee[4];

        employees[0] = new TaxEmployee("Вася Пупкин", "ставка", 12000);
        employees[1] = new Employee("Вася Пупкин", "ставка", 12000);
        employees[2] = new Employee("Вася Пупкин", "ставка", 12000);
        employees[3] = new Employee("Вася Пупкин", "ставка", 12000);
        employees = Arrays.copyOf(employees, employees.length + 1);

        employees[4] = new Employee("Вася Пупкин", "ставка", 12000);
    }
}

interface List {
    void add(Object item);
    void remove(Object item);
    int size();
    default void stream() {
        System.out.println("List stream");
    }
}

class ArrayList implements List{

    @Override
    public void stream() {
        System.out.println("ArrayList stream");
    }

    @Override
    public void add(Object item) {

    }

    @Override
    public void remove(Object item) {

    }

    @Override
    public int size() {
        return 0;
    }
}


class LinkedList implements List{

    @Override
    public void add(Object item) {

    }

    @Override
    public void remove(Object item) {

    }

    @Override
    public int size() {
        return 0;
    }
}

class Zoo {
    // ассоциация
    private Animal[] animals;

    // агрегация
    public void add(Animal animal) {
        if (animals == null) {
            animals = new Animal[1];
        } else {
            animals = Arrays.copyOf(animals, animals.length + 1);
        }
        animals[animals.length - 1] = animal;
    }
}

class Hart {

}

class Human {
    private Hart hart;

    public Human() {
        // композиця
        this.hart = new Hart();
    }
}

class Animal {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void eat() {
        System.out.println(name + " is eating");
    }
}

class Horse extends Animal {

    public Horse() {
        super("Horse", 0);
    }
}

class Dog extends Animal {
    public Dog() {
        super("Dog", 0);
    }
}

class Cat extends Animal {
    public Cat() {
        super("Cat", 0);
    }

    public Cat(String name, int age) {
        super(name, age);
    }
}

class Lion extends Cat {
    public Lion() {
        super("Lion", 0);
    }
}

interface Movable {
    public static final int LEFT = 1;
    int RIGHT = 2;
    int DOWN = 3;
    int UP = 4;

    public abstract void moveRight();
    // JDK 8
    default void moveLeft() {
        System.out.println("Move left");
    }

    static void staticMethod() {
        System.out.println("It is static method");
    }
}

interface FlyableAndMovable extends Movable {

    void flyUp();
    void flyDown();
}

abstract class Lorry {
    private boolean move;

    public boolean isMove() {
        return move;
    }

    public void setMove(boolean move) {
        this.move = move;
    }
}

class Car extends Lorry implements Movable {

    @Override
    public void moveRight() {
        System.out.println("Car move right");
    }

    @Override
    public void moveLeft() {
        System.out.println("Car move left");
    }
}

class Employee {
    private String fillName;
    private String paymentType;
    private double money;

    public Employee(String fillName, String paymentType, double money) {
        this.fillName = fillName;
        this.paymentType = paymentType;
        this.money = money;
    }

    public String getFillName() {
        return fillName;
    }

    public void setFillName(String fillName) {
        this.fillName = fillName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "fillName='" + fillName + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", money=" + money +
                '}';
    }
}

class TaxEmployee extends Employee {

    public TaxEmployee(String fillName, String paymentType, double money) {
        super(fillName, paymentType, money);
    }
}