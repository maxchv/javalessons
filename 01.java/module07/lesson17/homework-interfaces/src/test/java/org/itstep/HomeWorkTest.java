package org.itstep;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.itstep.TestUtil.*;

/**
 * Unit test for simple App.
 */
public class HomeWorkTest {
    public static final String BASE_PACKAGE = "org.itstep";

    @Test
    @DisplayName("Проверка на наличие интерфейса Printable")
    public void isPrintableExists() throws ClassNotFoundException {
        isPublicInterface(Class.forName(BASE_PACKAGE + ".Printable"));
    }

    @Test
    @DisplayName("Интерфейс Printable содержит метод void print()")
    public void isPrintableHasMethodPrint() throws ClassNotFoundException, NoSuchMethodException {
        hasPublicAbstractNoDefaultMethod(Class.forName(BASE_PACKAGE + ".Printable"),
                void.class, "print");
    }

    @Test
    @DisplayName("Проверка наличия класса Book")
    public void isBookExists() throws ClassNotFoundException {
        isPublicNoFinalNoAbstractClass(Class.forName(BASE_PACKAGE + ".Book"));
    }


    @ParameterizedTest(name = "{0} {1}?")
    @CsvSource({"java.lang.String,title", "java.lang.String,author", "int,pages", "int,year", })
    @DisplayName("Проверка полей класса Book")
    public void isBookHasFields(Class<?> type, String field) throws ClassNotFoundException, NoSuchFieldException {
        Class<?> clazz = Class.forName(BASE_PACKAGE + ".Book");
        hasPrivateNoStaticField(clazz, field, type);
    }

    @Test
    @DisplayName("Класс Book реализует интерфейс Printable")
    public void isBookImplementsPrintable() throws ClassNotFoundException {
        isClassImplementsInterface(Class.forName(BASE_PACKAGE + ".Book"), Class.forName(BASE_PACKAGE + ".Printable"));
    }

    @Test
    @DisplayName("Проверка наличия класса Magazine")
    public void isMagazineExists() throws ClassNotFoundException {
        isPublicNoFinalNoAbstractClass(Class.forName(BASE_PACKAGE + ".Magazine"));
    }


    @ParameterizedTest(name = "{0} {1}?")
    @DisplayName("Проверка полей класса Magazine")
    @CsvSource({"java.lang.String,title", "java.lang.String,publisher", "int,number", "int,year", })
    public void isMagazineHasFields(Class<?> type, String field) throws ClassNotFoundException, NoSuchFieldException {
        Class<?> clazz = Class.forName(BASE_PACKAGE + ".Magazine");
        hasPrivateNoStaticField(clazz, "title", String.class);
    }

    @Test
    @DisplayName("Проверка наличия статического метода printMagazines класса Magazine")
    public void isStaticMethodPrintMagazinesExists() throws ClassNotFoundException, NoSuchMethodException {
        Class<?> printableArray = Class.forName("[Lorg.itstep.Printable;");
        TestUtil.hasPublicStaticMethod(Class.forName(BASE_PACKAGE + ".Magazine"), "printMagazines", printableArray);
    }

    @Test
    @DisplayName("Проверка наличия статического метода printBooks класса Book")
    public void isStaticMethodPrintBooksExists() throws ClassNotFoundException, NoSuchMethodException {
        Class<?> printableArray = Class.forName("[Lorg.itstep.Printable;");
        TestUtil.hasPublicStaticMethod(Class.forName(BASE_PACKAGE + ".Book"), "printBooks", printableArray);
    }

}
