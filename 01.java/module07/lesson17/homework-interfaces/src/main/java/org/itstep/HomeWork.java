package org.itstep;


/**
 * Домашнее задание на работу с интерфейсами
 *
 * а) Создать публичный интерфейс Printable, содержащий метод void print().
 *
 * б) Создать публичный класс Book, реализующий интерфейс Printable.
 *    Добавить поля: название (title), автор (author), количество страниц (pages), год публикации (year)
 *    Добавить нужные конструкторы и методы
 *    реализовать метод print() для вывода информации о книге
 *
 * в) Создать публичный класс Magazine, реализующий интерфейс Printable.
 *    Добавить поля: название (title), издатель (publisher), номер (number), год (year)
 *    Добавить нужные конструкторы и методы
 *    реализовать метод print() для вывода информации о журнале
 *
 * г) Создать массив типа Printable, который должен содержать содержать 4 книги и 4 журнала.
 *
 * д) В цикле пройти по массиву и вызвать метод print() для каждого объекта.
 *
 * е) Создать статический метод printMagazines(Printable[] printable) в классе Magazine,
 *    который выводит на консоль только журналы.
 *
 * ж) Создать статический метод printBooks(Printable[] printable) в классе Book,
 *    который выводит на консоль книги.
 *
 * В задании е и ж используем оператор instanceof.
 */
public class HomeWork {
    public static void main(String[] args) {
        // TODO: проверяйте здесь
    }
}
