package org.itstep;

import java.util.Arrays;
import java.util.Comparator;

public class App {
    public static void main(String[] args) {
        //demo01();
        User user = new User("admin", "1234567");
        user.show();
        new User("user", "1234567").show();

        user.new Query().printToLog();

        Integer[] arr = {2,3,9,1,25,1,5,9,};
        Arrays.sort(arr, SortUtil.getComparator(SortDirection.ASC));

        System.out.println(Arrays.toString(arr));

        Arrays.sort(arr, SortUtil.getComparator(SortDirection.DESC));

        System.out.println(Arrays.toString(arr));
    }

    private static void demo01() {
        Country ukraine = new Country("Ukraine");
        ukraine.addCity("Dnipro");

        Human vasja = new Human("Вася", 21);
        vasja.brain.print();
        //Country.City city = new Country.City("New York");

        Human.Brain brainFree = vasja.new Brain(50);
        brainFree.print();
    }
}

enum SortDirection {
    ASC, DESC
}

class SortUtil {

    public static Comparator<Integer> getComparator(SortDirection direction) {
        // локальный класс
        class SortAsc implements Comparator<Integer> {
            @Override
            public int compare(Integer a, Integer b) {
                return a - b;
            }
        }

        class SortDesc implements Comparator<Integer> {
            @Override
            public int compare(Integer a, Integer b) {
                return b - a;
            }
        }

        return direction == SortDirection.ASC ? new SortAsc() : new SortDesc();
    }

}

class User {
    private String login;
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    class Query {
        void printToLog() {
            System.out.println("Поступил запрос от " + login + " пароль: " + password);
        }
    }

    public void show() {
        System.out.println("Login: " + login);
    }
}

class Country {
    private static int countCity;
    private String name;
    private City[] cities;

    // nested static class - вложенный статический класс
    private static class City {
        private String name;
        public City(String name) {
            this.name = name;
            countCity++;
        }
    }

    public Country(String name) {
        this.name = name;
        this.cities = new City[0];
    }

    public void addCity(String name) {
        this.cities = Arrays.copyOf(this.cities, this.cities.length +1);
        this.cities[this.cities.length - 1] = new City(name);
    }
}

abstract class Animal {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public final String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

final class Human extends Animal {
    private boolean hasChild;
    private String name;
    private int age;

    public final Brain brain;

    class Brain { // Внутренний класс (inner class)
        private int weight;
//        static int test;

        Brain(int weight) {
            this.weight = weight;
            age = 0;
        }

        public void print() {
            System.out.println("Я мозг " + name + " мой вес " + weight + " г");
        }
    }

    public Human(String name, int age) {
        super(name, age);
        this.name = name;
        brain = new Brain(100);
        //brain.weight = 100;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }
}

//class Monster extends Human {
//
//}
