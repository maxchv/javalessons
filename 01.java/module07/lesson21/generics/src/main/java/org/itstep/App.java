package org.itstep;

public final class App {

    private App() {
    }

    public static void main(String[] args) {
//		demo01();
        // Box teaBox = new Box() - row type
        Box<Tea> teaBox = new Box<Tea>(new Tea("Черный чай"));
        ////
        ///
        //teaBox.setValue(new Coffee());
        ///
        ////
        //if(teaBox.getValue() instanceof Tea) {
        //	((Tea) teaBox.getValue()).drink();
        teaBox.getValue().drink();
        //} else {
        //	System.out.println("It is not tea. It is " + teaBox.getValue().getClass().getSimpleName());
        //}
        Box<Coffee> coffeeBox = new Box<Coffee>(new Coffee());

//        makeDrink(teaBox);
//        makeDrink(coffeeBox);

        // Примитивные типы данных не могут быть параметрами типов
        // С java 7 - алмазный (diamond) синтаксис
        int i = 0;

        Integer ii = i;// autoboxing - перемещение данных из стека в кучу  new Integer(i);
        i = ii;// unboxing - перемещение данных из кучи в стек

        System.out.println(Integer.parseInt("1"));
        System.out.println(ii.doubleValue());

        Point pp = new Point(1, 2);
        System.out.println((Integer) pp.getX());

        Point<String, Double> p = new Point<>("10", 10.0);
        p.show();

        Drink drink = new Tea("Зеленый");

        drink = new Coffee();

        Box<Drink> box1 = new Box<>(drink);
        box1.setValue(new Coffee());

        Box<Nail> nailBox = new Box<>(new Nail());
        makeDrink(box1);
        makeDrink(coffeeBox);  //ошибка компиляции
        makeDrink(teaBox);  //ошибка компиляции
        //makeDrink(nailBox);

        Box<? extends Drink> boxDrink = new Box<>(new Coffee());
        boxDrink.getValue().drink(); // producer - extends
        Box<? super Drink> boxSuperDrink = new Box<>(new Coffee());
        boxSuperDrink.setValue(new Coffee()); // consumer - super
        boxSuperDrink.setValue(new Tea(""));
        Object d = boxSuperDrink.getValue();
    }

    private static void makeDrink(Box<? extends Drink> box) {
        box.getValue().drink();
    }

    private static void demo01() {
        Person[] people = {
                new Person("Вася", 10),
                new Person("Маша", 30),
                new Person("Даша", 41),
                new Person("Рома", 66),
                new Person("Рома", 16),
        };

        sort(people, (p1, p2) ->
//				((Person)p1).getName().compareTo(((Person)p2).getName())
//                        ((Person) p1).getAge() - ((Person) p2).getAge()
                        p1.getAge() - p2.getAge()
        );

        for (Person p : people) {
            System.out.println(p);
        }
        String[] strings = {"zero", "one", "two", "three" };//, 1, true, people[0]};
        ObjectCompare<String> stringCompare = new StringCompare();
        sort(strings, stringCompare);
//		sort(strings, (Object s1, Object s2) -> {
//			if(!(s1 instanceof String)) {
//				return -1;
//			}
//			if(!(s2 instanceof String)) {
//				return -1;
//			}
//			return ((String)s1).compareTo((String)s2);});
        for (Object str : strings) {
            System.out.println(str);
        }
    }

    static class StringCompare implements ObjectCompare<String> {

        @Override
        public int сompare(String s1, String s2) {
            return s1.compareTo(s2);
//            if (!(s1 instanceof String)) {
//                return -1;
//            }
//            if (!(s2 instanceof String)) {
//                return -1;
//            }
//            return ((String) s1).compareTo((String) s2);
        }
    }

    interface ObjectCompare<T> {
        // Если p1 > p2 - возвращается число больше 0
        // Если p1 < p2 - возвращается число меньше 0
        // Если p1 == p2 - возвращается 0
        int сompare(T p1, T p2);
    }

    private static <T> void sort(T[] arr, ObjectCompare<T> cmp) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 1; j < arr.length; j++) {
                if (cmp.сompare(arr[j - 1], arr[j]) > 0) {
                    T tmp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

}

class Nail {

}

class Point<T1, T2> {
    private T1 x;
    private T2 y;

    public Point(T1 x, T2 y) {
        this.x = x;
        this.y = y;
    }

    public T1 getX() {
        return x;
    }

    public void setX(T1 x) {
        this.x = x;
    }

    public void show() {
        System.out.println(x.getClass().getSimpleName());
        System.out.println(y.getClass().getSimpleName());
    }
}

class Box<T> {
    private T value;

    public Box(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

abstract class Drink {
    public abstract void drink();
}

class Coffee extends Drink {

    @Override
    public void drink() {
        System.out.println("Drink coffee");
    }
}

class Tea extends Coffee {
    private String name;
    private int weight;

    public Tea(String name) {
        this.name = name;
        this.weight = 100;
    }

    @Override
    public void drink() {
        System.out.println("Tea.drink");
        weight -= 10;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Person {
    private static int count;
    private int id;
    private String name;
    private int age;

    public Person(String name, int age) {
        this.id = ++count;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
