package org.itstep;

public class MyArrayList<T> {

	// Задание 1 Создать два конструктора.
	/**
	 * По умолчанию (без параметров). Который выделяет память под массив на 10 элементов,
	 * равных нулю (capacity = 10, size = 0). Переиспользовать конструктор с параметрами
	 * для уменьшения кода
	 */
	public MyArrayList() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * С параметром типа int. Задающего начальную емкость массива. Принимает один параметр
	 * (задает capacity), выделяет память под массив (size = 0).
	 * @param size - размер списка
	 */
	public MyArrayList(int size) {
		throw new RuntimeException("Not implemented yet");
	}

	// Задание 2 Реализовать методы

	/**
	 * геттеры для size. Сеттера для size не должно быть!
	 * @return размер списка
	 */
	public int getSize() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * переопределить метод toString и реализовать строковое представление элементов
	 * массива через пробел.
	 * @return строковое представление списка
	 */
	@Override
	public String toString() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * ensureCapacity – закрытый метод! проверяет, достаточно ли резерва памяти для
	 * хранения указанного в параметре количества элементов. Если значение параметра
	 * меньше текущего capacity, то ничего не происходит. Если значение параметра больше
	 * текущего capacity, то массив пересоздает- ся, памяти выделяется в 1,5 раза + 1
	 * элемент больше. Существующие элементы переносятся в новый массив. Существующие
	 * элементы не должны быть потеряны.
	 */
	private void ensureCapacity() {
		throw new RuntimeException("Not implemented yet");
	}

	// Задание 3. Реализовать методы

	/**
	 * добавление элемента в конец массива. Должна быть проверка, достаточно ли памяти!
	 * Если памяти не достаточно увеличить емкость массива данных
	 * @param item элемент списка
	 */
	public void pushBack(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * удаление первого элемента из массива
	 * @return удаленный элемент списка
	 */
	public T popFront() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * добавление нового элемента в начало массива
	 * @param item элемент списка
	 */
	public void pushFront(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * вставка нового элемента в массив по указанному индексу, с проверкой на выход за
	 * пределы массива
	 * @param item элемент списка
	 * @param index индекс вставки
	 */
	public void insert(T item, int index) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * удаление одного элемента по указанному индексу. Должна быть проверка на
	 * допустимость индекса
	 * @param index индекс массива
	 * @return удаленный элемент или null
	 */
	public T removeAt(int index) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * удаление одного элемента, значение которого совпадает со значением переданного
	 * параметра
	 * @param item элемент массива
	 * @return индекс удаленного элемента или -1
	 */
	public int remove(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * удаление всех элементов, значения которых совпадает со значением переданного
	 * параметра
	 */
	public void removeAll(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * удаление последнего элемента из массива
	 * @return удаленный элемент с конца списка или null
	 */
	public T popBack() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * обнуление массива – всем элементам массива по индексам от 0 до size-1 присвоить
	 * значение null, полю size присвоить значение 0
	 */
	public void clear() {
		throw new RuntimeException("Not implemented yet");
	}

	// Задание 4 Реализовать методы:
	/**
	 * Проверка на пустой список
	 * @return метод возвращает true, если size = 0, и false в обратном случае
	 */
	public boolean isEmpty() {
		return true;
	}

	/**
	 * метод подгоняет значение capacity под size, естественно с перевыделением памяти
	 */
	public void trimToSize() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * линейный поиск слева направо первого вхождения в массив указанного значения.
	 * @param item элемент списка
	 * @return элемента, а eсли ничего не найдено, вернуть -1
	 */
	public int indexOf(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * линейный поиск справа налево вхождения в массив указанного значения.
	 * @param item элемент списка
	 * @return В результате работы вернуть индекс найденного элемента, а eсли ничего не
	 * найдено, вернуть -1
	 */
	public int lastIndexOf(T item) {
		throw new RuntimeException("Not implemented yet");
	}

	// Задание 5 Реализовать методы:
	/**
	 * изменение порядка следования элементов в массиве на противоположный
	 */
	public void reverse() {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * случайное перемешивание элементов массива
	 */
	public void shuffle() {
		throw new RuntimeException("Not implemented yet");
	}

	// Задание 6 Реализовать методы
	/**
	 * Метод сравнивает массивы не только по количеству элементов, но и по их содержимому
	 * @param other - в качестве параметра передается ссылка на другой объект класса
	 * MyArrayList.
	 * @return true - если элементы одинаковы по количеству и по содержимому
	 */
	public boolean equals(MyArrayList<T> other) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * возврат копии элемента массива по указанному индексу, с проверкой на выход за
	 * пределы массива
	 * @param index - индекс списка
	 * @return найденный элемент списка или null
	 */
	public T getElementAt(int index) {
		throw new RuntimeException("Not implemented yet");
	}

	/**
	 * переопределить метод clone – метод создает точную копию MyArrayList и возвращает
	 * ссылку на эту копию (неглубокое копирование).
	 * @return копию списка
	 * @throws CloneNotSupportedException
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
