package org.itstep;

// Импорт статических членов классов

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static java.lang.Math.min;
import static java.lang.System.out;
import static org.itstep.Logger.getInstance;

public class App {
    public static void main(String[] args) {

        // demo01();
        // demo02();

        // Порядок вызовов конструкторов

        //Animal animal = new Animal();
        out.println();
        // Блоки инициализации
        Cat cat = new Cat();
        cat.say();
        cat.jump();

        // DRY    - Don`t repeat yourself
        // KISS   - Keep it stupid simple
        // YAGNI  - You aren't gonna need it
        //
        // S - Единство ответственности
        // O - Принцип открытости для модификации, закрытости для изменений
        // L - Принцип подстановки Барбары Лисков
        Animal animal = cat;
        Object object = animal;

        Cat cat2 = (Cat)animal;

        animal = new Dog();
        animal.say();
        // I - Разделение интерфейсов
        // D - Инверсия зависимости

        // Диаграмма классов

        // Абстрактный класс - класс, экземпляр которого невозможно
        // создать


        // Аргрегация и композиция
        // Перечисления

    }

    private static void demo02() {
        // Глобальный объект
        Logger logger = Logger.getInstance();
        logger.info("Сообщение обычное");

        getInstance().error("Сообщение с ошибкой");

        out.println(min(1, 2));
    }

    private static void demo01() {
        System.out.println(Student.getCounter());
        Student s1 = new Student("Вася", "Пупкин", 21);
        Student s2 = new Student("Маша", "Распутина", 91);
        System.out.println(Student.getCounter());

        System.out.println("s1 = " + s1);
        System.out.println("s2 = " + s2);
    }
}

// Реализация паттерна Одиночка (Singleton)
class Logger {
    private static Logger instance;

    public static Logger getInstance() {
        if(instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    private Logger() {
    }

    public void info(String message) {
        System.out.println(this);
        System.out.println(message);
    }

    public void error(String message) {
        System.out.println(this);
        System.err.println(message);
    }
}

abstract class Animal {
    private static int counter = 1;

    private String name;

    // статический блок нужен для инициализации статических полей
    static {
        out.println("Animal: First static block");
    }

    // нестатический блок инициализации
    {
        out.println("Animal: First nostatic block");
    }

    public Animal() {
        say();
        counter++;
        out.println("Constructor: Animal created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCounter() {
        return counter;
    }

    // абстрактные методы
    public abstract void say();

    static {
        out.println("Animal: Second static block");
    }

    {
        out.println("Animal: Second no static block");
    }
}

class Cat extends Animal {
    static {
        out.println("Cat: First static block");
    }

    {
        out.println("Cat: First nostatic block");
    }

    public Cat() {
        super();
        out.println("Constructor: Cat created");
    }

    public void jump() {
        out.println("Cat jumped");
    }

    @Override
    public void say() {
        out.println("Мяу");
    }

    static {
        out.println("Cat: Second static block");
    }

    {
        out.println("Cat: Second nostatic block");
    }
}

class Dog extends Animal {

    @Override
    public void say() {
        out.println("Гав-гав");
    }
}

// Immutable object - String
// Mutable object - StringBuilder
class Student {
    private static int counter;

    private final int id;
    private final String firstName;
    private final String lastName;
    private final int age;

    public Student(String firstName, String lastName, int age) {
        this.id = ++counter;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    static int getCounter() {
        return counter;
    }

    public int getId() {
//        System.out.println("Invoke method getId()");
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}

