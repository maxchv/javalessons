package org.itstep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapeTest {

    @Test
    void testConstructorCircle() {
        Shape circle = new Circle(1);
        assertEquals(1, ((Circle)circle).getRadius());
    }

    @Test
    void testCircleArea() {
        Shape circle = new Circle(1);
        assertEquals(Math.PI, circle.area());
    }

    @Test
    void testCirclePerimeter() {
        Shape circle = new Circle(1);
        assertEquals(Math.PI * 2, circle.perimeter());
    }

    @Test
    void testCircleToString() {
        Shape circle = new Circle(10);
        assertEquals("Circle: radius = 10.0", circle.toString());
    }

    @Test
    void testConstructorSquare() {
        Shape square = new Square(2);
        assertEquals(2, ((Square)square).getSide());
    }

    @Test
    void testSquareArea() {
        Shape square = new Square(2);
        assertEquals(4, square.area());
    }

    @Test
    void testSquarePerimeter() {
        Shape square = new Square(3);
        assertEquals(12, square.perimeter());
    }

    @Test
    void testSquareToString() {
        Shape circle = new Square(5);
        assertEquals("Square: side = 5.0", circle.toString());
    }
}
