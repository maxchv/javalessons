package org.itstep;


import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Gender gender;// = Gender.MALE;

        Gender[] genders = Gender.values();
        for (Gender g: genders) {
            System.out.println(g.ordinal() + ") " + g
//                    + ": " + g.getName()
            );
        }
        System.out.print("Введите свой пол: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        gender = Gender.valueOf(input);

        switch (gender) {
            case FEMALE:
                System.out.println("Female");
                break;
            case MALE:
                System.out.println("Male");
                break;
            case UNKNOWN:
                System.out.println("Unknown");
                break;
            default:
                System.out.println("I don`t undestend you");
                break;
        }

        System.out.println("Male" == new String("Male"));
        System.out.println(gender == Gender.MALE);
    }
}

enum Gender {
    MALE("Мужской"), FEMALE("Женский"), UNKNOWN("Неопределенный");

    private final String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return  name;
    }

    @Override
    public String toString() {
        return name;
    }
}
