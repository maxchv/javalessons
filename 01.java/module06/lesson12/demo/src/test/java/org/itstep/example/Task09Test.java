package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование метода, который возвращает истину, если в массиве есть
 * отрицательные значения
 */
public class Task09Test {

    @Test
    public void testNegativExist() {
        int[] input = {1, -1};
        boolean result = Task09.isNegativeExist(input);
        assertTrue(result);
    }

    @Test
    public void testNegativNotExist() {
        int[] input = {1, 2};
        boolean result = Task09.isNegativeExist(input);
        assertFalse(result);
    }

    @Test
    public void testEmptyArray() {
        int[] input = {};
        boolean result = Task09.isNegativeExist(input);
        assertFalse(result);
    }

    @Test
    public void testArrayIsNull() {
        int[] input = null;
        boolean result = Task09.isNegativeExist(input);
        assertFalse(result);
    }
}