package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Метод getEven должен возвращать новый массив в котором содержатся все четные
 * числа из исходного массива mas. Если четных чисел в массиве нет, метод должен
 * возвращать null.
 */
public class Task10Test {

    @Test
    public void testEvenExist() {
        int[] input = {1, 2};
        int[] result = Task10.getEven(input);
        assertArrayEquals(result, new int[]{2});
    }

    @Test
    public void testNotNull() {
        int[] input = {1, 2};
        int[] result = Task10.getEven(input);
        assertNotNull(result);
    }

    @Test
    public void testTwoEvenExist() {
        int[] input = {1, 2, 3, 4};
        int[] result = Task10.getEven(input);
        assertArrayEquals(result, new int[]{2, 4});
    }

    @Test
    public void testEvenNotExist() {
        int[] input = {1, 3};
        int[] result = Task10.getEven(input);
        assertNull(result);
    }
}