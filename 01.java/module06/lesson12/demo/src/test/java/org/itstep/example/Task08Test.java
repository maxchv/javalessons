package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Метода getHalf, должен возвращать первую половину строки. Если число символов
 * в строке не четное, в первую половину должен входить центральный символ. Если
 * строка пустая или null метод должен вернуть пустую строку.
 */
public class Task08Test {

    @Test
    public void testAbcde() {
        String input = "ABCDE";
        String result = Task08.getHalf(input);
        assertEquals("Неверный результат", result, "ABC");
    }

    @Test
    public void testAbcd() {
        String input = "ABCD";
        String result = Task08.getHalf(input);
        assertEquals("Неверный результат", result, "AB");
    }

    @Test
    public void testA() {
        String input = "A";
        String result = Task08.getHalf(input);
        assertEquals("Неверный результат", result, "A");
    }

    @Test
    public void testEmpty() {
        String input = "";
        String result = Task08.getHalf(input);
        assertEquals("Неверный результат", result, "");
    }

    @Test
    public void testNull() {
        String input = null;
        String result = Task08.getHalf(input);
        assertEquals("Неверный результат", result, "");
    }
}