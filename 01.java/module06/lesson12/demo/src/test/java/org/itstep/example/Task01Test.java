package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.assertSame;

/**
 * Проверка корректности работы метода, используя тестовый метод.
 */
public class Task01Test {

    /**
     * Тестирующий метод
     */
    @Test
    public void testMethod() {
        // подготовка входных данных
        int input1 = 2;
        int input2 = 2;
        // вызов тестируемого метода и получение результата
        int actual = Task01.test(input1, input2);
        // сверка результата с требованиями к работе метода
        assertSame("Неверная сумма, результат должен быть равен 4", 4, actual);
    }
}