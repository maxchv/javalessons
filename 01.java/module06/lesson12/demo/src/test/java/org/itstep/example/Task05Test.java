package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.assertSame;

/**
 * Тестирование времени работы метода.
 */
public class Task05Test {

    /**
     * Тестирующий метод проверяет что при указанных входных данных, метод
     * бросит исключение во время выполнение программы
     */
    @Test(expected = ArithmeticException.class)
    public void testMethod() {
        // подготовка входных данных
        int input1 = 2;
        int input2 = 0;
        // вызов тестируемого метода и получение результата
        int actual = Task05.test(input1, input2);
        // сверка результата с требованиями к работе метода
        assertSame(2, actual);
    }
}