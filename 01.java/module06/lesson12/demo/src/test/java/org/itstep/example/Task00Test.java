package org.itstep.example;

// статический импорт

import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Тестовый метод
 */
public class Task00Test {
    @Test
    public void testMethod() {
        System.out.println("Тестовый метод запущен");
        fail("Комментарий на случай провала теста");
        // FIXME: закомментируй строчку выше
    }
}