package org.itstep.example;

import org.junit.Test;

import static org.junit.Assert.assertSame;

/**
 * Тестирование времени работы метода.
 */
public class Task02Test {

    /**
     * Тестирующий метод проверяет результат, а так же чтобы метод выполнялся не
     * дольше 1 миллисекунды.
     */
    @Test(timeout = 1)
    public void testMethod() {
        // подготовка входных данных
        int input1 = 2;
        int input2 = 2;
        // вызов тестируемого метода и получение результата
        int actual = Task02.test(input1, input2);
        // сверка результата с требованиями к работе метода
        assertSame(40, actual);
    }
}