package org.itstep.example;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @RunWith позволяет объеденить запуск нескольких тестовых классов в один тест
 */
@Suite.SuiteClasses({Task01Test.class, Task08Test.class})
@RunWith(Suite.class)
public class Task11Test {

}