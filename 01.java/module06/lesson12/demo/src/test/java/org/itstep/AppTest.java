package org.itstep;

import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {

    @Test // аннотация
    public void add() {
        int expected = 6;
        int a = 2, b = 4;
        int actual = App.add(a, b);
//        if(actual != expected) {
//            System.err.println("Метод add() работает не верно");
//        }
        assertEquals("Метод add() работает не верно", expected, actual);
    }

    // TDD - Test Driven Development (Разработка через тестирование)
    // Красный - тест не компилируется
    // Желтый - тест компилируется, но не проходит
    // Зеленый - тест проходит
    @Test
    public void sub() {
        int a = 5, b = 3;
        int expected = 2;
        int actual = App.sub(a, b);
        assertEquals("Метод sub() работает не верно", expected, actual);
    }

    @Test
    public void isLucky() {
        assertTrue("Это счастливое число", App.isLucky(123321));
        assertFalse("Это НЕ счастливое число", App.isLucky(122321));
    }
}
