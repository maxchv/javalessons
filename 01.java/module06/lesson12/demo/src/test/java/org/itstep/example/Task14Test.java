package org.itstep.example;

// статический импорт

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Игнорирование тестового метода
 */
public class Task14Test {
    @Ignore("Причина игнорирования")
    @Test
    public void testIgnoredMethod() {
        fail("Опа-па");
    }

    @Test
    public void testFailMethod() {
        fail("тест");
    }

    @Test
    public void testMethod() {

    }

}