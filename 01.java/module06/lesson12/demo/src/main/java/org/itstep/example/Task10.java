package org.itstep.example;

public class Task10 {
    // FIXME исправьте метод, чтобы все тесты проходили
    public static int[] getEven(int[] mas) {
        int count = 0;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] % 2 == 0) {
                count++;
            }
        }
        int[] result = new int[count];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                if (mas[j] % 2 == 0) {
                    result[i] = mas[j];
                }
            }
        }
        return result;
    }
}
