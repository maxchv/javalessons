package org.itstep.example;

public class Task05 {
    /**
     * Тестируемый метод
     */
    public static int test(int value1, int value2) {
        return value1 / value2;
    }

}
