package org.itstep.example;

public class Task01 {
    /**
     * Тестируемый метод.
     * Возвращает сумму входных параметров
     */
    public static int test(int value1, int value2) {
        // FIXME исправь код метода таким образом, чтобы тест проходил.
        // Напиши еще три тестовых метода проверяющих работу метода test
        return value1 + value2 + 1;
    }
}
