package org.itstep.example;

import java.security.InvalidParameterException;

public class Task15 {
    /**
     * Метод calc производит целочисленные арифметические операции над входными
     * значениями из переменных value1 и value2. Тип операции задается символом
     * в переменной operator. Метод возвращает результат вычисления операции.
     */
    public static int calc(int value1, int value2, char operator) {
        switch (operator) {
            case '+':
                return value2 + value1;
            case '-':
                return value2 - value1;
            case '*':
                return value2 * value1;
            case '/':
                return value2 / value2;
        }
        // бросание исключительной ситуации
        throw new InvalidParameterException("Неверная операция");
    }
}
