package org.itstep;

import okhttp3.OkHttpClient;

/**
 * Hello world!
 */
public class App {
    /*
       Тестирование - проверка соответствия фактического результата ожидаемому
     */
    public static void main(String[] args) {

    }

    static int add(int a, int b) {
        int c = a + b;
//        if(b == 4) {
//            return 5;
//        }
        return c;
    }

    static boolean isLucky(int number) {
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 1; i <= 6; i++) {
            if (i <= 3)
                sum1 = (sum1 + number % 10);
            else
                sum2 = (sum2 + number % 10);
            number = number / 10;
        }
        return sum1 == sum2;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}
