package org.itstep.example;

public class Task02 {
    /**
     * Тестируемый метод
     */
    public static int test(int value1, int value2) {
        int sum = 0;
        int n = 10_000;
        for (int i = 0; i < n; i++) {
            sum += value1 * value2;
        }
        return sum;
    }

}
