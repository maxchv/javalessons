package org.itstep.example;

public class Task08 {
    // FIXME Исправь метод getHalf чтобы он работал согласно требованиям к
    // задаче и тестам
    public static String getHalf(String value) {
        int mid = value.length() / 2;
        return value.substring(mid);
    }
}
