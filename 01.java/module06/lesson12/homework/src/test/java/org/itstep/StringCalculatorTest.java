package org.itstep;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThrows;

/**
 * Unit test for simple App.
 */
public class StringCalculatorTest {

    /*
    Добавить поддержку различных разделителей. Начало
    строки будет содержать подстроку, которая выглядит следу-
    ющим образом: "// [разделитель] \n [числа ...]". Разделитель
    не является обязательным. В качестве разделителя не долж-
    ны использоваться цифры. В случае если в строке встречает-
    ся символ, который не является цифрой или разделителем,
    вывести сообщение об ошибке. Все существующие до этого
    сценарии должны оставаться рабочими.

    Входные данные Результат
    "//;\n1; 2"     3
    "//* \n2,3"     5
    "//#\n3# 4"     7
    "//1\n1 1 1"    throw SpliterFormatException
    "//;\n1#2"      throw SpliterFormatException
     */
    @Test
    public void kata5() {
        assertEquals(3, StringCalculator.add("//;\n1; 2"));
        assertEquals(5, StringCalculator.add("//* \n2,3"));
        assertEquals(7, StringCalculator.add("//#\n3# 4"));
        assertEquals(6, StringCalculator.add("//[***]\n1 *** 2 *** 3"));
        assertEquals(20, StringCalculator.add("//[xy]\n3xy4xy5xy8"));
        assertThrows(SpliterFormatException.class, () -> {
            StringCalculator.add("//1\n1 1 1"); //?
        });
        assertThrows(SpliterFormatException.class, () -> {
            StringCalculator.add("//;\n1#2"); //?
        });
    }


}
