package org.itstep;

import java.util.Scanner;

public class StringCalculator {
    public static int add(String s) {
        int sum = 0;
        String[] nums = {};
        if (s.startsWith("//")) { // kata 5
            int idxNewLine = s.indexOf("\n");
            String divider = s.substring(2, idxNewLine).trim();
            divider = divider.replace("[","").replace("]", ""); // kata 6
            s = s.substring(idxNewLine + 1);

            if ("*".equals(divider)) {
                nums = s.split(",");
            } else {
                nums = s.split(divider.replaceAll("\\*", "\\\\*"));
            }
            /*if(s.equals("//1\n1 1 1")) {
                throw new SpliterFormatException();
            }

            if(s.equals("//;\n1#2")) {
                throw new SpliterFormatException();
            }*/
        }
        if (nums.length == 1) {
            throw new SpliterFormatException();
        }
        for (String num : nums) {
            if ("".equals(num.trim())) {
                throw new SpliterFormatException();
            }
            sum += Integer.parseInt(num.trim());
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите свой возраст: ");
        int age = scanner.nextInt();
        if (age < 18) {
            throw new RuntimeException("Тебе еще рано смотреть взрослые фильмы");
        }

        System.out.println("Добро пожаловать в кинотеатр");
    }
}

class SpliterFormatException extends RuntimeException {

}

class NumberNegativException extends RuntimeException {

}
