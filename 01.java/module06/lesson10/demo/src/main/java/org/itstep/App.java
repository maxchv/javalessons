package org.itstep;


import java.util.Arrays;
import java.util.Random;

public class App {

    // ООП - Объектно-ориентированное программирование
    //    - Инкапсуляция
    //    - Наследование
    //    - Полиморфизм
    //    - Абстракция

    // Переменная - именованная ячейка памяти
    // Метод - именованный блок кода
    //
    // DRY - не повторяй сам себя
    //
    // Объявление метода:
    // [<спецификатор доступа>] [<модификаторы>] <тип возвращаемого значения> <имя метода>([<тип параметра> <имя параметра>,...])
    // {
    //     <тело метода>
    // }
    //
    // Спецификаторы доступа (Инкапсуляция)
    //      public
    //      private
    //      protected
    //      <package private> or <default>
    //
    // Модификаторы:
    //      static - относится к классу, а не к экземпляру класса (объекту)
    //      abstract - абстрактный метод

    // Тип возвращаемого значения: любой допустимый тип данных Java (int, String, void...)
    //
    // Вызов метода:
    //     <имя метода>(аргументы);

    // initialise
    static int count; // Инициализируется 0 (значение по умолчанию для числовых типов данных)
    // Для boolean - false
    // Для классов - null

    static int summ(int a, int b) {
        count++;
        //System.out.println("a + b = " + (a + b));
        int c = a + b; // a, b, c - локальные переменные,
                       // которые существуют только в момент вызова метода
        return c;
    }

    /**
     * Создает массив случайных чисел в заданном диапазоне
     *
     * @param size - размер массива
     * @param from - начальный диапазон случайных чисел
     * @param to - конечный диапазон случайных чисел (включая)
     * @return - массив случайных чисел
     */
    static int[] randomNums(int size, int from, int to) {
        int count = 0;
        count++;     // локальная переменная
        App.count++; // переменная уровня класса
        int[] arr = new int[size];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(Math.abs(from - to) + 1) + Math.min(from, to);
        }
        return arr;
    }

    static int[] randomNums() {
        return randomNums(10, 1, 100);
    }

    static int[] randomNums(int size) {
        return randomNums(size, 1, 100);
    }

    public static void main(String[] args) {
        System.out.println(1);
        System.out.println("1");
        System.out.println();
        // demo1(); // вызов метода App.demo1();
        System.out.println("count = " + count);
        int d = summ(1, 2);
        System.out.println("d = " + d);
        System.out.println(summ(5, 2));
        summ(10, 1);
        System.out.println("count = " + count);

        int[] arr = randomNums(); //randomNums(10, 5, 10);
        System.out.println(Arrays.toString(arr));
    }

    static void demo1() {
        // Строки неизменные immutable
        // Для чего - безопасность (многопоточность)
        StringBuilder s = new StringBuilder("Hello World");
        s = new StringBuilder(s.toString().concat("!"));
        System.out.println(s);

        // StringBuffer - не рекомендуется
        StringBuilder stringBuilder = new StringBuilder(s.toString());
        stringBuilder.append("!");
        stringBuilder.replace(6, 12, "Java");
        stringBuilder.delete(0, 6);
        stringBuilder.insert(4, " is great");
        stringBuilder.reverse();
        System.out.println(stringBuilder);

        s = new StringBuilder();
        for (int i = 1; i <= 10; i++) {
            s.append("item ").append(i).append("\n");
        }
        System.out.println(s);

        Demo d = new Demo();
        d.notStaticMethod();
        Demo.staticMethod();
    }

}

class Demo {
    void notStaticMethod() {

    }

    public static void staticMethod() {
        System.out.println("Hello World");
    }
}
