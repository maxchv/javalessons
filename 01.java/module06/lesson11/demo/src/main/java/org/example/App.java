package org.example;

import java.util.Arrays;
import java.util.Random;

public class App {

    public static void main(String[] args) {
        int[] arr = new int[args.length];
        // Типичные задачи - обход дерева каталогов файловой системы
        //                 - построение фракталов
        //                 - обход бинарного дерева поиска
        //                 - быстрая сортировка
        // Алгоритмы Разделяй и Властвуй (Ханойская башня)
        Random rnd = new Random(1);
        arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt(10);
        }
        //        |
        // [0, 8, 9, 7, 5]
        // [0, 5, 7, 9, 8]
        // [0, 5, 7] [8, 9]
        System.out.println(Arrays.toString(arr));
        qs(arr);
        System.out.println(factorial(5));
        int[][] mat = {
                {5, 7, 1},
                {-4, 1, 0},
                {2, 0, 3},
        };
        int det = determinant(mat);
        System.out.println("det = " + det);
    }

    static int[][] minor(final int[][] mat, final int exclude) {
        int[][] m = new int[mat.length - 1][mat.length - 1];
        int ii = 0;
        for (int i = 0; i < mat.length; i++) {
            if (i == exclude) {
                continue;
            }
            m[ii++] = Arrays.copyOfRange(mat[i], 1, mat[i].length);
        }
        return m;
    }

    static int determinant(final int[][] mat) {
        if (mat.length == 2) {
            return mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
        }
        int sum = 0;
        for (int i = 0; i < mat.length; i++) {
            sum += (int) Math.pow(-1, i) * mat[i][0] * determinant(minor(mat, i));
        }
        return sum;
    }

    static int factorial(int num) {
        if (num == 1) {
            return 1;
        }
        return num * factorial(num - 1);
    }

    static void qs(int[] arr) {
        qs(arr, 0, arr.length - 1);
    }

    static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    static void qs(int[] arr, int l, int h) {
        int m = l + (h - l) / 2;
        int i = l, j = h;
        while (i <= j) {
            while (arr[i] < arr[m]) {
                i++;
            }
            while (arr[j] > arr[m]) {
                j--;
            }
            if (i <= j) {
                swap(arr, i, j);
                i++;
                j--;
            }
        }
        if (l < j) {
            qs(arr, l, j);
        }
        if (i > h) {
            qs(arr, i, h);
        }
    }
}
