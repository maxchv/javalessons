package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class Task01Test extends BaseTest {
    @Parameterized.Parameters(name = "Input = {0} Expected = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"5 1","5 4 3 2 1"},
                {"1 5","1 2 3 4 5"},
        });
    }

    private final String input;
    private final String expected;

    public Task01Test(String input, String expected) {
        super(null, Task01.class);
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if(testClass == null) return;
        String errorMessage = String.format("Ожидается, что для входных чисел %s вывод будет %s", input, expected);
        super.systemInputTest(input, expected, errorMessage);
    }
}