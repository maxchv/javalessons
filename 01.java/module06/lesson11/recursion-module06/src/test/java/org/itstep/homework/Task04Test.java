package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Task04Test extends BaseTest {
    @Parameterized.Parameters(name = "Input = {0} Expected = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"5 9", "35"},
                {"1 3", "6"},
        });
    }

    private final String input;
    private final String expected;

    public Task04Test(String input, String expected) {
        super(null, Task04.class);
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (testClass == null) return;
        String errorMessage = String.format("Ожидается, что для числа %s вывод будет %s", input, expected);
        super.systemInputTest(input, expected, errorMessage);
    }
}