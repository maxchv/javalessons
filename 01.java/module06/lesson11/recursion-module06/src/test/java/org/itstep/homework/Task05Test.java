package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Task05Test extends BaseTest {
    @Parameterized.Parameters(name = "Input = {0} Expected = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"radar", "YES"},
                {"mama", "NO"},
                {"madam", "YES"},
        });
    }

    private final String input;
    private final String expected;

    public Task05Test(String input, String expected) {
        super(null, Task05.class);
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (testClass == null) return;
        String errorMessage = String.format("Ожидается, что для слова %s вывод будет %s", input, expected);
        super.systemInputTest(input, expected, errorMessage);
    }
}