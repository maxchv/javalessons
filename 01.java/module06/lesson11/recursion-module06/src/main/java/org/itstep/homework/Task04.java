package org.itstep.homework;

import java.util.Scanner;

/**
 * Задание 4. Сумма чисел в диапазоне
 *
 * Вычислить сумму чисел в определенном диапазоне. Начало
 * и конец диапазона задается параметрами.
 * Ввод: 5 9
 * Вывод: 35
 */
public class Task04 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		sum(a, b);
	}

	static void sum(int a, int b) {
		// TODO: Ваш код пишите здесь
	}

}
