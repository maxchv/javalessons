package org.itstep.homework;

import java.util.Scanner;

/**
 * Задание 2. Точная степень двойки
 *
 * Дано натуральное число N. Выведите слово YES, если число
 * N является точной степенью двойки, или слово NO – в противном случае.
 * Операцией возведения в степень пользоваться нельзя!
 *
 * Ввод: 3
 * Вывод: NO
 */
public class Task02 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		isPow2(a);
	}

	static void isPow2(int a) {
		// TODO: Ваш код пишите здесь
	}

}
