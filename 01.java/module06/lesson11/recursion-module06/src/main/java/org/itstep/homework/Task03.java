package org.itstep.homework;

import java.util.Scanner;

/**
 * Задание 3. Сумма цифр числа
 *
 * Дано натуральное число N. Вычислите сумму его цифр.
 *
 * Ввод: 179
 * Вывод: 17
 */
public class Task03 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		sum(a);
	}

	static void sum(int a) {
		// TODO: Ваш код пишите здесь
	}

}
