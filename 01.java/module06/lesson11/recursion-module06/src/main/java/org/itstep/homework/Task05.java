package org.itstep.homework;

import java.util.Scanner;

/**
 * Задание 5. Палиндром
 *
 * Дано предложение, состоящее только из строчных латинских букв.
 * Проверьте, являются ли слова в предложении
 * палиндромом. Выведите YES или NO.
 *
 * Ввод: radar
 * Вывод: YES
 */
public class Task05 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String word = scanner.nextLine();
		isPalindrome(word);
	}

	static void isPalindrome(String word) {
		// TODO: Ваш код пишите здесь
	}
}
