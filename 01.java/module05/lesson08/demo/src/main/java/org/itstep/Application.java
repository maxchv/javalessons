package org.itstep;

import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
//        System.out.println("args: " + Arrays.toString(args));
//        // Перетвореня з рядка в потрібний примітивний тип відбувається за допомогою
//        // класу обгортки
//        int sum = 0;
//
//        // for, while, do..while
////        for (int i = 0; i < args.length; i++) {
////            String item = args[i];
////            sum += Integer.parseInt(item);
////        }
////        System.out.println("sum = " + sum);
////
//        // [foreach]
//        int max = Integer.parseInt(args[0]);
//        for (String item : args) {
//            sum += Integer.parseInt(item);
//            max = Math.max(Integer.parseInt(item), max);
//        }
//        System.out.println("sum = " + sum);
//        System.out.println("max = " + max);
//
//        int[] arr = {1, 2, 3, 4, 6};
//        System.out.println("arr = " + Arrays.toString(arr));
//        for (int item : arr) {
//            System.out.print(item * item + " ");
//        }

        int[] arr = {1, 5, 6, 8, 9, 3};
        //           1, 5, 6, 8, 3, 9
        //           1, 5, 6, 3, 8, 9
        //           1, 5, 3, 6, 8, 9
        //           1, 3, 5, 6, 8, 9
        // Типові алгоритми:
        // - Пошук елементів в масиві
        //      * Алгоритм лінейного пошуку в масиві O(n)
        //        big O
        int num = arr[0]; // O(1)

        int search = 8;
        int found = -1;
        for (int item : arr) {
            if (item == search) {
                found = item;
                break;
            }
        }
        System.out.println("found = " + found);

        //      * Алогоритм бінарного пошуку О(log2 n)
        int[] arr2 = {1, 3, 5, 6, 8, 9};
        int left = 0;
        int right = arr2.length - 1;
        int mid;
        do {
            mid = (left + right) / 2;
            if (arr2[mid] == search) {
                found = arr2[mid];
                break;
            } else if (arr2[mid] < search) {
                left = mid;
            } else {
                right = mid;
            }
        } while (true);
        System.out.println("arr[" + mid + "] = " + found);

        // - Сортування елементів в масиві
        System.out.println("Before: " + Arrays.toString(arr));
        //      * Бульбашки, Виборкою, Вставками: O(n^2)
        //      * Швидке сортування (quick sort)
        //for (int i = 0; i < arr.length; i++) {
        int i = 0;
        while(true) {
            boolean shift = false;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    shift = true;
                }
            }
            i++;
            if(!shift) {
                break;
            }
            System.out.println(Arrays.toString(arr));
        }
        System.out.println("After: " + Arrays.toString(arr));

    }
}
