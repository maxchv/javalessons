package ua.step.homework;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 
 * Создать массив из 20 случайных чисел в диапазоне от -10 до 30. Написать
 * программу, определяющую сумму элементов массива, находящихся в массиве после
 * первого отрицательного элемента. Вывести на консоль полученный массив и
 * сумму.
 *
 * Пример вывода программы:
 *
 * [15, 3, 26, 2, 28, 4, 19, 5, 8, -3, 8, -5, 15, 2, 21, 6, -8, 12, 26, 15]
 * 92
 *
 */
public class Task02 {
	public static void main(String[] args) {
		// TODO: не менять стоки ниже - необходимо для тестирования
		//  @see ua.step.homework.TaskTest02
		long seed = args.length > 0 ? Long.parseLong(args[0]) : LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

		// Использовать для генерирования элементов массива
		Random rnd = new Random(seed);
		final int MIN = -10;
		final int MAX = 30;

		int[] arr;
		// TODO: Пишите код здесь

	}
}
