package ua.step.homework;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 
 * Создать массив из 20 случайных чисел в диапазоне от - 300 до 555. Написать
 * программу, копирующую один массив в другой следующим образом: сначала
 * копируются последовательно все элементы, большие 0, затем последовательно все
 * элементы, равные 0, а затем последовательно все элементы, меньшие 0. Вывести
 * исходный массив. Вывести результирующий массив.
 *
 * Пример вывода:
 *
 * [406, 552, -175, 195, 234, 239, -103, 265, -88, 496, 363, 329, 276, 534, 204, 319, 412, -44, 131, 12]
 * [406, 552, 195, 234, 239, 265, 496, 363, 329, 276, 534, 204, 319, 412, 131, 12, -175, -103, -88, -44]
 *
 */
public class Task04 {
	public static void main(String[] args) {
		// TODO: не менять стоки ниже - необходимо для тестирования
		//  @see ua.step.homework.TaskTest04
		long seed = args != null && args.length > 0 ? Long.parseLong(args[0]) : LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

		// Использовать для генерирования элементов массива
		Random rnd = new Random(seed);
		final int MIN = -300;
		final int MAX = 555;
		final int COUNT = 20;

		int[] arr;
		// TODO: Пишите код здесь

	}
}
