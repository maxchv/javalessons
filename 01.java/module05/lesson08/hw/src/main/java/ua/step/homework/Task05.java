package ua.step.homework;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Random;

/**
 * Создать массив из 20 случайных чисел в диапазоне от -10 до 20. Определить
 * максимальное количество подряд идущих положительных элементов, не прерываемых
 * ни нулями, ни отрицательными числами. Вывести на консоль исходный массив и
 * найденный фрагмент (числа через пробел).
 * <p>
 * Пример вывода:
 * <p>
 * [17, -10, 12, 8, 7, 10, 17, 8, 5, -8, 11, -7, 20, 14, 10, 0, -4, -3, -8, 6]
 * 12 8 7 10 17 8 5
 * <p>
 * 0    1    2  3  4   5   6  7  8   9  10  11  12  13  14 15  16  17  18 19
 * [17, -10, 12, 8, 7, 10, 17, 8, 5, -8, 11, -7, 20, 14, 10, 0, -4, -3, -8, 6]
 * ^
 * [ 1,  9,  11, 16,  20 ] <- end
 * [ 0,  2,  10, 12,  19 ] <- start
 * 1   7    1   4    1
 * ^ min
 */
public class Task05 {
    public static void main(String[] args) {
        // TODO: не менять стоки ниже - необходимо для тестирования
        //  @see ua.step.homework.TaskTest05
        long seed = args != null && args.length > 0 ? Long.parseLong(args[0]) : LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);

        // Использовать для генерирования элементов массива
        Random rnd = new Random(seed);
        final int MIN = -10;
        final int MAX = 20;
        final int COUNT = 20;

        int[] arr;
        // TODO: Пишите код здесь

    }
}
