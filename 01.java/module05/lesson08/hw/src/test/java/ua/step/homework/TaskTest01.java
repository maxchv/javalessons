package ua.step.homework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ua.step.BaseTest;
import ua.step.homework.Task01;

public class TaskTest01 extends BaseTest {
	private final String expected;

	public TaskTest01() {
		super(null, Task01.class);
		this.expected = "[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361]";
	}

	@Test
	public void testValues() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		super.invokeMain(this.args);
		String errorMessage = "Ожидается " + expected;
		String actual = outContent.toString();
		assertEquals(errorMessage, expected, actual.trim());
	}
}
