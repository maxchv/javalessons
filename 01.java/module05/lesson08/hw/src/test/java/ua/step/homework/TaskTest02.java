package ua.step.homework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ua.step.BaseTest;
import ua.step.homework.Task02;

@RunWith(Parameterized.class)
public class TaskTest02 extends BaseTest {
	private final String expected;

	@Parameterized.Parameters(name = "seed = {0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{"0", "[16, 13, 7, 20, -2, 21, 4, 21, 12, 17, -10, 6, -1, 0, 0, 18, 0, 13, 11, 19]\r\n" +
						"131"},
				{"1", "[13, 28, 8, 4, 29, 10, 7, -1, 29, 27, -5, 21, -6, 10, 12, 19, 19, 7, 26, 7]\r\n" +
						"166"},
				{"3", "[18, 0, 5, -5, 10, -9, 2, 0, -6, 8, 26, -8, 2, -7, -9, 19, 22, 18, -4, 29]\r\n" +
						"93"},
		});
	}

	public TaskTest02(String seed, String expected) {
		super(new String[]{seed}, Task02.class);
		this.expected = expected;
	}

	@Test
	public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		super.invokeMain(this.args);
		String errorMessage = "Ожидается " + expected;
		String actual = outContent.toString();
		assertEquals(errorMessage, expected, actual.trim());
	}
}
