package ua.step.homework;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ua.step.BaseTest;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TaskTest04 extends BaseTest {
	private final String expected;

	@Parameterized.Parameters(name = "seed = {0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{"0", "[-300, 72, 521, -237, -81, -15, 367, 77, -53, -110, -127, -23, 133, 474, 467, -152, 232, 39, 485, 140]\r\n" +
						"[72, 521, 367, 77, 133, 474, 467, 232, 39, 485, 140, -300, -237, -81, -15, -53, -110, -127, -23, -152]"},
				{"1", "[-75, 72, -125, 197, -214, -76, 78, -62, 78, 376, 93, 373, -55, -77, -18, 390, -116, -78, 464, 433]\r\n" +
						"[72, 197, 78, 78, 376, 93, 373, 390, 464, 433, -75, -125, -214, -76, -62, -55, -77, -18, -116, -78]"},
				{"2", "[104, -64, -260, 111, 545, 106, 90, -5, 179, -224, 290, 42, 150, 226, 86, -20, 71, 411, 526, 51]\r\n" +
						"[104, 111, 545, 106, 90, 179, 290, 42, 150, 226, 86, 71, 411, 526, 51, -64, -260, -5, -224, -20]"},
		});
	}

	public TaskTest04(String seed, String expected) {
		super(new String[]{seed}, Task04.class);
		this.expected = expected;
	}

	@Test
	public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		super.invokeMain(this.args);
		String errorMessage = "Ожидается " + expected;
		String actual = outContent.toString();
		assertEquals(errorMessage, expected, actual.trim());
	}
}