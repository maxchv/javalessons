package ua.step.homework;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ua.step.BaseTest;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TaskTest05 extends BaseTest {
	private final String expected;

	@Parameterized.Parameters(name = "seed = {0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
				{"0", "[-8, -8, 10, 1, 3, -3, -9, -5, -3, 5, 7, 6, 17, -2, 12, -7, -2, 12, 20, 12]\r\n" +
						"5 7 6 17"},
				{"1", "[3, -10, 6, 15, 5, -4, -6, 8, -1, -2, -1, 18, -8, 17, 1, 4, -2, 3, -10, 11]\r\n" +
						"17 1 4"},
				{"2", "[-1, -8, 19, 2, -3, -2, 17, 2, -7, -10, -7, 14, 8, -9, 2, 0, -3, 5, 17, 17]\r\n" +
						"5 17 17"},
		});
	}

	public TaskTest05(String seed, String expected) {
		super(new String[]{seed}, Task05.class);
		this.expected = expected;
	}

	@Test
	public void test() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		super.invokeMain(this.args);
		String errorMessage = "Ожидается " + expected;
		String actual = outContent.toString();
		assertEquals(errorMessage, expected, actual.trim());
	}
}