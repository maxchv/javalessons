package ua.step.homework;

import java.util.Scanner;

/**
 * Задание: Вывести бинарное представление значения переменной value типа int, используя только
 * один цикл, управляющую переменную, принт, и побитовые операции. Не использовать готовые функции (Integer.toBinaryString()).
 * Число должно вводится с клавиатуры.
 * <p>
 * Пример работы программы:
 * Введите число: 10
 * 00000000000000000000000000001010
 * <p>
 * Введите число: 1
 * 00000000000000000000000000000001
 */
public class Task02 {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число: ");
        int number = scanner.nextInt();

        // TODO: Здесь Ваш код.

    }
}
