package ua.step.homework;

import java.util.Scanner;

/**
 * Написать программу для шифрования/дешифрования сообщений по алгоритму XOR
 * Строка для шифрования должна вводится с клавиатуры при запуске программы
 * После необходимо ввести пароль. Пароль - строка.
 * Каждый символ сообщения должен шифроваться последовательным символом пароля.
 * Например, если сообщение "Hello World", а пароль "pswd", то буква 'H' должна шифроваться
 * символом 'p', следующая буква 'e' должна шифроваться символом 's' и т.д.
 * <p>
 * Пример работы программы:
 * <p>
 * Введите сообщение: Hello World!
 * Введите пароль: 123
 * Результат: yW_]]f]A]V
 *
 * @see <a href='https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_XOR'>Алгоритм XOR</a>
 */
public class Task01 {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        System.out.print("Введите сообщение: ");
        final String message = args.length > 0 ? args[0] : scanner.nextLine();
        System.out.print("Введите пароль: ");
        final String password = args.length > 1 ? args[1] : scanner.nextLine();

        // TODO: Здесь Ваш код.

    }
}
