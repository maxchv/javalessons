package ua.step.homework;

import org.junit.Test;
import ua.step.BaseTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class Task01Test extends BaseTest {
	@Test
	public void test() {
		Task01.main(new String[]{"Hello World!", "123"});
		String errorMessage = "Wrong result";
		String expected = "yW_]]\u0013f]A]V\u0012";
		String output = outContent.toString();
		String[] parts = output.split(": ");
		assertEquals(4, parts.length);
		assertEquals(errorMessage, expected, parts[parts.length-1]);
	}
}
