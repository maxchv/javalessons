package ua.step.homework;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ua.step.BaseTest;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class Task02Test extends BaseTest {

    private final String number;
    private final String expected;

    @Parameterized.Parameters(name="{index}: {0} must be {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"1", "00000000000000000000000000000001"},
                {"2", "00000000000000000000000000000010"},
                {"3", "00000000000000000000000000000011"},
                {"4", "00000000000000000000000000000100"},
                {"5", "00000000000000000000000000000101"},
                {"6", "00000000000000000000000000000110"},
                {"7", "00000000000000000000000000000111"},
                {"8", "00000000000000000000000000001000"},
                {"9", "00000000000000000000000000001001"},
                {"10","00000000000000000000000000001010"},
        });
    }

    public Task02Test(String number, String expected) {
        this.number = number;
        this.expected = expected;
    }

    @Test
    public void test() {
        System.setIn(new java.io.ByteArrayInputStream(number.getBytes()));
        Task02.main(null);
        String actualResult = outContent.toString();
        int idx = actualResult.indexOf(":");
        String actual = actualResult.substring(idx + 1).trim();
        String errorMessage = "for " + number + " name must be " + expected + " actual " + actual;
        assertEquals(errorMessage, actual, expected);
    }
}
