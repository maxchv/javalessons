package org.itstep;

import java.util.Arrays;
import java.util.Random;

public class Application {
    public static void main(String[] args) {
//        // Array, масив: набор однотипних занчень
//        int a; // 4 байт
//        int[] arr;// = new int[10]; // int carr[];
//        arr = new int[10]; // 10 * 4 = 40 байт + додаткова службова пам'ять
//        // всі елементи мають значення за замовчуванням:
//        // - для числових типів (int, float, char..): 0
//        // - для логічного типу (boolean): false
//        // - для ссилочних типів: null
//        arr[0] = 10;
//        Random random = new Random();
//        int n = random.nextInt(100);
//        int[] narr = new int[n];
//        for (int i = 0; i < narr.length; i++) {
//            narr[i] = random.nextInt(1000);
//        }
////        for (int i = 0; i < narr.length; i++) {
////            System.out.println("narr[" + i + "] = " + narr[i]);
////        }
//        System.out.println(Arrays.toString(narr));
////        {
////            int[] tmp = new int[narr.length * 2];
////            // copy data from original array (arr)
////            for (int i = 0; i < narr.length; i++) {
////                tmp[i] = narr[i];
////            }
////            narr = tmp;
////        }
//        narr = Arrays.copyOf(narr, narr.length * 2);
//
//        int[] copy = narr;
//
//        System.out.println("narr.length = " + narr.length);

        /*
        Сгенерувати 10 пар випадкових чисел (від 2 до 10) таким чином щоб вони були унікальними
        (тобто 8 і 1 та 1 і 8 - це повтор). Вивести ці числа на консоль
         */
        int[] numbers1 = new int[10];
        int[] numbers2 = new int[10];
        Random random = new Random();
        for (int i = 0; i < numbers1.length; i++) {
            int a, b;
            boolean unique;
            do {
                a = random.nextInt(2, 10);
                b = random.nextInt(2, 10);
                unique = true;
                for (int j = 0; j < i; j++) {
                    if ((numbers1[j] == a && numbers2[j] == b) || (numbers1[j] == b && numbers2[j] == a)) {
                        unique = false;
                        break;
                    }
                }
            } while (!unique);
            numbers1[i] = a;
            numbers2[i] = b;
        }
        System.out.println(Arrays.toString(numbers1));
        System.out.println(Arrays.toString(numbers2));

    }
}
