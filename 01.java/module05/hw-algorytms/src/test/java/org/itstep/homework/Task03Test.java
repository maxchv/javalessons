package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task03Test extends BaseTest {
    @Test
    public void test() {
        Task03.main(new String[]{"10", "13"});
        String errorMessage = "Array is not right divide";
        String actual = outContent.toString().trim();
        String expected = "[[48, 8, 35, 52, 82, 58, 24, 71, 89, 8], [10, 32, 20]]";
        assertEquals(errorMessage, expected, actual);
    }
}
