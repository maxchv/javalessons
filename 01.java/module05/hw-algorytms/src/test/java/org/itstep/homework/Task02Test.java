package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class Task02Test  extends BaseTest {
    @Test
    public void test() {
        Task02.main(new String[]{"10"});
        String errorMessage = "Array is not sorted";
        String actual = outContent.toString().trim();
        String expected = "[[8, 8, 24, 35, 48, 52, 58, 71, 82, 89], [69, 57, 38, 34, 34, 32, 22, 20, 12, 10], [6, 21, 24, 36, 52, 64, 71, 77, 86, 94], [71, 62, 51, 40, 40, 19, 19, 18, 5, 4], [13, 25, 28, 31, 35, 43, 52, 56, 91, 94]]";
        assertEquals(errorMessage, expected, actual);
    }
}
