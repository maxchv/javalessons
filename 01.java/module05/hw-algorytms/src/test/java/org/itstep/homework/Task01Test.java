package org.itstep.homework;

import org.itstep.BaseTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class Task01Test extends BaseTest {
    @Test
    public void test() {
        Task01.main(null);
        String errorMessage = "Array is not sorted";
        String output = outContent.toString().replace("[", "").replace("]", "");
        int[] expected = IntStream.range(1, 101).toArray();
        int[] actual = Arrays.stream(output.split(",")).map(String::trim).mapToInt(Integer::parseInt).toArray();
        assertFalse(errorMessage, Arrays.equals(actual, expected));
    }
}
