package org.itstep.homework;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;

/**
 * Завдання 2
 * <p>
 * Заданий двовимірний масив заповнений випадковими числами
 * Потрібно кожний парний рядок відсортувати за допомогую алоритму виборки по зростанню значень,
 * а кожений непарний рядок відсортувати за допомогою алгоритму включенням за зменшенням значень
 */
public class Task02 {
    private static final int ROWS = 5;
    private static final int COLS = 10;

    public static void main(String[] args) {
        final Random random = new Random(args.length > 0 ? Integer.parseInt(args[0]) : new Date().getTime());
        final int[][] marr = new int[ROWS][COLS];

        // Заповніть масив викпадковими значеннями від 0 до 100


        // Пишіть ваш код тут


        // Це повинен бути останній рядок прогами
        System.out.println(Arrays.deepToString(marr));
    }
}
