package org.itstep.homework;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 * Завдання 3
 * <p>
 * Є одновимірний масив заповнений випадковими числами.
 * Перетворити цей масив на двовимірний з кількостю елементів в рядку не більше 10
 */
public class Task03 {
    public static void main(String[] args) {
        final Random random = new Random(args.length > 0 ? Integer.parseInt(args[0]) : new Date().getTime());
        final int COUNT = args.length > 1 ? Integer.parseInt(args[1]) : random.nextInt(101) + 15;
        final int MAX_COLS = 10;
        final int[] arr = new int[COUNT];

        int[][] marr = null; // тут повинен бути результат

        // Заповніть масив arr викпадковими значеннями від 0 до 100

        // Пишіть ваш код тут


        // Це повинен бути останній рядок прогами
        System.out.println(Arrays.deepToString(marr));
    }
}
