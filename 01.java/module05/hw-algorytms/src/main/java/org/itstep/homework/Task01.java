package org.itstep.homework;

import java.util.Arrays;
import java.util.Random;

/**
 * Завдання 01
 * <p>
 * Реалізувати алгоритм перемішування масива чисел arr
 */
public class Task01 {
    public static final int COUNT = 100;

    public static void main(String[] args) {
        final int[] arr = new int[COUNT];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i + 1;
        }
        // Пишіть ваш код тут


        // Це повинен бути останній рядок прогами
        System.out.println(Arrays.toString(arr));
    }
}
