package org.itstep;

import java.util.Arrays;
import java.util.Random;

public class Application {
    public static void main(String[] args) {
//        // Многомерные массивы
//        //     0  1  2  3  <- номер колонки
//        //  0  0  1  2  3
//        //  1  4  5  6  7
//        //  2  8  9 10 11
//        //  ^ номер строки
//        Random random = new Random();
//        int[][] marr = new int[3][];
//        marr[0] = new int[3];
//        marr[1] = new int[4];
//        marr[2] = new int[5];
//        int count = 0;
//        for(int i = 0; i<marr.length; i++) {
//            for(int j=0; j<marr[i].length; j++) {
//                marr[i][j] = random.nextInt(100); //count++;
//            }
//        }
//
////        System.out.println(Arrays.deepToString(marr));
//        for(int[] arr: marr) {
//            for(int item: arr) {
//                System.out.printf("%-3d", item);
//            }
//            System.out.println();
////            System.out.println(Arrays.toString(arr));
//        }
//        for (int[] arr : marr) {
//            Arrays.sort(arr);
//        }
////        System.out.println(Arrays.deepToString(marr));
//        for(int[] arr: marr) {
////            for(int item: arr) {
////                System.out.printf("%-3d", item);
////            }
//            System.out.println(Arrays.toString(arr));
//        }
//
//        //int[][] table = new int[10][10];
//
////        for (int i = 0; i < marr.length; i++) {
////            int[] arr = marr[i];
//        for(int[] arr: marr) {
//            for (int j = 0; j < arr.length; j++) {
//                System.out.printf("%-3d", arr[j]);
//            }
//            System.out.println();
//        }

//        int[][][] cube = new int[3][3][3];
//        System.out.println(Arrays.deepToString(cube));

        // [1, 2, 3, 4, 5, 6, 7]
        // [[1, 2, 3],
        // [4, 5, 6],
        // [7]]

        // Строки
        int[] arr = {'H', 'e', 'l', 'l', 'o'};
        System.out.println(Arrays.toString(arr));

        char[] chars = {'H', 'e', 'l', 'l', 'o'};
        System.out.println(chars);

        boolean bol;
        Boolean bbol;
        byte b;
        Byte bb;
        int a;     // примитивный тип данных
        Integer aa;// класс обертка
        a = Integer.parseInt("10");
        a = Integer.parseInt("101", 2);
        char ch;
        Character cch;
        float f;
        Float ff;
        double d;
        Double dd;

        System.out.println(Character.isJavaIdentifierPart('$'));
        System.out.println(Character.isJavaIdentifierPart('_'));
        System.out.println(Character.isJavaIdentifierPart('*'));

        String str = new String(chars);
        System.out.println(str);
        char[] chars1 = str.toCharArray();
        System.out.println(chars == chars1);
        System.out.println(Arrays.equals(chars, chars1));
//        System.out.println("Hello" == str); // ошибка!!!
        System.out.println("Hello".equals(str));


        String s1 = "Hello World";
        String s2 = new String(s1);//"Hello World";

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        System.out.println(s1.toUpperCase());
        System.out.println(s1.toLowerCase());
        for (int i = 0; i < s1.length(); i++) {
            System.out.println(s1.charAt(i));
        }
        System.out.println(s1.concat("!")); // s1 + "!"
        System.out.println(s1.contains("llo"));
        System.out.println(Arrays.toString(s1.getBytes()));

        String filename = "file.docx";
        if(filename.endsWith(".pdf")) {
            System.out.println("Open file with Acrobat Reader");
        } else if(filename.endsWith(".docx")) {
            System.out.println("Open file with MS Word");
        }

        System.out.println(s1.equalsIgnoreCase("hello world"));

        //    012345678910
        s1 = "I love Java and I love Life";
        int idx = s1.indexOf("love"); // 2
        System.out.println(idx);
        System.out.println(s1.substring(2, 6));
        idx = s1.indexOf("love", idx + 1); // 18
        System.out.println(idx);
        idx = s1.indexOf("love", idx + 1); // -1
        System.out.println(idx);
    }
}
