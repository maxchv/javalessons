package org.itstep;

import java.util.Arrays;
import java.util.Random;

public class App {

    static Random random = new Random();

    public static void main(String[] args) {
        // demo01();
        // demo02();

        // You should write DRY code, not WET
        // Don't Repeat Yourself (DRY)
        // Write Everything Twice (WET)
        // метод - іменований блок коду
        { // блок коду

        }
        method();
        App.method();
        int r;
        for (int i = 0; i < 10; i++) {
            r = nextInt(0, 5);
            System.out.println("r = " + r);
        }
        nextInt(10, 20);
        print("Test message");

        int[] rarr = nextIntArray(0, 10, 10);
        System.out.println("Arrays.toString(rarr) = " + Arrays.toString(rarr));
    }

    static void print(String msg) {
        System.out.println(msg);
    }

    static int[] nextIntArray(int a, int b, int count) {
        int[] arr = new int[count];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = nextInt(a, b);
        }
        return arr;
    }

    static int nextInt(int a, int b) {
        //System.out.println(random.nextInt(b - a + 1) + a);
        return random.nextInt(b - a + 1) + a;
    }

    static void method() {
        System.out.println("My method");
    }

    private static void demo02() {
        char[] cstr = {'H', 'e', 'l', 'l', 'o'};
        String s = "abcdb";
        System.out.println("cstr.length = " + cstr.length);
        System.out.println("s.length() = " + s.length());

        cstr = s.toCharArray();
        byte[] bytes = s.getBytes();
        System.out.println("bytes.length = " + bytes.length);

        for (int i = 0; i < s.length(); i++) {
            int code = s.codePointAt(i);
            char ch = s.charAt(i); // cstr[i]
            System.out.println(code + " -> " + ch);
        }

        // String is immutable, but arrays are mutable
        String s1 = s.toUpperCase();
        System.out.println("s1 = " + s1 + " s = " + s);

        s1 = s1.toLowerCase();
        System.out.println("s1 = " + s1);

        System.out.println("s1.indexOf('b') = " + s1.indexOf('b'));
        System.out.println("s1.indexOf('b', 2) = " + s1.indexOf('b', 2));
        System.out.println("s1.indexOf('e') = " + s1.indexOf('e'));

        System.out.println("s1.lastIndexOf(\"b\") = " + s1.lastIndexOf("b"));

        System.out.println("s1.contains(\"bc\") = " + s1.contains(s1.charAt(1) + ""));

        System.out.println("s1==\"abcdb\" = " + (s1 == "abcdb"));
        System.out.println("s1.equals(\"abcdb\") = " + s1.equals("abcdb"));

        s = "";
        for (int i = 0; i < 10; i++) {
            s += i;
        }
        System.out.println("s = " + s);

        s = " \t\r\n";
        System.out.println("s.length() = " + s.length());
        System.out.println("s.isEmpty() = " + s.isEmpty());
        System.out.println("s.isBlank() = " + s.isBlank()); // since 11

        // RegExp
        s = "Hello.World.I.Am.String";
        System.out.println("s = " + s);
        int idx = s.indexOf(".");
        System.out.println("idx = " + idx);
        System.out.println("s.substring() = " + s.substring(0, idx));
        int idx2 = s.indexOf(".", idx + 1);
        System.out.println("idx2 = " + idx2);
        System.out.println("s.substring() = " + s.substring(idx + 1, idx2));

        String[] words = s.split("\\."); // () [] ^ $ - |
        System.out.println("Arrays.toString(words) = " + Arrays.toString(words));

        s = "aaaAAAaaaAaaAAAA";
        String[] results = s.split("A");
        System.out.println("Arrays.toString(results) = " + Arrays.toString(results));
        s = "I love C++";
        System.out.println("s = " + s);
        s = s.replace("C++", "Java");
        System.out.println("s = " + s);
        s = "first second.third;fourth-fifth:sixth";
        results = s.split("[ .;:-]");
        System.out.println("Arrays.toString(results) = " + Arrays.toString(results));
        System.out.println("s.replaceAll(\"[ .;:-]\", \"\") = " + s.replaceAll("[ .;:-]", " "));

        String filename = "book.pdf";
        if (filename.endsWith(".pdf")) {
            System.out.println("Open in Acrobat Reader");
        } else if (filename.endsWith(".txt")) {
            System.out.println("Open in Notepad");
        } else if (filename.endsWith(".java")) {
            System.out.println("Open in ItelliJ IDEA");
        }
        String login = "    \t    admin \r\n";
        if ("admin".equals(login.trim())) {
            System.out.println("Welcome admin");
        }

        results = new String[]{"One", "Two", "Three"};
        String join = String.join(" ", results);
        System.out.println("join = " + join);

        // String pool - Object Pool
        int[] arr1 = {1, 2, 3};
        int[] arr2 = arr1;
        String one = "string";
        String two = new String("string");
        System.out.println("one == two: " + (one == two));
        System.out.println("one.equals(two): " + one.equals(two));

        // Compact String since 9
        // Java 8 String -> char array
        //      "hello" (5 symbols, 10 byte in memory)
        // Java 9 Compat String -> char or byte array
        //      "hello" (5 symbols, 5 byte in memory)
        //      "привіт" (6 symbols, 12 byte in memory)


        // StringBuilder (not StringBuffer)
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html>").append("\n")
                .append("<header>").append("\n")
                .append("   <title>My HTML Page</title").append("\n")
                .append("</header>").append("\n")
                .append("<body>").append("\n")
                .append("</body>").append("\n");
        int i = stringBuilder.indexOf("</body>");
        stringBuilder.insert(i, "   <h1>Hello World</h1>\n");
        System.out.println("stringBuilder = " + stringBuilder);

        stringBuilder = new StringBuilder();
        s = "";
        for (int j = 0; j < 10000; j++) {
            stringBuilder.append(j);
        }
        s = stringBuilder.toString();
        System.out.println("s = " + s);
    }

    private static void demo01() {
        // class Arrays
        int[] arr = {9, 3, 4, 4, 5, 6, 7, 8, 10};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println("Find index of 4");
        int idx = Arrays.binarySearch(arr, 4);
        System.out.println("idx = " + idx);
        System.out.println("arr[idx] = " + arr[idx]);

        System.out.println("Find index of 2");
        idx = Arrays.binarySearch(arr, 2);
        System.out.println("idx = " + idx);
        if (idx < 0) {
            System.out.println("Not found");
        }

        System.out.println("Equals");
        int[] arr2 = {1, 2, 3};
        int[] arr3 = {1, 2, 3};
        System.out.println("arr2 == arr3: " + (arr2 == arr3));
        System.out.println("Arrays.equals(arr2, arr3): " + Arrays.equals(arr2, arr3));

        int[] bigArr = new int[100];
        System.out.println("Arrays.fill()");
        Arrays.fill(bigArr, 31);
        System.out.println("Arrays.toString(bigArr) = " + Arrays.toString(bigArr));

        System.out.println("Copy arrays");
        int[] one = {1, 2, 3, 4, 5};
        int[] two = new int[3];
        System.arraycopy(one, 2, two, 1, 2);
        System.out.println("Arrays.toString(two) = " + Arrays.toString(two));

        int[] copy = Arrays.copyOf(one, 10);
        System.out.println("Arrays.toString(copy) = " + Arrays.toString(copy));

        copy = Arrays.copyOfRange(one, 2, 6); // length = to - from = 6 - 2 = 4
        System.out.println("Arrays.toString(copy) = " + Arrays.toString(copy));
    }
}
