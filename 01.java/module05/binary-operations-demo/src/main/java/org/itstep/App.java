package org.itstep;


import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        // Конкатенація: String + Object -> String
        System.out.println("a = " + 1);
        // Аріфметичні операції: +, -, *, /, ++, --, +=, % ...
        // Логічні операції: >, <, >=, <=, !=, ==
        // && (&), || (|), !

        // Бінарні операції:
        // | (бінарне або), & (бінерне і), ^ (виключне або),
        // ~ (інверсія), >> (зсув праворуч), >>> (зсув беззнаковий), << (зсув ліворуч)
        for (int i = 0; i < 128; i++) {
            System.out.printf("%4d|%8s\n", i, Integer.toBinaryString(i));
        }

        // бінарне або |
        //   100
        // |
        //   110
        // ------
        //   110
        int a = 4; // 0b100
        int b = 6; // 0b110
        int c = a | b;
        System.out.println(Integer.toBinaryString(a) + "(" + a + ")" + " | "
                + Integer.toBinaryString(b) + "(" + b + ")"
                + " = " + Integer.toBinaryString(c) + "(" + c + ")");

        // бінарне i &
        //   100
        // &
        //   110
        // ------
        //   100 (4)
        c = a & b;
        System.out.println(Integer.toBinaryString(a) + "(" + a + ")" + " | "
                + Integer.toBinaryString(b) + "(" + b + ")"
                + " = " + Integer.toBinaryString(c) + "(" + c + ")");

        final int READ_ONLY = 0b0001;
        final int ARCHIVE = 0b0010;
        final int SYSTEM = 0b0100;
        final int HIDDEN = 0b1000;
        int attributes = 0;
        
        //attributes |= READ_ONLY;
        attributes |= HIDDEN;
        System.out.println("Integer.toBinaryString(attributes) = " + Integer.toBinaryString(attributes));

        if((attributes & READ_ONLY) == READ_ONLY) {
            System.out.println("Read only");
        }
        if((attributes & ARCHIVE) == ARCHIVE) {
            System.out.println("Archive");
        }
        if((attributes & SYSTEM) == SYSTEM) {
            System.out.println("System");
        }
        if((attributes & HIDDEN) == HIDDEN) {
            System.out.println("Hidden");
        }

        // Хешування (отримання хеша): перетворення без можливості зворотнього перетворення

        // Шифрування (отримаємо шифр): перетворення данних з можливістю зворотнього перетворення
        // Шифр Цезаря
        //  а, б, в, г, ґ, д, е, є, ж, з, и, і, ї, й, к, л, м, н, о, п, р, с, т, у, ф, х, ц, ч, ш, щ, ь, ю, я.
        // де -> зи
        String message;
        int key;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Що потрібно зашифрувати? ");
        message = scanner.nextLine();
        System.out.print("Введіть ключ шифрування: ");
        key = scanner.nextInt();
        char[] arr = message.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            arr[i] ^= key;
        }
        System.out.println("Зашифроване повідомлення: ");
        System.out.print(arr);

        // бінарне або ^
        //   100  origin
        // ^
        //   110  key
        // ------
        //   010  encrypt
        //
        //   010  encrypt
        // ^
        //   110  key
        // -----------
        //   100  origin

//        a = 4; // 0b100
//        b = 6; // 0b110
//        c = a ^ b;
//        System.out.println(Integer.toBinaryString(a) + "(" + a + ")" + " ^ "
//                + Integer.toBinaryString(b) + "(" + b + ")"
//                + " = " + Integer.toBinaryString(c) + "(" + c + ")");

        // Шифр XoR
        //

        // Операція інверсія ~: 1 -> 0, 0 -> 1
        System.out.println();
        System.out.println("~");
        a = 0b1010;
        c = ~a;
        System.out.println("Integer.toBinaryString(c) = " + Integer.toBinaryString(c));
        System.out.println("c = " + c);

        // Операція зсуву праворуч >>
        System.out.println(">>");
        a = -4;
        System.out.println("a = " + a);
        System.out.println("Integer.toBinaryString(a) = " + Integer.toBinaryString(a));
        c = a >> 1;
        System.out.println("Integer.toBinaryString(c) = " + Integer.toBinaryString(c));
        System.out.println("c = " + c);

        // Операція зсуву праворуч беззнакова >>>
        System.out.println(">>>");
        a = -4;
        System.out.println("a = " + a);
        c = a >>> 1;
        System.out.println("Integer.toBinaryString(c) = " + Integer.toBinaryString(c));
        System.out.println("c = " + c);

        // Операція зсуву ліворуч <<
        System.out.println("<<");
        a = -4;
        System.out.println("a = " + a);
        c = a << 2;
        System.out.println("Integer.toBinaryString(c) = " + Integer.toBinaryString(c));
        System.out.println("c = " + c);
    }
}
