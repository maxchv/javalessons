package org.itstep;


import java.util.Arrays;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        int[] arr = {5, 7, 1, 2, 4, 3, 6};
        System.out.println(Arrays.toString(arr));

        // Сортування вибором
        System.out.println("Сортування вибором");
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                int tmp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = tmp;
            }
        }
        System.out.println(Arrays.toString(arr));

        arr = new int[]{5, 7, 1, 2, 4, 3, 6};
        // 0  1  2  3
        // 5, 7, 1, 2, 4, 3, 6
        // key = 1
        // 1, 5, 7, 2, 4, 3, 6

        // Сортування включенням
        System.out.println("Сортування включенням");
        // System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
            int j;
            boolean found = false;
            for (j = i + 1; j < arr.length - 1 && j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    found = true;
                    break;
                }
            }
            if (found) {
                int tmp = arr[j];
                while (j > 0 && arr[j - 1] > tmp) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = tmp;
            }
        }
        System.out.println(Arrays.toString(arr));

        arr = new int[]{5, 7, 1, 2, 4, 3, 6};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        // Алгоритм перемішування


        // Багатомірні масиви
        Random rnd = new Random();
        int[][] marr = new int[10][];
        for (int i = 0; i < marr.length; i++) {
            marr[i] = new int[i];
            for (int j = 0; j < marr[i].length; j++) {
                marr[i][j] = rnd.nextInt(100);
            }
        }
        //System.out.println(Arrays.deepToString(marr));
        for (int[] a : marr) {
            //System.out.println(Arrays.toString(a));
            for (int item : a) {
                System.out.printf("%5d", item);
            }
            System.out.println();
        }
    }
}
