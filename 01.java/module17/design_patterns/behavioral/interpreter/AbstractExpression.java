import java.util.List;

public abstract class AbstractExpression {
    abstract boolean interpret(String context);
}
