package org.itstep.lesson04;

public class App {
    public static void main(String[] args) {

        // цикли
//        System.out.println("1");
//        System.out.println("1");
//        System.out.println("1");
//        System.out.println("1");
//        System.out.println("1");

        // while
        int n = 0;
        System.out.println("-== while ==-");
        while (n < 5) { // цикл з передумовою
            System.out.print(n + " ");
            n += 2;
        }

        System.out.println("-== do..while ==-");
        // do..while
        do {
            System.out.println("n = " + n);
        } while (n < 5);

//        int hours;
//        Scanner scanner = new Scanner(System.in);
//        do {
//            System.out.print("Скільки зараз годин? ");
//            hours = scanner.nextInt();
//        } while (hours < 0 || hours > 23);
//        System.out.println("hours = " + hours);

        System.out.println("-== for ==-");
        // for - цикл з лічильником
        for (int i = 1; i <= 10; i++) {
            System.out.print(i * i + " ");
        }

        // continue & break

    }
}
