package org.itstep;

import java.math.BigDecimal;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        int time = 10;
        System.out.println(--time);
        System.out.println(time);

        char sym = 97;
        System.out.println(sym);

        int num = sym;

        sym = (char)num;

        boolean yes = (sym % 2) == 0; // четным
        System.out.println("yes = " + yes);

        int age = 18;
        long l = 10l;
        float $f = 3.14F;

        boolean isStudent18AgeOld = (age == 18);

        System.out.println("Студенву 18 лет? " + isStudent18AgeOld);

        // Логические операторы - возвращают true false > < >= <= != ==
        // Логическое и   &&
        // Логическое или ||
        // Логическое не  !

        int score = 9;
        System.out.println(age == 18 && score >= 10);

        age = 16;

        if(age >= 18) {
            System.out.println("Добро пожаловать в кино для взрослых");
        } else if(age >= 15) {
            System.out.println("Тебе можно посмотреть Джокера");
        } else {
            System.out.println("Тебе еще нужно смотреть мультики");
        }

        char keyCode = 'W';

        switch (keyCode){
            case 'w':
            case 'W':
                System.out.println("Move up");
                break;
            case 's':
                System.out.println("Move down");
                break;
            case 'a':
                System.out.println("Move left");
                break;
            case 'd':
                System.out.println("Move right");
                break;
            default:
                System.out.println("Default value: " + keyCode);
                break;
        }
        /*
        if(keyCode == 'w') {
            System.out.println("Move up");
        } else if(keyCode == 's') {
            System.out.println("Move down");
        } else if(keyCode == 'a') {
            System.out.println("Move left");
        } else if(keyCode == 'd') {
            System.out.println("Move right");
        } else {

        }*/

        int a = -age; // - унарный оператор
        boolean not = !true;

        a = a + age;

        double price = 1501; //BigDecimal

        price *= (price >= 1000) ? 0.9 : 0.99;
//        if(price >= 1000) {
//            price *= 0.9;
//        } else {
//            price *= 0.99;
//        }

        System.out.println("Price: " + price);

        // Работа с клавиатурой
        // System.out - стандартный поток вывода (по умолчанию консоль)
        // System.in  - стандартный поток ввода (по умолчанию консоль)
        Scanner scanner = new Scanner(System.in); // Создали объект Scanner
        // методы hasXXX() возвращают boolean и позовляют проверить наличие
        // данных тип XXX (Int, Short, Double...)
        System.out.print("Как тебя зовут? ");
        String name = scanner.nextLine(); // метод nextLine() считывает строку
        System.out.println("Привет " + name);
        System.out.print("Сколько тебе лет? ");
        age = scanner.nextInt();
        if(age > 55) {
            System.out.println("Ты находишья в зоне риска!!!");
        }

    }
}
