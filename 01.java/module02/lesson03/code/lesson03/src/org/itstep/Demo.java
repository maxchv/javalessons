package org.itstep;

import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        // 1. Математичні функції, класс Math
        System.out.println("");
        int a = 4;
        int b = (int) Math.sqrt(a);
        System.out.println("b = " + b);
        // приведення даних: явне і неявне

        short s = 100;
        int i;
        i = s; // неявне
        s = (short) i; // явне
        byte b1 = 127;

        a = Math.abs(-123); // absolute
        System.out.println("|a| = " + a);
        long ll = Math.abs(-1L);

        a = Math.max(12, 32);
        System.out.println("a = " + a);
        a = Math.min(12, Math.min(15, 16));
        System.out.println("a = " + a);

        // power - ступін
        a = (int) Math.pow(2, 3);
        System.out.println("a = " + a);
        double r = 2.5;
        double p = 2 * r * Math.PI;
        System.out.println("P = " + p);

        System.out.println("ceil(4.1) = " + Math.ceil(4.1));
        System.out.println("ceil(4.5) = " + Math.ceil(4.5));
        System.out.println("ceil(4.9) = " + Math.ceil(4.9));

        System.out.println("floor(4.1) = " + Math.floor(4.1));
        System.out.println("floor(4.5) = " + Math.floor(4.5));
        System.out.println("floor(4.9) = " + Math.floor(4.9));

        System.out.println("round(4.1) = " + Math.round(4.1));
        System.out.println("round(4.5) = " + Math.round(4.5));
        System.out.println("round(4.9) = " + Math.round(4.9));

        p = Math.round(p * 1000) / 1000.0;
        System.out.println("p = " + p);

        r = Math.random(); // (0..1)
        System.out.println("r = " + r);
        // випадкове число від 0 до 99
        i = (int) Math.floor(r * 100);
        System.out.println("i = " + i);

        // діапазон [20, 50] = [0, 30] + 20
        i = (int) Math.floor(r * 31) + 20;
        System.out.println("i = " + i);

        // a % b - залишок від цілочисленого діленя числа a на b
        long time = 10_000;
        long hours = time / 3600;
        long minutes = time / 60 % 60; //(time - hours * 3600) / 60;
        long seconds = time % 60; //(time - hours * 3600 - minutes * 60);
        System.out.println(hours + ":" + minutes + ":" + seconds);

        Scanner scanner = new Scanner(System.in);
        System.out.print("How old are you? ");
        int age;
        age = scanner.nextInt();
        System.out.println("You are " + age + " old");

        // 2. Логічний тип даних (boolean)

        // 3. Умовні оператори. if, else, else if

        // 4. Перемикач case
    }
}
