package org.itstep;


import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public class Application {

    static class StringOutputStream implements Closeable {

        private final OutputStream outputStream;

        public StringOutputStream(OutputStream outputStream) throws NoSuchFileException {
            this.outputStream = outputStream;
        }

        public void println(String message) throws IOException {
            outputStream.write((message + "\n").getBytes(StandardCharsets.UTF_8));
        }

        @Override
        public void close() throws IOException {
//            System.out.println("Closed");
            if(outputStream!=null) {
                outputStream.close();
            }
        }
    }

    public static void main(String[] args) throws IOException {

        //demo01();
        //demo02();
        //demo03();
//        try(Resource resource = new Resource()) {
//
//        } catch (NoSuchFileException e) {
//            e.printStackTrace();
//        }
        //demo04();
        //demo5();
        //demo6();
        //demo7();
        //demo8();
    }

    private static void demo8() throws IOException {
        Path path = Paths.get("index.html");
        if(Files.exists(path)) {
            System.out.println("File exists");
        }
        for(Path name: Files.list(Paths.get(".")).collect(Collectors.toSet())) {
            System.out.println(name);
        }
    }

    private static void demo7() throws IOException {
        File file = new File("filename.txt");
        file.createNewFile();
        if(file.exists() && file.isFile()) {
            System.out.println("Exists");
        }
        file.deleteOnExit();
        file = new File(".");
        for(String name: file.list()) {
            System.out.println(name);
        }
    }

    private static void demo6() throws IOException {
        try(InputStream in = new FileInputStream("access.log.gz");
            GZIPInputStream gzip = new GZIPInputStream(in);
            Reader reader = new InputStreamReader(gzip);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            for(int i=0; i<10; i++) {
                System.out.println(bufferedReader.readLine());
            }
        }
    }

    private static void demo5() throws IOException {
        URL url = new URL("http://httpbin.org");
        try(InputStream in = url.openStream();
            Reader rdr = new InputStreamReader(in);
            BufferedReader reader = new BufferedReader(rdr);
            PrintStream printStream = new PrintStream("index.html")) {
            String line;
            while((line = reader.readLine()) != null) {
                printStream.println(line);
            }
        }
    }

    private static void demo04() throws IOException {
        try (OutputStream out = new FileOutputStream("text.txt");
             //StringOutputStream stringStream = new StringOutputStream(out);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(out, 100);
             PrintStream printStream = new PrintStream(bufferedOutputStream);
             Scanner scanner = new Scanner(System.in)) {
            String message = scanner.nextLine();
            printStream.println(message); //out.write(message.getBytes(StandardCharsets.UTF_8));
            bufferedOutputStream.flush(); // принудительный сброс буфера
        }
    }

    private static void demo03() throws IOException {
        // try with resource statement
        try (OutputStream out = new FileOutputStream("text.txt");
             Scanner scanner = new Scanner(System.in);) {
            String message = scanner.nextLine();
            out.write(message.getBytes(StandardCharsets.UTF_8));
        }
    }

    private static void demo02() throws IOException {
        InputStream in = new FileInputStream("test.txt");

        int count;
        byte[] buff = new byte[5];
        count = in.read(buff);
        while (count > 0) {
//            System.out.println("Read " + count + " bytes");
            String line = new String(buff, 0, count, StandardCharsets.UTF_8);
            System.out.print(line);
            count = in.read(buff);
        }
        in.close();
    }

    private static void demo01() throws IOException {
        OutputStream out = null;
        try {
            out = new FileOutputStream("text.txt");
            Scanner scanner = new Scanner(System.in);
            String message = scanner.nextLine();
            out.write(message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            PrintWriter s = new PrintWriter(new File("error.log"));
            ex.printStackTrace(s);
            s.close();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
