package org.itstep;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.fail;

class ApplicationTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    void testClassExists() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("org.itstep.Fraction");
    }

    @Test
    void createInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<?> clazz = Class.forName("org.itstep.Fraction");
        Constructor<?> constructor = clazz.getConstructor(int.class, int.class);
        Object fraction = constructor.newInstance(1, 2);
    }

    @Test
    void isInitFields() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> clazz = Class.forName("org.itstep.Fraction");
        Constructor<?> constructor = clazz.getConstructor(int.class, int.class);
        Object fraction = constructor.newInstance(1, 2);
        Field aField = clazz.getDeclaredField("a");
        aField.setAccessible(true);
        aField.canAccess(fraction);
        Field bField = clazz.getDeclaredField("b");
        bField.setAccessible(true);
        Assertions.assertEquals(1, aField.get(fraction));
        Assertions.assertEquals(2, bField.get(fraction));
    }

    @AfterEach
    void tearDown() {

    }
}