package org.itstep;

import java.io.*;
import java.util.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Collection<Student> students = new ArrayList<>();
//        students.add(new Student(1, "Вася", 19, "1234"));
//        students.add(new Student(2, "Маша", 20, "4321"));
//        students.add(new Student(3, "Петя", 21, "eieie"));
//        save(students, "students.csv");
//        saveObject(students, "students.dat");
//        students = read("students.csv");
        students = readObject("students.dat");
        for (Student s: students) {
            System.out.println(s);
        }
    }

    private static Collection<Student> readObject(String filename) {
        List<Student> students = null;
        try(ObjectInput input = new ObjectInputStream(new FileInputStream(filename))) {
            students = (List<Student>) input.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return students;
    }

    private static void saveObject(Collection<Student> students, String filename) {
        try (ObjectOutput output = new ObjectOutputStream(new FileOutputStream(filename))) {
            output.writeObject(students);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Student> read(String filename) {
        List<Student> students = new ArrayList<>();
        try (Reader rdr = new FileReader(filename);
             BufferedReader buffRdr = new BufferedReader(rdr)) {
            String line;
            while ((line = buffRdr.readLine()) != null) {
                String[] fields = line.split(";");
                students.add(new Student(Integer.parseInt(fields[0]), fields[1], Integer.parseInt(fields[2]), ""));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return students;
    }

    private static void save(List<Student> students, String filename) {
        try (PrintStream stream = new PrintStream(filename)) {
            for (Student s : students) {
                stream.println(String.format("%d;%s;%d", s.getId(), s.getName(), s.getAge()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

class Student implements Serializable {

    private static final long serialVersionUID = 5908172130778071865L;

    private int id;
    private String name;
    private int age;
//    private String group;
    private transient String password;

    public Student(int id, String name, int age, String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void readObject(ObjectInputStream objectInput) throws IOException, ClassNotFoundException {
        objectInput.defaultReadObject();
//        this.id = objectInput.readInt();
//        this.name = objectInput.readUTF();
//        this.age = objectInput.readInt();
//        this.password = objectInput.readUTF();
        System.out.println("Connect to database...");
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.defaultWriteObject();
//        outputStream.writeInt(id);
//        outputStream.writeUTF(name);
//        outputStream.writeInt(age);
//        outputStream.writeUTF(password);
        System.out.println("Send email...");
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", password='" + password + '\'' +
                '}';
    }
}

class Test implements Externalizable {

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
}
