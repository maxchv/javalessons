package org.itstep;

import java.util.*;

public class MapLab {

    public static void main(String[] args) {
        // TODO: если файл network.dat содержит данные - считать его и поместить в networkMap
        Map<String, Set<String>> networkMap = new HashMap<>();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Введите канал. Для выхода - quit, для вывода - list");
            String channel = scanner.nextLine();
            if ("quit".equals(channel))
                break;
            if ("list".equals(channel)) {
                System.out.println(networkMap);
                continue;
            }
            if (!networkMap.containsKey(channel)) {
                networkMap.put(channel, new TreeSet<>());
            } else {
                System.out.println(networkMap.get(channel).toString());
            }
            System.out.println("Введите название телешоу");
            String show = scanner.nextLine();
            if (!networkMap.get(channel).contains(show)) {
                networkMap.get(channel).add(show);
            }
        }
        // TODO: сохранить данные networkMap в файл network.dat
    }
}
