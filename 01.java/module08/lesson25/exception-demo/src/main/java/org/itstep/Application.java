package org.itstep;

public class Application {

    public static void main(String[] args) {
//        try {
//            Application.main(null);
//        } catch (StackOverflowError error) {
//          error.printStackTrace();
//        }
//        String str = "Hello World";
//        System.out.println(str.indexOf(' '));
//        System.out.println(str.indexOf('x'));
//
//        Scanner scanner = new Scanner(System.in);
//        boolean isCompeted = false;
//        do {
//            System.out.print("Введите два числа: ");
//
//            try {
//                int a = Integer.parseInt(scanner.next());
//                int b = Integer.parseInt(scanner.next());
//                System.out.println(div(a, b));
//                isCompeted = true;
//            } catch (NumberFormatException ex) {
//                System.err.println("Введите числа в корректном формате");
//            } catch (DivideByZeroException ex) {
//                //ex.printStackTrace();
//                System.err.println(ex.getMessage());
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException ex1) {
//                    ex1.printStackTrace();
//                }
//            } catch (Throwable ex) {
//
//            } finally {
//                System.out.println("finally");
//            }
//            // StackOverflowError
//            // OutOfMemoryError
//        } while (!isCompeted);
//        System.out.println("End program");
        System.out.println(divFinally(10, 2));
        System.out.println();
        System.out.println(divFinally(10, 0));
    }

    static int divFinally(int a, int b) {
        try {
            System.out.println("try");
            return a / b;
        } catch (ArithmeticException ex) {
            System.out.println(ex.getMessage());
            return 1;
        } finally {
            System.out.println("finally");
            return 0;
        }
    }

    /**
     * @param first
     * @param second
     * @return
     * @throws IllegalArgumentException в случае если second = 0
     */
    private static Integer div(int first, int second) throws DivideByZeroException {
        if (second == 0) {
            throw new DivideByZeroException("Деление на 0");
        }
        return first / second;
    }
}

class DivideByZeroException extends Exception {
    public DivideByZeroException() {
    }

    public DivideByZeroException(String message) {
        super(message);
    }

    public DivideByZeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public DivideByZeroException(Throwable cause) {
        super(cause);
    }

    public DivideByZeroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}