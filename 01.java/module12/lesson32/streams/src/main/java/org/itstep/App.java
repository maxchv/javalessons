package org.itstep;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        // Потки данных - InputStream/OutputStream & Reader/Writer
        // Связаны с внешними источниками/приемниками данных

        // Потоки Stream (коллекции)
        // Появились в 8 версии Java
        // lambda - функциональный подход в программировании

        // 1. Создание Stream
        createStream();

        // 2. Операции над Stream:
        //    Терминальные - конечные (count, forEach, collect, ...)
        //    Нетерминальные - промежуточные (peek, filter, sorted, map, ...)

        // Сгенерировать список на 100 случайных чисел в диапазоне от 0 до 99
        // Для нечетных найти среднее арифметическое
        solutionUsingList();
        solutionUsingStream();

        // Нетерминальные операции
        List<Product> products = Arrays.asList(
                new Product(1, "Молоко", 28, 10, ProductCategory.DRINK),
                new Product(2, "Coca Cola", 38, 100, ProductCategory.DRINK),
                new Product(2, "Coca Cola", 38, 100, ProductCategory.DRINK),
                new Product(3, "Хлеб", 14, 50, ProductCategory.FOOD),
                new Product(4, "Масло", 50, 60, ProductCategory.FOOD),
                new Product(4, "Масло", 50, 60, ProductCategory.FOOD),
                new Product(5, "Водка", 78, 60, ProductCategory.DRINK)
        );

        IntStream.of(1, 2, 1, 2, 1, 1, 3, 4)
                .distinct()
//                .forEach(n -> System.out.println("n: " + n));
                .forEach(App::printInt); // ссылка на метод printInt

        products.stream()
                .distinct() // получить набор уникальных объектов
                .filter(p -> p.getPrice() > 30) // фильтрация
                .peek(System.out::println)
//                .sorted((p1, p2) -> p1.getCount() - p2.getCount())
                .sorted(Comparator.comparingInt(Product::getCount).thenComparing(Product::getName)) // сортировка
//                .peek(System.out::println)
//                .map(p -> p.getName())
                .skip(1) // пропускать заданное количество элементов
                .limit(2) // ограничение количества объектов в Stream
                .map(Product::getName)
                .forEach(System.out::println);

        List<Product> unique = products
                .stream()
                .distinct()
//              unique.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
                .collect(Collectors.toList());
        long c = unique.stream().peek(System.out::println).skip(1).count();
        System.out.println("c = " + c);
        Optional<Product> maxCountProduct = unique.stream()
                .max(Comparator.comparingInt(Product::getCount));
        Optional<Product> p = newProduct();
//        p.orElseThrow(RuntimeException::new);
        p.ifPresent(product -> System.out.println(product.getName()));
    }

    public static Optional<Product> newProduct() {
        Random rnd = new Random();
        if (rnd.nextBoolean()) {
            return Optional.of(new Product(0, "", 0, 0, ProductCategory.FOOD));
        }
        return Optional.empty();
    }

    public static void printInt(int n) {
        System.out.println("n: " + n);
    }

    private static void createStream() {
        Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5);
        Stream<Object> objStream = Stream.builder().add(1).add(2).add(3).add(4).add(5).build();
        Stream<Integer> infIntStream = Stream.iterate(1, x -> x + 1);
        Random rnd = new Random();
        final Random r1 = rnd;
        Stream<Integer> rndIntStream = Stream.generate(() -> r1.nextInt(100));

        int[] arr = {1, 2, 3, 4, 5};
        IntStream arrStream = Arrays.stream(arr);

        List<String> strList = Arrays.asList("one", "two", "tree");
        Stream<String> strStream = strList.stream();
    }

    private static void solutionUsingStream() {
        Random rnd = new Random(0);
        class Generator implements Supplier<Integer> {
            @Override
            public Integer get() {
                return rnd.nextInt(100);
            }
        }

        class IsOddPredicate implements Predicate<Integer> {

            @Override
            public boolean test(Integer n) {
                return n % 2 == 1;
            }
        }
        Generator generator = new Generator();
        IsOddPredicate predicate = new IsOddPredicate();
        double avg = Stream
                .generate(generator)
//                           .generate(() -> rnd.nextInt(100))
                .limit(100)
                .filter(predicate)
//                           .filter(n -> n % 2 == 1)
                .mapToInt(n -> n)
                .average().getAsDouble();
        System.out.println("avg: " + avg);
    }

    private static void solutionUsingList() {
        Random rnd = new Random(0);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(rnd.nextInt(100));
        }
        List<Integer> odd = new ArrayList<>();
        for (Integer integer : list) {
            if (integer % 2 == 1) {
                odd.add(integer);
            }
        }
        double sum = 0;
        for (Integer item : odd) {
            sum += item;
        }
        System.out.println("avg: " + (sum / odd.size()));
    }
}

@FunctionalInterface
interface MyInterface {
    void one();
    default void two() {

    }
}

enum ProductCategory {
    FOOD, DRINK
}

class Product {
    private final int id;
    private final String name;
    private final double price;
    private final int count;
    private final ProductCategory productCategory;

    public Product(int id, String name, double price, int count, ProductCategory productCategory) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
        this.productCategory = productCategory;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                Double.compare(product.price, price) == 0 &&
                count == product.count &&
                name.equals(product.name) &&
                productCategory == product.productCategory;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, count, productCategory);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                ", productCategory=" + productCategory +
                '}';
    }
}

interface Comparable {
    default int comparetTo() {
        return 0;
    }
}