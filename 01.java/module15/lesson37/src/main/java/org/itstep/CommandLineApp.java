package org.itstep;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class CommandLineApp {
    public static void main(String[] args) throws IOException {
        if(args.length == 0) {
            System.err.println("Необходимо передать путь к файлу");
            return;
        }
        Files
                .readAllLines(Paths.get(args[0]))
                .forEach(System.out::println);
    }
}
