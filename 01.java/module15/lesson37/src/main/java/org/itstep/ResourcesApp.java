package org.itstep;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ResourcesApp {
    public static void main(String[] args) throws IOException {
        ClassLoader classLoader = ResourcesApp.class.getClassLoader();
        System.out.println(classLoader);
        System.out.println(classLoader.getParent());
        System.out.println(classLoader.getParent().getParent());
        try(InputStreamReader in = new InputStreamReader(classLoader.getResourceAsStream("data/file.txt"));
            BufferedReader rdr = new BufferedReader(in)) {
            String line;
            while((line = rdr.readLine()) != null) {
                System.out.println(line);
            }
//            Files.readAllLines(Paths.get("data", "file.txt"))
//                    .forEach(System.out::println);
        }
    }
}
