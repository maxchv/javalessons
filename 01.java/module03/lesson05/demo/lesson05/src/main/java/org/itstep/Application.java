package org.itstep;

public class Application {
    public static void main(String[] args) {
        System.out.println("Start");
        // break & continue
        System.out.println("-== break ==-");
        int ii = 0;
        while (true) {
            System.out.println("i = " + ii);
            if (ii > 5) {
                break;
            }
            ii++;
        }
        System.out.println("-== continue ==-");
        for (int j = 0; j < 10; j++) {
            if (j % 2 == 1) {
                continue;
            }
            System.out.print(j + " ");
        }
        System.out.println();

        // labels
        System.out.println("-== labels ==-");

        outer:
        for (int j = 0; j < 10; j++) {
            switch (j) {
                case 1:
                    System.out.println("one");
                    break outer;
                case 2:
                    System.out.println("two");
                    break;
                default:
                    break;
            }
        }
        System.out.println("-== nested loops ==-");
        // nested loops
        for (int i = 0; i < 10; i++) { // loop by rows
            for (int j = 0; j < 10; j++) { // loop by columns
                System.out.printf("%-4d", (i + 1) * (j + 1));
            }
            System.out.println();
        }

        System.out.println("-== demo ==-");
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < i + 1; j++) {
//                if(j > i) {
//                    break;
//                }
                System.out.print("* ");
            }
            System.out.println();
        }

        System.out.println("End");
    }
}
