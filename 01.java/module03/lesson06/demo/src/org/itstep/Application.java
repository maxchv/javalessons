package org.itstep;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        // Форматирование вывода в Java
        // \n - new line (перевод на новую стрку)
        // \r - перевод карретки
        // \t - горизонтальная табуляци

        System.out.println("Hello");
        System.out.print("Hello\n");
        System.out.print("World\n");

        int a = 10;
        int b = 20;
        int c = a + b;

        System.out.println("a + b = " + a + " + " + b + " = " + c);
        // format
        String str = String.format("a + b = %d + %d = %d", a, b, c);
        System.out.println(str);
        // %d, %i вывод целых чисел в десятичном виде
        // %o     вывод целых чисел в восьмиричном виде
        // %x     вывод целых чисел в шестнадцатиричном виде
        // %f     вывод с плавающей точкой
        // %e
        // %g
        // %%     знак %
        // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
        System.out.printf("a + b = %d + %d = %d\n", a, b, c);

        // Класс Math, случайные числа
        double pi = Math.PI;
        System.out.printf("%%f = %f, %%e = %e, %%g = %g\n", pi, pi, pi);
        // 1 000,000

        System.out.format(Locale.US, "%.3f\n", pi);

        // 123 <- ширина поля
        //  10
        // 10
        System.out.format("%+4d%+4d%+4d\n", a, b, c);
        System.out.format("%3d%3d%3d\n", a, b, c);
        System.out.format("%-3d%-3d%-3d\n", a, b, c);
        System.out.format("%010d %010d %010d", a, b, c);

        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.format("%-4d", i * j);
            }
            System.out.println();
        }

        // округление чисел
        double num = 1.4;
        System.out.printf("%.2f %.2f %.2f %d\n\n", num,
                Math.floor(num), // отбрасывает дробную часть
                Math.ceil(num),  // округляет до большего целого
                Math.round(num));// округляет по правилам математики

        // Math.sqrt(4) == 2 - возращает квадратный корень числа
        // Math.pow(2, 3) == 8 - возвращает степень числа
        // Math.abs(-1) == 1 - модуль числа
        double rand = Math.random(); // возвращает псевдослучайные числа в диапазоне от 0..1
        int irand = (int) (rand * 100);
        System.out.println(irand);

        Random random = new Random();
        irand = random.nextInt(100); // 0..99
        System.out.println("irand = " + irand);

        // [0..100] + 100 => [100..200]
        for (int i = 0; i < 10; i++) {
            irand = random.nextInt(101); // 100..200
            irand += 100;
            System.out.println("after irand = " + irand);
        }

        // Константы
        final double PI = 3.141;
        double r;
        r = 1.5; // радиус круга
        // PI = 3; изменить значение final константы не возможно
        double p = r * r * Math.PI;

        // Повторение циклов
        /*
        Написать программу, которая выводит таблицу ежемесяч-
        ных платежей по кредиту. Исходные данные для расчета: сумма
        кредита, срок и процентная ставка. Предполагается, что кредит
        возвращается (выплачивается) ежемесячно равными долями.
        Проценты начисляются ежемесячно на величину долга. Рекомен-
        дуемый вид экрана приведен иже.
        Cумма (грн.) -> 150 000
        Срок (мес.) -> 12
        Процентная ставка (годовых) -> 14

        -----------------------------------
        Долг Процент Платеж
        -----------------------------------
        1  150000.00 1750.00 14250.00
        2  137500.00 1604.17 14104.17
        3  125000.00 1458.33 13958.33
        4  112500.00 1312.50 13812.50
        5  100000.00 1166.67 13666.67
        6   87500.00 1020.83 13520.83
        7   75000.00 875.00  13375.00
        8   62500.00 729.17  13229.17
        9   50000.00 583.33  13083.33
        10  37500.00 437.50  12937.50
        11  25000.00 291.67  12791.67
        12  12500.00 145.83  12645.83
        -----------------------------------
        Всего процентов: 11375.00
         */
        // Scanner - класс
        // scanner - объект или экземпляр класса
        // new Scanner(System.in) - конструктор
        Scanner scanner = new Scanner(System.in);
        double summ, money = 0.0;
        int month;
        double percent;
        System.out.print("Cумма (грн.) -> ");
        // nextDouble() - метод экземпляра scanner
        summ = scanner.nextDouble();
        System.out.print("Срок (мес.) -> ");
        month = scanner.nextInt();
        System.out.print("Процентная ставка (годовых) -> ");
        percent = scanner.nextDouble();
        percent /= 100;
        System.out.println("\n-----------------------------------\n" +
                "     Долг     Процент Платеж\n" +
                "-----------------------------------");
        double body = summ / month;
        double add;
        for (int i = 0; i < month; i++) {
            add = summ / month * percent;
            money += add;
            System.out.format(Locale.US, "%-3d%9.2f%9.2f%9.2f\n",
                    i + 1, summ, add, body + add);
            summ -= body;
        }
        System.out.println("-----------------------------------");
        System.out.printf(Locale.US, "Всего процентов: %.2f", money);

        // soutv+Tab
        System.out.println("body = " + body);

        // Ctrl-Alt-L
    }
}
