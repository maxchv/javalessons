package org.itstep;

import java.util.Random;

public class Application {
    public static void main(String[] args) {
//        // Округлення чисел
//        double d = 1.5;
//        System.out.println("d = " + d);
//        int i = (int) d;
//        System.out.println("(int)d = " + i);
//        double o = Math.floor(d);
//        System.out.println("Math.floor(d) = " + o);
//        o = Math.ceil(d);
//        System.out.println("Math.ceil(d) = " + o);
//        long l = Math.round(d);
//        System.out.println("Math.round(d) = " + l);
//
//        // Зберігти складові числа
//        // 1234 -> 4 3 2 1
//        // 4*10 + 3 = 43
//        // 43*10 + 2 = 432
//        // 432*10 + 1 => 4321
//        int num = 12;
//        int rev = 0;
//        String str = "";
//        int r = 0;
//        while (num != 0) {
//            int n = num % 10;
//            num /= 10;
//            rev = rev * 10 + n;
//            str += n;
//            r++;
//        }
//        System.out.println("rev = " + rev);
//        System.out.println("str = " + str);
//        System.out.println("r = " + r);
//
//        // Форматування виводу
//        System.out.print("\n");
//        System.out.println();
        // -a
        // a + b
        // Тернарний оператор <умова> ? <код якщо правда> : <код інакше>
        int n = 18;
//        if(n % 2 == 0) {
//            System.out.println("парне число");
//        } else {
//            System.out.println("непарне число");
//        }
        String text = n % 2 == 0 || n > 100 ? "парне число" : "непарне число";
//        double nn = true ? (false ? 1 : 6) : 10.0;
//        System.out.println("nn = " + nn);
        System.out.println(text);

        // Випадкові числа
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            int r = random.nextInt(1000, 10_000); // [0, 100)
            System.out.println("r = " + r);
        }
    }
}
