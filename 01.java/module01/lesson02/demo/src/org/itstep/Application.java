package org.itstep; // пакет

import java.util.Scanner; // импортируем все, что находится в пакете java.util

public class Application {
    public static void main(String[] args) {

        ////////////////////////////////////////////////////////////////
        // Пакеты - обязательно должны быть в домашних заданиях
        // Пакет - это папка в проекте
        // Папки (пакеты) друг от друга отделяются точкой (.)

        // java.lang - пакет, который автоматически подключается
        // к вашему проекту
        java.lang.System.out.println("Hello World");
        System.out.println("Hello World");

        // Это классы стандартной библиотеки java (Java SE, Java Core)
        Scanner sc = new Scanner(System.in);

        // Однострочный комментарий
        /*
            Многострочный
            комментарий
         */

        ////////////////////////////////////////////////////////////////
        // Базовые (примитивные) типы данных - 8
        // 2 + 2 = 4

        // Целые: byte (1 байт), short (2 байт), int (4 байт), long (8 байт)
        // 1 байт - 8 бит
        // переменная - это именованная ячейка памяти
        byte b; // объявление переменной
        // (byte) 255 - явное приведение типа. Из int (по умолчанию) к byte
        b = 127; // присваиваем переменной (помещаем в ячейку памяти) значение 255
        System.out.print("byte min: ");
        System.out.println(Byte.MIN_VALUE);
        System.out.print("byte max: ");
        System.out.println(Byte.MAX_VALUE);

        long lon = 1L; // суффикс L - признак целого числа двойной точности (8 байте)

        // Скопировать: Ctrl-D, Ctrl-C

        b = 3;
        System.out.println(b / 2); // byte/int -> целое

        // Дробные: double (8 байт) до 16 знака,
        // float (4 байт) точность до 7 знака после запятой
        float f = 3.11111111111111111111111111f; // суффикс F - признак числа с плавоющей точкой одинарной точности (4 байт)
        System.out.println(f); // float / int -> float

        double d = 3.11111111111111111111111111; // BigDecimal
        System.out.println(d);

        // Символьный тип: char - беззнаковое целое число (2 байт)
        // unicode table: https://unicode-table.com/ru/
        char sym = 'α';//'a';// 945;
        // конкатенация (соединение) нескольких строк в одну через оператор +
        System.out.println(sym + " -> " + (short) sym);
        System.out.println(sym + ' ' + sym);
        System.out.println('\u03A9');

        // Логический (булевый): boolean только true или false
        boolean t = true;
        t = false;

        // Строковый тип (класс): String
        String str = "Это простая строка";

        ////////////////////////////////////////////////////////////////
        // Операции над данными

        // Математические операции: + - * / %
        // Бинарные - операция над двумя операндами (данными)
        int a = 10;
        int bb = 20;
        int c;
        c = a + bb;
        // % - операция остаток от целочисленного деления
        System.out.println(1%2);
        System.out.println(2%2);
        System.out.println(3%2);
        System.out.println(4%2);
        System.out.println(5%2);
        System.out.println(6%2); // для четного числа остаток от деления % на 2 == 0

        System.out.println(6%5);

        int time = 1234; // в секундах
        // перевести в мин и сек
        int min = time / 60;
        int sec = time % 60; //time - min * 60;
        System.out.println(min + " min " + sec + " sec");

        // Сокращенные формы операций: += -= *= /= %=
        time += 60;// time + 60;
        System.out.println(time);

        // Приращение на 1 - инкремент
        time++; //time += 1;
        System.out.println(time);

        // Уменьшение на 1 - декремент
        time--;
        System.out.println(time);

        // Инкремент и декремент - постфиксная и префиксная формы
        System.out.println("Постфикс");
        System.out.println(time++);
        System.out.println(time);
        System.out.println("Префикс");
        System.out.println(++time);
        System.out.println(time);

        // Возведение в степень Math.pow(), квадратный корень числа Math.sqrt()
        int res = (int)Math.pow(2, 8);
        System.out.println(res);

        System.out.println(Math.sqrt(121));

        ////////////////////////////////////////////////////////////////
        // Условные конструкции

        ////////////////////////////////////////////////////////////////
        // Циклы

    }
}
