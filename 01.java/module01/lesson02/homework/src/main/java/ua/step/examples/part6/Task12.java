package ua.step.examples.part6;

/**
 * 
 * Видимость переменных ограничена блоком кода
 *
 */
public class Task12 {
    public static void main(String[] args) {
        int i = 3;
        {
            int j = 5;
            System.out.println(i + j);
        }
        System.out.println(i);
        // FIXME: раскоментируй 18 строчку кода переменной с именем j в этой строчке не
        // видно!!!
        // System.out.println(i + j);
    }
}