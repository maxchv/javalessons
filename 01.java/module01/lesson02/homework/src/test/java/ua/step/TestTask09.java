package ua.step;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import ua.step.homework.Task09;

@RunWith(Parameterized.class)
public class TestTask09 {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final String num;
    private final String expected;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    public TestTask09(String num, String expected) {
        this.num = num;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][] { { "3.14", "да" }, { "3", "нет" }, { "4.123", "да" }, { "4", "нет" } });
    }

    @Test
    public void test1() {
        Task09.main(new String[] { num });
        assertEquals(expected, outContent.toString().trim());
    }
}