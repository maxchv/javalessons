package org.itstep;

class Lesson02 {
    public static void main(String[] args) {
        System.out.println("Lesson02");

        // Типи даних
        // Групи даних: примітивні (прості), ссилочні
        // Примітивні дані: 8 (цілі, дробні, символьні, логічні)
        // Цілі: byte (1 байт) -128..+127, short (2 байт), int (4 байт), long (8 байт)
        // Дробні: float (4 байт), double (8 байт)
        // Символьні: char (2 байт)
        // Логічні: boolean

        // Змінні
        byte b = 1; // об'явили змінну
        b = 127; // задали значення
        b++; // інкремент
        System.out.println(b);
        b--;
        System.out.println(b);
        b = (byte) 0b1111_1111;
        System.out.println("b = " + b);
        b++; // переповнення пам'яті
        System.out.println("b = " + b);

        char ch = 'A'; //65;
        System.out.println(ch);

        int count = 0012;// 0xf182;
        long l = 20_000_000_000L;
        double dbl = 3.14;
        float fl = 3.14f;

        // Операції над даними

        // Бінарні операції
        // Аріфметичні операції: +, -, *, /
        int a = 10;
        int d = 3;
        float c = (a + (float) a) / d;
        System.out.println("a / d = " + a + " / " + d + " = " + c);

        // Залишок від ділення на ціле: %
        // Скорочені операції: +=, -=, *=, /=, %=
        a += 10; //a = a + 10;
        System.out.println("a = " + a);

        // Унарні операції
        // Інкремент ++, Декремент --, Плюс +, Мінус -
        System.out.println(+a);
        System.out.println(-a);
        System.out.println(a++);
        System.out.println(a);
        System.out.println(++a);
        System.out.println(a);
    }
}
