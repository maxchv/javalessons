# Module 1

## Introduction to the Java programming language

**Module duration**: 4 double-classes

1. Introduction.
2. Algorithm.
3. The concept of flow-chart.
4. Itellij IDEA.
5. JShell.

## Usefull links

1. [The Java™ Tutorials ](https://docs.oracle.com/javase/tutorial/)
2. [Java Tutorial](https://www.tutorialspoint.com/java/)
3. [Java SE Downloads](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
4. [Open JDK](https://openjdk.java.net/)
5. [Eclipse IDE](https://www.eclipse.org/downloads/)
6. [NetBeans IDE](https://netbeans.org/)
7. [Intellij IDEA](https://www.jetbrains.com/ru-ru/idea/)
8. [EduTools -  plugin for Intellij IDEA to learn and teach programming languages such as Kotlin, Java, and Python](https://plugins.jetbrains.com/plugin/10081-edutools)
9. [Get Java Magazine!](https://www.oracle.com/technetwork/java/javamagazine/index.html)