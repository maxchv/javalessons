package org.itstep;

public class Demo2 {

    static class MyThread extends Thread {

        private int result;
        private int num;

        public MyThread(int num) {
            super("Sleeping Thread");
            this.num = num;
        }

        @Override
        public void run() {
//            try {
//                java.lang.Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println("End thread: " + java.lang.Thread.currentThread().getName());
//            number = 1000;
            result=1;
            while (num > 0) {
                result*=num;
                num--;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(result);

        }
    }


    public static void main(String[] args) throws InterruptedException {

        MyThread t1 = new MyThread(5);
        t1.setDaemon(true);
        t1.start();
        //t1.join();
        System.out.println(t1.result);

        System.out.println("End of program");
    }
}
