package org.itstep;

public class Demo3 {

    public static void main(String[] args) throws Exception {
        concurrencyRace();
    }

    // Демонстрация проблемы борьбы за ресурсы
    private static void concurrencyRace() throws InterruptedException {
        Account account = new Account(100_000);
        System.out.println("Begin balance " + account.getBalance());

        // TODO: Создать поток для снятия денег со счета
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 100_000; i++) {
                account.deposit(1);
            }
        });

        // TODO: Создать поток для внесения денег на счет
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 100_000; i++) {
                account.withdraw(1);
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("End balance " + account.getBalance());
    }
}

/**
 * Банковский счет
 */
class Account {
    private long balance;

    //final Object locker = new Object();

    public Account(long l) {
        this.setBalance(l);
    }

    public long getBalance() {
        return balance;
    }

    private void setBalance(long balance) {
        this.balance = balance;
    }

    // пополнения счета
    public synchronized void deposit(long amount) throws IllegalArgumentException {
//        synchronized (this) { // lock
            checkAmountNonNegative(amount);
            balance += amount; // Concurrency Race Condition
//        } // unlock
    }

    private static void checkAmountNonNegative(long amount) throws IllegalArgumentException {
        if (amount < 0) {
            throw new IllegalArgumentException("negative amount");
        }
    }

    // снятие денег
    public synchronized void withdraw(long amount) throws IllegalArgumentException {
//        synchronized (this) {
            checkAmountNonNegative(amount);
            balance -= amount; // Concurrency Race Condition
//        }
    }
}
