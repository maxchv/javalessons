package org.itstep;

public class App {
    static class WorkThread extends Thread {
        @Override
        public void run() {
            System.out.println("thread in run method: " + Thread.currentThread().getName());
            for (int i = 0; i < 10; i++) {
                System.out.print(i);
                if(isInterrupted()) {
                    break;
                }
            }
            System.out.println();
        }
    }

    static class Work implements Runnable {

        @Override
        public void run() {
            for (int i = 10; i > 0; i--) {
                System.out.print(i);
            }
            System.out.println();
        }
    }

    public static class SleepingThread implements Runnable {

        @Override
        public void run() {
            try {
                System.out.println("Поток уснул");
                Thread.sleep(1000);
                System.out.println("Поток выспался");
            } catch (InterruptedException e) {
//                e.printStackTrace();
                System.out.println("Поток разбудили");
            }
        }
    }

    static class HardWorkThread implements Runnable {

        @Override
        public void run() {
            int s = 0;
            System.out.println("Начал работу");
            int i;
            for (i = 0; i < 1000_000_000; i++) {
                s+=i;
                if (Thread.interrupted()) {
                    break;
                }
            }
            System.out.println("Работа потока успешно завершена на i = " + i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //demo01();
        Thread t1 = new Thread(new SleepingThread());
        t1.start();
        Thread.sleep(100);
        t1.interrupt();

        Thread t2 = new Thread(new HardWorkThread());
        t2.setPriority(5);
        t2.start();
        Thread.sleep(1);
        t2.interrupt();
        System.out.println("Конец работы программы");
    }

    private static void demo01() {
        System.out.println("Start");
        System.out.println("thread in main method: " + Thread.currentThread().getName());

//        new WorkThread().start();
////        new Thread(new Work()).start();
//        new Thread(() -> {
//            for (int i = 10; i > 0; i--) {
//                System.out.print(i);
//            }
//            System.out.println();
//        }).start();
        for(int i=0; i<10; i++) {
            new Thread(() -> {
                for(int j=0; j<100; j++) {
                    System.out.println(Thread.currentThread().getName());
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        System.out.println("Stop");
    }
}
