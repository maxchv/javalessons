package org.itstep;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by max on 01.10.2017.
 */
public class Demo8 {
    // TODO: implements interface Callable instead Runnable
    public static class Worker implements Callable<String> {

        final String name;


        public Worker(String string) {
            name = string;
        }

        @Override
        public String call() {
            long sleepTime = (long) (Math.random()*10000L);
            System.out.println(name + " started, going to sleep for " + sleepTime + " seconds");
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " finished");
            // FIXME: return name???
             return name;
        }
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // TODO: ExecutorService, Executors
        ExecutorService executorService = Executors.newCachedThreadPool();

        for(int i=0; i<10; i++) {
            executorService.execute(() -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
            });
            Thread.sleep(5);
        }

        Future<Long> future = executorService.submit(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                int n = 5;
                long f = 1;
                while(n > 0) {
                    f *= n;
                    n--;
                }
                return f;
            }
        });
        System.out.println(future.get());
        executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
        executorService.shutdown();
    }
}
