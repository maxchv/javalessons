package org.itstep;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Demo5 {

    static class Friend {
        private final String name;
        private final Lock lock = new ReentrantLock();

        public Friend(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public void seyHi(Friend friend) throws InterruptedException {
            System.out.format("%s: %s said hi to me!%n", name, friend.getName());

            while(true) {
                boolean myLock = lock.tryLock();
                boolean yourLock = friend.lock.tryLock();

                //System.out.println("myLock = " + myLock);
                //System.out.println("yourLock = " + yourLock);

                if (!(myLock && yourLock)) {
                    if (myLock) {
                        //System.out.println("free myLock");
                        lock.unlock();
                    }
                    if (yourLock) {
                        //System.out.println("free yourLock");
                        friend.lock.unlock();
                    }
                }

                if (myLock && yourLock) {
                    friend.hiBack(this);
                    lock.unlock();
                    friend.lock.unlock();
                    break;
                }
            }
        }

        public void hiBack(Friend friend) {
            System.out.format("%s: %s has said hi back to me!%n", name, friend.getName());
        }
    }

    // Deadlock demo
    public static void main(String[] args) throws InterruptedException {
        final Friend petro = new Friend("Petro");
        final Friend vano = new Friend("Vano");

//        for(int i=0; i<10; i++) {
            new Thread(() -> {
                try {
                    petro.seyHi(vano);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();

            new Thread(() -> {
                try {
                    vano.seyHi(petro);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
//        }
    }
}