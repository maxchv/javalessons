package org.itstep;


import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;


public class Demo7 {
    public static void main(String[] args) {
        // Конкурентные коллекции

        // CopyOnWriteArrayList<E>
        // CopyOnWriteArraySet<E>
        // ConcurrentSkipListSet<E>

        // ConcurrentHashMap<K, V>
        // ConcurrentSkipListMap<K, V>

        // ConcurrentLinkedQueue<E>
        // ConcurrentLinkedDeque<E>
        // ArrayBlockingQueue<E>
        // LinkedBlockingQueue<E>
        // PriorityBlockingQueue<E>
        // SynchronousQueue<E>

        // LastIn FirstOut (LIFO)
//        Stack<Integer> stack = new Stack<>();
//        stack.push(1);
//        stack.push(2);
//        stack.push(3);
//        stack.push(4);
//        while(!stack.empty()) {
//            System.out.println(stack.pop());
//        }

        // FirstIn FirstOut
//        Queue<Integer> queue = new ArrayDeque<>();
//        queue.add(1);
//        queue.add(2);
//        queue.add(3);
//        queue.add(4);
//        while (!queue.isEmpty()) {
//            System.out.println(queue.poll());
//        }

        Collection<Integer> list = new CopyOnWriteArrayList<>(Arrays.asList(1, 2, 3, 4, 5));

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next() + 1);
        }
        System.out.println(list);
    }
}
