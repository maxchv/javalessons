package org.itstep;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Demo4 {

    public static void main(String[] args) throws InterruptedException {
        // Начальный счет
        Account2 account = new Account2(0);

        // Накопим немного денег
        Thread t = new Thread(() -> {
            for (int i = 0; i < 50_000_000; i++) {
                account.deposit(1);
            }
        });
        t.start();

        // Снять деньги со счета???
//        account.withdraw(50_000_000);

        /* TODO: вызвать метод waitAndWithdraw()*/
        System.out.println("Calling waitAndWithdraw()...");
        account.waitAndWithdraw(50_000);

        t.join();
        System.out.println("waitAntWithdraw() finished " + account.getBalance());
    }
}

class Account2 {
    private long balance;

    Lock lock = new ReentrantLock();
    Condition condition = lock.newCondition();

    public Account2(long l) {
        this.setBalance(l);
    }

    public long getBalance() {
        return balance;
    }

    private void setBalance(long balance) {
        this.balance = balance;
    }

    //    public synchronized void deposit(long amount) {
    public void deposit(long amount) {
        checkAmountNonNegative(amount);
        try {
            lock.lock();
            balance += amount;
            condition.signal();// notify();
        } finally {
            lock.unlock();
        }
    }

    private static void checkAmountNonNegative(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("negative amount");
        }
    }

    //    public synchronized void waitAndWithdraw(long amount) throws InterruptedException {
    public void waitAndWithdraw(long amount) throws InterruptedException {
        try {
            lock.lock();
            while (balance < amount) {
//            if(balance >= amount) {
//                break;
//            }
                condition.await(); //  wait(100);
            }
        } finally {
            lock.unlock();
        }
        withdraw(amount);
    }

    public synchronized void withdraw(long amount) {
        checkAmountNonNegative(amount);

        if (balance < amount) {
            throw new RuntimeException("Not money");
        }

        balance -= amount;
    }
}