package org.itstep;

//import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

//@Slf4j
@Info(value = "value")
public class Application<T> {

    //@Info(name = "some data")
    private T arg;
    protected int value;

    @Info
    Application(T arg) {
        this.arg = arg;
    }

    @Info
    @Override
    public int hashCode() {
        return arg.hashCode();
    }

    @Override
    public String toString() {
        return "Application{" +
                "arg=" + arg +
                '}';
    }

    //@SuppressWarnings("deprication")
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException, InvocationTargetException {
        /*Application<Integer> app = new Application<>();
        System.out.println(app.equals(app));
        System.out.println(app.equals(1));*/
        /*System.out.println(div(10, 5));
        System.out.println(div(10, 0));*/
        /*Map<String, String> dic = new HashMap<>();

        dic.put("hello", "привет");
        dic.put("sell", "продать");

        for(String eng: dic.keySet()) {
            System.out.println(eng + " -> " + dic.get(eng));
        }

        for(String rus: dic.values()) {
            System.out.println(" -> " + rus);
        }

        for(Map.Entry<String, String> entry: dic.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
        */
        /*Application<Integer> app1 = new Application<>(1);
        Application<Integer> app2 = new Application<>(1);
        System.out.println(app1.hashCode());
        System.out.println(app2.hashCode());*/

        //log.info("Hello World");

        //System.out.println(div(10, 2));
        //reflectionDemo();

        Object obj = new Derived();
        Class<?> clazz = obj.getClass(); //Derived.class;
        Map<Integer, Method> beforeMethods = new TreeMap<>();
        List<Method> runMethods = new ArrayList<>();
        for (Method method: clazz.getDeclaredMethods()) {
            if(method.isAnnotationPresent(Before.class)) {
                Before before = method.getAnnotation(Before.class);
                beforeMethods.put(before.order(), method);
                //beforeMethods.add(method);
            }
            if(method.isAnnotationPresent(Run.class)) {
                runMethods.add(method);
            }
        }

        for(Integer order: beforeMethods.keySet()) {
            Method method = beforeMethods.get(order);
            System.out.println("Invoke method: " + method.getName());
            method.invoke(obj, "Invoke method @Before");
        }

        for(Method method: runMethods) {
            System.out.println("Invoke method: " + method.getName());
            method.invoke(obj, "Invoke method @Run");
        }
    }

    private static void reflectionDemo() throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        Application<Integer> app = new Application<>(1);

        Class<?> clazz = Application.class;
        System.out.println(clazz.getName());
        clazz = app.getClass();
        System.out.println(clazz.getSimpleName());
        clazz = Class.forName("org.itstep.Application");
        System.out.println(clazz.getSimpleName());

        System.out.println("Annotation? " + clazz.isAnnotation());
        System.out.println("Interface? " + clazz.isInterface());
        int modifiers = clazz.getModifiers();
        System.out.println("Public? " + Modifier.isPublic(modifiers));
        System.out.println(app);
        Field argField = clazz.getDeclaredField("arg");
        System.out.println("isAccessible: " + argField.isAccessible());
        argField.setAccessible(true);
        System.out.println("isAccessible: " + argField.isAccessible());
        argField.set(app, 2);
        System.out.println(app);

        for (Field field : clazz.getDeclaredFields()) {
//            int fieldModifiers = field.getModifiers();
//            System.out.println("Public? " + Modifier.isPublic(fieldModifiers));
//            System.out.println("Protected? " + Modifier.isProtected(fieldModifiers));
//            System.out.println("Private? " + Modifier.isPrivate(fieldModifiers));
            System.out.println(field.getType().getSimpleName() + " " + field.getName());
        }

        for(Method method: clazz.getDeclaredMethods()) {
            System.out.println(method.getReturnType().getSimpleName() + " "+
                    method.getName() + "()");
        }

        clazz = Derived.class;
        Object obj = clazz.newInstance();
        System.out.println(obj.getClass().getName());
    }

    /**
     * @param a
     * @param b
     * @return
     * @deprecated since version 2.0
     */
    @Deprecated
    public static int div(int a, int b) {
        assert b != 0;
        return a / b;
    }
}

@Info
class Base {

}

class Derived extends Base {

    @Before(order = 1)
    public void method1(String message) {
        System.out.println(message);
    }

    @Run("ksjkfj")
    public void method2(String message) {
        System.out.println(message);
    }

    @Before(order = 2)
    public void method3(String message) {
        System.out.println(message);
    }

    public void method4(String message) {
        System.out.println(message);
    }
}
