package org.itstep;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Info {
    String value() default "";
    String name() default "test";
}
