package org.itstep.pattern.structural.facade;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Demo {
    public static void main(String[] args) throws IOException {
        Files.lines(Paths.get("input.txt")).forEach(System.out::println);
    }
}
