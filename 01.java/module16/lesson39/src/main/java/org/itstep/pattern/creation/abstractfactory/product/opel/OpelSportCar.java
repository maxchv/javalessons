package org.itstep.pattern.creation.abstractfactory.product.opel;

import org.itstep.pattern.creation.abstractfactory.product.SportCar;

public class OpelSportCar extends SportCar {

    public OpelSportCar(String model, int maxSpeed, String color, double price) {
        super(model, maxSpeed, color, price);
    }
}
