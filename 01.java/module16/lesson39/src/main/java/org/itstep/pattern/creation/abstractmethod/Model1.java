package org.itstep.pattern.creation.abstractmethod;

public class Model1 extends Car {
    @Override
    public String getName() {
        return "Model1";
    }

    @Override
    public double getMaxSpeed() {
        return 10;
    }
}
