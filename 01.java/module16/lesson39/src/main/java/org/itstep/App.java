package org.itstep;

import org.itstep.pattern.creation.objectpool.Car;
import org.itstep.pattern.creation.objectpool.CarRent;

import java.util.Comparator;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
//        AbstractFactory factory = new Mercedes();
//
//        SportCar mercedesSport = factory.newSportCar();
//        System.out.println("mercedesSport = " + mercedesSport);
//        ElectroCar mecedesElectro = factory.newElectroCar();
//        System.out.println("mecedesElectro = " + mecedesElectro);
//        Car car = new Car.Builder()
//                .addColor("red")
//                .addModel("X5")
//                .addManufacture("BMW")
//                .addPrice(new BigDecimal(100_000))
//                .addTransmission(Transmission.AUTO)
//                .build();
//        System.out.println(car);
//
//        String str = new StringBuilder()
//                .append("one")
//                .append(" ")
//                .append("two")
//                .toString();
//        Factory factory = new GarageFactory();
//        Car car = factory.newCar();
//        System.out.println(car);
//
//        List<Student> students = Arrays.asList(new Student("Маша", 32), new Student("Маша", 21));
//        students = students.stream().sorted(StudentComparingFactory.newComparator()).collect(Collectors.toList());

//        Car car = new Car("VAZ 2010", 120, "Baklagan", 1000);
//        Car copy = car.clone();
//        System.out.println(copy);
        CarRent carRent = CarRent.getInstance();
        Car car1 = carRent.rent();
        Car car2 = carRent.rent();
        carRent.releas(car1);
        System.out.println(car1 == carRent.rent());

    }

    static class StudentComparingFactory {
        static Comparator<Student> newComparator() {
            return Comparator.comparing(Student::getName).thenComparing(Student::getAge);
        }
    }

    static class Student{
        String name;
        int age;

        public Student(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
