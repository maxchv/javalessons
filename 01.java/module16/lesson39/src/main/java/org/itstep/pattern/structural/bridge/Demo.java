package org.itstep.pattern.structural.bridge;

public class Demo {
    public static void main(String[] args) {
        Menu mainMenu = new Menu("Welcome to program");
        mainMenu.add(new Menu("Sub1"))
                .add(new Menu("SubSub1", new Submenu()));
        mainMenu.add(new Menu("Sub2", () -> System.out.println("Action for submenu2")));
        mainMenu.add(new Menu("Sub3", () -> System.out.println("Action for submenu3")));

        mainMenu.show();
    }

    static class Submenu implements Action {

        @Override
        public void doIt() {
           System.out.println("Action for submenu1");
        }
    }
}
