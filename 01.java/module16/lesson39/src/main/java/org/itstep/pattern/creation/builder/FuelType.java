package org.itstep.pattern.creation.builder;

public enum FuelType {
    PETROL, DISEL, ELECTRO, HIBRIDE, LPG, HPG
}
