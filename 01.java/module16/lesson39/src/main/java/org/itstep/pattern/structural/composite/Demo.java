package org.itstep.pattern.structural.composite;

public class Demo {
    public static void main(String[] args) {
        Directory c = new Directory("c:");
        Directory pf = new Directory("Program Files");
        c.addChild(pf);
        Directory jdk = new Directory("Java");
        pf.addChild(jdk);
        Directory bin = new Directory("bin");
        bin.addChild(new File("java.exe"));
        jdk.addChild(bin);
        Directory idea = new Directory("Idea");
        pf.addChild(idea);
        File log = new File("logfile.log");
        c.addChild(log);

        System.out.println(c);
    }
}
