package org.itstep.pattern.creation.abstractmethod;

public abstract class Car {
    public abstract String getName();
    public abstract double getMaxSpeed();

    @Override
    public String toString() {
        return "Name: " + getName() + " Max Speed: " + getMaxSpeed();
    }
}
