package org.itstep.pattern.creation.builder;

import java.math.BigDecimal;


public class Car {
    private String model;
    private String manufacture;
    private String color;
    private Transmission transmission;
    private FuelType fuelType;
    private BigDecimal price;

    public static class Builder {
        Car car;

        public Builder() {
            car = new Car();
            car.color = "white";
            car.fuelType = FuelType.PETROL;
            car.transmission = Transmission.MANUAL;
        }

        public Builder addModel(String model) {
            car.model = model;
            return this;
        }

        public Builder addManufacture(String manufacture) {
            car.manufacture = manufacture;
            return this;
        }

        public Builder addColor(String color) {
            car.color = color;
            return this;
        }

        public Builder addTransmission(Transmission transmission) {
            car.transmission = transmission;
            return this;
        }

        public Builder addFuelType(FuelType fuelType) {
            car.fuelType = fuelType;
            return this;
        }

        public Builder addPrice(BigDecimal price) {
            car.price = price;
            return this;
        }

        public Car build() {
            return car;
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", manufacture='" + manufacture + '\'' +
                ", color='" + color + '\'' +
                ", transmission=" + transmission +
                ", fuelType=" + fuelType +
                ", price=" + price +
                '}';
    }
}
