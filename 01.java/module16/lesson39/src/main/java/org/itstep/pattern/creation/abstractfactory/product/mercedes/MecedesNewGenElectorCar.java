package org.itstep.pattern.creation.abstractfactory.product.mercedes;

public class MecedesNewGenElectorCar extends MercedesElectroCar {
    public MecedesNewGenElectorCar(String model, int maxSpeed, String color, double price, int power) {
        super(model, maxSpeed, color, price, power, 1000);
    }
}
