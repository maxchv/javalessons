package org.itstep.pattern.structural.bridge;

@FunctionalInterface
public interface Action {
    void doIt();
}
