package org.itstep.pattern.structural.composite;

public class File implements FileSystemEntity {

    private final String name;

    public File(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addChild(FileSystemEntity child) {
        throw new RuntimeException("It is no composite entity");
    }

    @Override
    public void removeChild(FileSystemEntity child) {
        throw new RuntimeException("It is no composite entity");
    }

    @Override
    public String toString() {
        return name + "\n";
    }
}
