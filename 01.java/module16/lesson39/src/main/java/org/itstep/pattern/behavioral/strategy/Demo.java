package org.itstep.pattern.behavioral.strategy;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Demo {
    public static void main(String[] args) throws IOException {
        Zip strategy1 = new Zip();
        Zip7 strategy2 = new Zip7();

        Archivator archivator = new Archivator("output");
        archivator.setCompressor(strategy1);
        archivator.achive("Hello World".getBytes());

        archivator.setCompressor(strategy2);
        archivator.achive("Hello World".getBytes());
    }
}

interface Compressor {
    byte[] compress(byte[] bytes);
    String extention();
}

class Zip implements Compressor {

    @Override
    public byte[] compress(byte[] bytes) {
        return new byte[0];
    }

    @Override
    public String extention() {
        return ".zip";
    }
}

class Zip7 implements Compressor {

    @Override
    public byte[] compress(byte[] bytes) {
        return new byte[0];
    }

    @Override
    public String extention() {
        return ".7z";
    }
}

class Archivator {
    Compressor compressor;
    String filename;

    public Archivator(String filename) {
        this.filename = filename;
    }

    public void setCompressor(Compressor compressor) {
        this.compressor = compressor;
    }

    public void achive(byte[] bytes) throws IOException {
        // Save to file
        Files.write(Paths.get(filename + compressor.extention()), compressor.compress(bytes));
    }
}
