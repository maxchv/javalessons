package org.itstep.pattern.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        Chat chat = new Chat();
        chat.add(new User("Вася"));
        chat.add(new User("Петя"));
        chat.add(new User("Даша"));
        chat.notifySubscribers("Привет всем!");
    }
}

interface Subscriber {
    void invoke(String message);
}

interface Publisher {
    void add(Subscriber subscriber);
    void notifySubscribers(String message);
}

class User implements Subscriber {

    private final String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public void invoke(String message) {
        System.out.println(name + " got message: " + message);
    }
}

class Chat implements Publisher {
    private final List<Subscriber> subscribers = new ArrayList<>();

    @Override
    public void add(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void notifySubscribers(String message) {
        for(Subscriber s: subscribers) {
            s.invoke(message);
        }
    }
}
