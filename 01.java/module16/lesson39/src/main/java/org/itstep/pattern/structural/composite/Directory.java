package org.itstep.pattern.structural.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Directory implements FileSystemEntity {
    private final List<FileSystemEntity> entities = new ArrayList<>();
    private final String name;

    public Directory(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void addChild(FileSystemEntity child) {
        entities.add(child);
    }

    @Override
    public void removeChild(FileSystemEntity child) {
        entities.remove(child);
    }

    @Override
    public String toString() {
        if(entities.size() == 0) return name + "\n";
        return name + "\\" + entities.stream().map(FileSystemEntity::toString).collect(Collectors.joining("\\"));
    }
}
