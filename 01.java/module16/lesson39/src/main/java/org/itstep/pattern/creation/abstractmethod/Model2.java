package org.itstep.pattern.creation.abstractmethod;

public class Model2 extends Car {
    @Override
    public String getName() {
        return "Model2";
    }

    @Override
    public double getMaxSpeed() {
        return 100;
    }
}
