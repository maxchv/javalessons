package org.itstep.pattern.structural.adapter;

import java.io.*;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream("input.txt");
        InputStreamReader rdr = new InputStreamReader(in); // adapter (byte stream -> character stream)
        BufferedReader brd = new BufferedReader(rdr); // proxy
        String line = brd.readLine();
        System.out.println(line);
        in.close();
    }
}
