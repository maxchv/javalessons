package org.itstep.pattern.structural.composite;

public interface FileSystemEntity {
    String getName();
    void addChild(FileSystemEntity child);
    void removeChild(FileSystemEntity child);
}
