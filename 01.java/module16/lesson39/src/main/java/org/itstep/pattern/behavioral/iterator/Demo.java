package org.itstep.pattern.behavioral.iterator;

public class Demo {
    public static void main(String[] args) {
        Array<Integer> arr = new Array<>(new Integer[]{1,2,3,4});
        Array.Iterator<Integer> iterator = arr.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}

class Array<T> {

    private T[] arr;

    public Array(T[] arr) {
        this.arr = arr;
    }

    static class Iterator<T> {
        int idx = 0;
        Array<T> arr;
        public Iterator(Array<T> array) {
            arr = array;
        }

        public boolean hasNext() {
            return idx < arr.arr.length;
        }

        public T next() {
            return arr.arr[idx++];
        }
    }

    Iterator<T> iterator() {
        return new Iterator<>(this);
    }
}
