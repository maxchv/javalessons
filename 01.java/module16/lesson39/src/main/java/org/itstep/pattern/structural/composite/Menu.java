package org.itstep.pattern.structural.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;

public class Menu {
    private String name;
    private List<Menu> menuList = new ArrayList<>();
    private Action action;

    public Menu(String name, Action action) {
        this.name = name;
        this.action = action;
    }

    public void make(Context context) {
        action.doIt(context);
    }

    public void addSubmenu(Menu subMenu) {
        menuList.add(subMenu);
    }

    public void print() {
        System.out.println(name);
        for(Menu subMenu: menuList) {
            subMenu.print(name.length());
        }
    }

    public void print(int offset) {
        for(int i=0; i<offset; i++) System.out.print(' ');
        System.out.println(name);
    }

    // 1. Добавить задачу
    // >> Ведите название
    // Изучить паттерны
    // >> Укажите приоритет задачи
    // Высокий
    //


}

interface Context {

}

interface Action {
    void doIt(Context context);
}
