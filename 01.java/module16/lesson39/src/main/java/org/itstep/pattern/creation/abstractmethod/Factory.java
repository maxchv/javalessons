package org.itstep.pattern.creation.abstractmethod;

public interface Factory {
    Car newCar();
}
