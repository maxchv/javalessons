package org.itstep.pattern.creation.abstractfactory.product;

public abstract class ElectroCar extends Car {
    private int power;
    private int maxDistance;

    public ElectroCar(String model, int maxSpeed, String color, double price, int power, int maxDistance) {
        super(model, maxSpeed, color, price);
        this.power = power;
        this.maxDistance = maxDistance;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public String toString() {
        return "ElectroCar{" +
                "power=" + power +
                ", maxDistance=" + maxDistance +
                '}';
    }
}
