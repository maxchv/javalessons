package org.itstep.pattern.creation.objectpool;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CarRent {
    private static CarRent instance;
    private List<Car> carsPool = new CopyOnWriteArrayList<>();
    public static CarRent getInstance() {
        if(instance == null) {
            instance = new CarRent();
        }
        return instance;
    }
    private CarRent() {

    }

    public Car rent() {
        if(carsPool.size() > 0) {
            return carsPool.remove(0);
        }
        return new Car();
    }

    public void releas(Car car) {
        carsPool.add(car);
    }
}
