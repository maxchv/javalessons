package org.itstep.pattern.structural.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalDateTime;

public class Demo {
    public static void main(String[] args) {
        ICar car = new Car();
        ICar proxyCar = new ProxyCar(car);
        printModel(proxyCar);

        ICar pCar = (ICar) Proxy.newProxyInstance(ICar.class.getClassLoader(), new Class[]{ICar.class}, (proxy, method, args1) -> {
            System.out.println("Invoke method " + method.getName() + " at: " + LocalDateTime.now());
            return method.invoke(car, args1);
        });
        printModel(pCar);
    }

    static void printModel(ICar car) {
        car.getModel();
    }
}

class ProxyCar extends Car {
    ICar car;

    public ProxyCar(ICar car) {
        this.car = car;
    }

    @Override
    public String getModel() {
        System.out.println("Invoked method getModel() at: " + LocalDateTime.now());
        return car.getModel();
    }
}

interface ICar {
    String getModel();
}

class Car implements ICar {
    public String getModel() {
        return "Mercedes";
    }
}
