package org.itstep.pattern.structural.decorator;

import java.io.*;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream("input.txt");
        BufferedInputStream bin = new BufferedInputStream(in); 
        Scanner scanner = new Scanner(bin); // Decorator
        String line = scanner.nextLine();
        System.out.println(line);
        in.close();
    }
}
