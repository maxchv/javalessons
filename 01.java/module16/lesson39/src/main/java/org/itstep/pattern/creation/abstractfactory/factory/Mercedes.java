package org.itstep.pattern.creation.abstractfactory.factory;

import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;
import org.itstep.pattern.creation.abstractfactory.product.SportCar;
import org.itstep.pattern.creation.abstractfactory.product.mercedes.MecedesNewGenElectorCar;
import org.itstep.pattern.creation.abstractfactory.product.mercedes.MercedesSportCar;

public class Mercedes implements AbstractFactory{
    @Override
    public SportCar newSportCar() {
        return new MercedesSportCar("Mercedes sport", 250, "Yellow", 25_000);
    }

    @Override
    public ElectroCar newElectroCar() {
        return new MecedesNewGenElectorCar("Mercedes electro", 150, "Brown", 35_000, 10_000);
    }
}
