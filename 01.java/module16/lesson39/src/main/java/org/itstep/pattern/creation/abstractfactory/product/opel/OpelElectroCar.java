package org.itstep.pattern.creation.abstractfactory.product.opel;

import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;

public class OpelElectroCar extends ElectroCar {
    public OpelElectroCar(String model, int maxSpeed, String color, double price, int power, int maxDistance) {
        super(model, maxSpeed, color, price, power, maxDistance);
    }
}
