package org.itstep.pattern.behavioral.chainofresponsibility;

public class Demo {
    public static void main(String[] args) {
        Filter f1 = new Filter() {
            @Override
            void filter(String data) {
                if(nextHandler != null) {
                    nextHandler.filter("filter1: " + data);
                }
            }
        };

        Filter f2 = new Filter() {
            @Override
            void filter(String data) {
                if(nextHandler != null) {
                    nextHandler.filter("filer2: " + data);
                }
            }
        };
        f1.setNext(f2);

        Filter f3 = new Filter() {
            @Override
            void filter(String data) {
                System.out.println(data);
            }
        };
        f2.setNext(f3);

        f1.filter("Test data");
    }
}

abstract class Filter {
    Filter nextHandler;

    public void setNext(Filter filter) {
        nextHandler = filter;
    }

    abstract void filter(String data);
}
