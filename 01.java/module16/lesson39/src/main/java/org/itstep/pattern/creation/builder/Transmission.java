package org.itstep.pattern.creation.builder;

public enum Transmission {
    MANUAL, AUTO, ROBOT
}
