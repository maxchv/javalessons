package org.itstep.pattern.creation.abstractmethod;

public class GarageFactory implements Factory {
    @Override
    public Car newCar() {
        return new Model2();
    }
}
