package org.itstep.pattern.creation.abstractfactory.product.mercedes;

import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;

public class MercedesElectroCar extends ElectroCar {
    public MercedesElectroCar(String model, int maxSpeed, String color, double price, int power, int maxDistance) {
        super(model, maxSpeed, color, price, power, maxDistance);
    }
}
