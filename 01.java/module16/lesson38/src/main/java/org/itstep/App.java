package org.itstep;

import org.itstep.pattern.creation.abstractfactory.factory.AbstractFactory;
import org.itstep.pattern.creation.abstractfactory.factory.Mercedes;
import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;
import org.itstep.pattern.creation.abstractfactory.product.SportCar;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        AbstractFactory factory = new Mercedes();

        SportCar mercedesSport = factory.newSportCar();
        System.out.println("mercedesSport = " + mercedesSport);
        ElectroCar mecedesElectro = factory.newElectroCar();
        System.out.println("mecedesElectro = " + mecedesElectro);
    }
}
