package org.itstep.pattern.creation.abstractfactory.product;

public abstract class SportCar extends Car {

    public SportCar(String model, int maxSpeed, String color, double price) {
        super(model, maxSpeed, color, price);
    }
}
