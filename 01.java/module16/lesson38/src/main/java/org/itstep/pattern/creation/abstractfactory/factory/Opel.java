package org.itstep.pattern.creation.abstractfactory.factory;

import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;
import org.itstep.pattern.creation.abstractfactory.product.SportCar;
import org.itstep.pattern.creation.abstractfactory.product.opel.OpelElectroCar;
import org.itstep.pattern.creation.abstractfactory.product.opel.OpelSportCar;

public class Opel implements AbstractFactory {
    @Override
    public SportCar newSportCar() {
        return new OpelSportCar("Opel sprot", 150, "Black", 10_000);
    }

    @Override
    public ElectroCar newElectroCar() {
        return new OpelElectroCar("Opel Electro", 120, "White", 25_000, 5_000, 150);
    }
}
