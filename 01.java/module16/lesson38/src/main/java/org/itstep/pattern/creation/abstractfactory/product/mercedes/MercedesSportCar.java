package org.itstep.pattern.creation.abstractfactory.product.mercedes;

import org.itstep.pattern.creation.abstractfactory.product.SportCar;

public class MercedesSportCar extends SportCar {
    public MercedesSportCar(String model, int maxSpeed, String color, double price) {
        super(model, maxSpeed, color, price);
    }
}
