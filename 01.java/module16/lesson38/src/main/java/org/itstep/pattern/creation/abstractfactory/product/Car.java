package org.itstep.pattern.creation.abstractfactory.product;

public abstract class Car {
    private String model;
    private int maxSpeed;
    private String color;
    private double price;

    public Car(String model, int maxSpeed, String color, double price) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.color = color;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}
