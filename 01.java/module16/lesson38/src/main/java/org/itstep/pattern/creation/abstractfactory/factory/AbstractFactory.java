package org.itstep.pattern.creation.abstractfactory.factory;

import org.itstep.pattern.creation.abstractfactory.product.ElectroCar;
import org.itstep.pattern.creation.abstractfactory.product.SportCar;

public interface AbstractFactory {
    SportCar newSportCar();
    ElectroCar newElectroCar();
}
