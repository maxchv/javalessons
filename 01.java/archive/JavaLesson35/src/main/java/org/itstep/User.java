package org.itstep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private String login;
    private transient String password;
    private List<Task> tasks = new ArrayList<>();

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public String toString() {
        String strTasks = tasks.stream()
                .map(Task::toString)
                .collect(Collectors.joining(";"));
        return String.format("%s;%s;%s\n", login, password, strTasks);
    }
}
