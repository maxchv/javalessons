package org.itstep;

import java.io.*;
import java.time.LocalDateTime;

public class Task implements Serializable, Externalizable {
    private static final long serialVersionUID = 2L;

    private String title;
    private String email;
    private int importance;
    private LocalDateTime deadline;
    private boolean done;

    public Task(String title) {
        this.title = title;
    }

    public Task(String title, int importance) {
        this.title = title;
        this.importance = importance;
    }

    public Task(String title, int importance, LocalDateTime deadline) {
        this.title = title;
        this.importance = importance;
        this.deadline = deadline;
    }

    public Task(String title, int importance, LocalDateTime deadline, boolean done) {
        this.title = title;
        this.importance = importance;
        this.deadline = deadline;
        this.done = done;
    }

    @Override
    public String toString() {
        return String.format("%s|%d|%s|%s",title, importance, deadline, done);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
}
