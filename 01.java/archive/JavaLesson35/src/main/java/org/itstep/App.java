package org.itstep;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws IOException {
//        demo01();

        // Old Interface: class File
        File file = new File("c:\\devtools");
        String absolutePath = file.getAbsolutePath();
        System.out.println("file.getName() = " + file.getName());
        System.out.println("absolutePath = " + absolutePath);
        System.out.println("file.exists() = " + file.exists());
        System.out.println("file.isDirectory() = " + file.isDirectory());
        System.out.println("file.isFile() = " + file.isFile());
        System.out.println("file.createNewFile() = " + file.createNewFile());
        System.out.println("file.listFiles() = " + Arrays.stream(file.list()).collect(Collectors.joining(", ")));
        System.out.println("file.getTotalSpace() = " + file.getTotalSpace()/1024/1024/1024); // totalSpace
        System.out.println("file.getFreeSpace() = " + file.getFreeSpace()/1024/1024/1024);
        System.out.println("file.getUsableSpace() = " + file.getUsableSpace()/1024/1024/1024);

        // New Interface: interface Path, class Files
        Path pathToTarget = Paths.get("target"); // since Java 11: Path.of(..)
        boolean exists = Files.exists(pathToTarget);
        if(exists) {
            System.out.println(pathToTarget.toAbsolutePath() + " exists");
        }
        Files.list(pathToTarget).forEach(System.out::println);
        System.out.println("File.pathSeparator = " + File.pathSeparator);
        Files.lines(Path.of("src", "main", "java", "org", "itstep", "App.java")) // src\\main\\java\\org\\itstep\\App.java
                .map(line -> String.format("%-3d%s", line.length(), line))
                .forEach(System.out::println);
    }

    private static void demo01() {
        List<User> users = new ArrayList<>();
        pupulateUsers(users);
        try (ObjectOutput out = new ObjectOutputStream(new FileOutputStream("users.bin"))) {
            out.writeObject(users);
        } catch (IOException e) {
            e.printStackTrace();
        }
        users = null;
        try(ObjectInput in = new ObjectInputStream(new FileInputStream("users.bin"))) {
            users = (List<User>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(users);

//        readUsers(users);
//        writeUsers(users);
    }

    private static void readUsers(List<User> users) {
        try (BufferedReader reader = new BufferedReader(new FileReader("tasks.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.isBlank()) {
                    continue;
                }
                String[] parts = line.split(";");
                if (parts.length < 2) {
                    continue;
                }
                User user = new User(parts[0], parts[1]);
                users.add(user);
                if (parts.length > 2) {
                    for (int i = 2; i < parts.length; i++) {
                        String[] taskPats = parts[i].split("\\|");
                        if (taskPats.length != 4) {
                            continue;
                        }
                        int importance = Integer.parseInt(taskPats[1]);
                        LocalDateTime deadline = "null".equals(taskPats[2]) ? null : LocalDateTime.parse(taskPats[2]);
                        boolean done = Boolean.parseBoolean(taskPats[3]);
                        user.add(new Task(taskPats[0], importance, deadline, done));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(users);
    }

    private static void writeUsers(List<User> users) {
        pupulateUsers(users);

        try (Writer writer = new BufferedWriter(new FileWriter("tasks.txt"))) {
            for (User u : users) {
                writer.write(u.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void pupulateUsers(List<User> users) {
        User user = new User("admin", "admin");
        users.add(user);
        user.add(new Task("Выгулять собаку", 4, LocalDateTime.of(2022, 3, 7, 19, 0)));
        user.add(new Task("Сделать домашнее задание по Java"));
    }
}
