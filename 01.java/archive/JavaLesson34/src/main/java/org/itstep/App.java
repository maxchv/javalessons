package org.itstep;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        //demo01();
        //demo02();
        //demo03();

//        Group group = new Group("Java 12");
//        group.add(new Student("Вася", "Пупкин", "+380671234567"));
//        group.add(new Student("Маша", "Ефрасинина", "+380671234567"));
//        group.add(new Student("Рома", "Быков", "+380671234567"));
//        System.out.println(group);
//
//        try (OutputStream out = new FileOutputStream("group.bin");
//             ObjectOutput objectOutput = new ObjectOutputStream(out)) {
//            objectOutput.writeObject(group);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Group group = null;
        try (InputStream in = new FileInputStream("group.bin");
             ObjectInput objectInput = new ObjectInputStream(in)) {
            group = (Group) objectInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(group);
    }

    private static void demo03() {
        try (InputStream in = new URL("https://itstep.dp.ua").openStream();
//                Reader reader = new FileReader("file.txt", StandardCharsets.UTF_8)) {
             Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            bufferedReader.lines().forEach(System.out::println);
//            StringBuilder stringBuilder = new StringBuilder();
//            char[] buff = new char[1024];
//            int len;
//            while ((len = reader.read(buff)) > 0) {
//                stringBuilder.append(new String(buff, 0, len));
//            }
//            System.out.println("text = " + stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void demo02() {
        try (OutputStream out = new FileOutputStream("output.txt");
             PrintStream printStream = new PrintStream(out)) {
            printStream.println("It is really usefull");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void demo01() {
        try (InputStream in = new URL("https://itstep.dp.ua").openStream(); //new ByteArrayInputStream("Hello World".getBytes(StandardCharsets.UTF_8));//new FileInputStream("file.txt");
             BufferedInputStream bin = new BufferedInputStream(in);
             FileStringInputStream sin = new FileStringInputStream(bin)) {
//            int size = sin.available();
//            byte[] buff = new byte[size];
//            int len = sin.read(buff);
//            String text = new String(buff, 0, len, StandardCharsets.UTF_8);
            System.out.println("text = " + sin.readAllText());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Group implements Serializable {
    private String name;
    private List<Student> group = new ArrayList<>();

    public Group(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Группа: " + name + "\n" + group.stream().map(Student::toString).collect(Collectors.joining("\n"));
    }

    public void add(Student student) {
        group.add(student);
    }
}

class Student implements Serializable {
    private String firstName;
    private String lastName;
    private String phone;
    private String email;

    public Student(String firstName, String lastName, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("%-10s | %-10s | %-15s | %s", firstName, lastName, phone, email);
    }
}

// это декоратор
class FileStringInputStream extends InputStream {
    private final InputStream inputStream;

    FileStringInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return inputStream.read(b);
    }

    @Override
    public int available() throws IOException {
        return inputStream.available();
    }

    public String readAllText() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buff = new byte[1024];
        int len;
        while ((len = read(buff)) > 0) {
            stringBuilder.append(new String(buff, 0, len, StandardCharsets.UTF_8));
        }
        return stringBuilder.toString();
    }
}
