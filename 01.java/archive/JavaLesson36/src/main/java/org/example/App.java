package org.example;

import java.time.LocalDateTime;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Start at " + LocalDateTime.now());
//        demo01();
        Thread sleepingThread = new Thread(() -> {
            System.out.println("Start sleep at " + LocalDateTime.now());
            try {
                Thread.sleep(5000);
                System.out.println("End sleep at " + LocalDateTime.now());
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        });
        Thread workingThread = new Thread(() -> {
            System.out.println("Start working at " + LocalDateTime.now());
            long count = 0;
            for (int i = 0; i < 1000_000_000; i++) {
                count++;
                if(Thread.interrupted()) {
                    System.out.println("End count on " + count);
                    return;
                }
            }
            System.out.println("count = " + count);
            System.out.println("End working at " + LocalDateTime.now());
        });
        sleepingThread.start();
        workingThread.start();
        Thread.sleep(10);
        sleepingThread.interrupt();
        workingThread.interrupt();
        System.out.println("End at " + LocalDateTime.now());
    }

    private static void demo01() {
        Thread currentThread = Thread.currentThread();
        System.out.println("currentThread.getName() = " + currentThread.getName());
        // class FirstTread extends Thread
        FirstThread firstThread = new FirstThread("FirstThread");
        firstThread.setDaemon(true);
        firstThread.start();

        // class SecondThread implements Runnable
        Thread t = new Thread(new SecondThread());
        t.start();
        // using lambdas
        new Thread(() -> {
            System.out.println("Runnable thread using lambda");
        }).start();
        //firstThread.join();
    }
}

class SecondThread implements Runnable {

    @Override
    public void run() {
        System.out.println("Run at " + LocalDateTime.now() + " in " + Thread.currentThread().getName());
    }
}

class FirstThread extends Thread {

    public FirstThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        Thread currentThread = Thread.currentThread();
        System.out.println("Run something useful in thread " + currentThread.getName());
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100); // 1000ms = 1s
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(".");
        }
        System.out.println("Done at " + LocalDateTime.now());
    }
}
