package org.itstep;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws IOException {
        // demo01();

        // Байтовыве потоки (базовые абстрактные классы)
        // -> InputStream
        //       public abstract int read();
        // <- OutputStream
        //       public abstract void write(int byte);
        // demo02();
        // demo03();
        // demo04();
        // demo05();
        System.out.print("Your message: ");
        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();
        Xor xor = new Xor();
        String encode = xor.encode(message, 101);
        System.out.println("encode = " + encode);
    }

    private static void demo05() throws IOException {
        OutputStream outputStream = new FileOutputStream("output.txt");
        String string = "Hello World";
        byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        outputStream.write(bytes);
        outputStream.close();
    }

    private static void demo04() throws IOException {
        InputStream inputStream = new FileInputStream("file.txt");
        int size = inputStream.available(); // плучаем доступный поток данных
        System.out.print("size = " + size + "\r\n");
        byte[] buff = new byte[size];
        int len = inputStream.read(buff); // считываем содержимое файла и помещаем в массив байт buff. len - количество считанных байт
        System.out.println("len = " + len);
        String text = new String(buff, 0, len, StandardCharsets.UTF_8);
        System.out.println("text = " + text);
        inputStream.close();
    }

    private static void demo03() throws IOException {
        InputStream inputStream = new FileInputStream("file.txt");
        int ch;
        while((ch = inputStream.read()) != -1) {
            System.out.print(ch);
            System.out.print(" ");
        }
        inputStream.close();
    }

    private static void demo02() throws IOException {
        System.out.println("PrintStream"); // PrintStream -> OutputSteam
        int ch = System.in.read(); // InputSteam
        System.out.println(ch);
    }

    private static void demo01() {
        Random random = new Random(0);
        Stream.generate(() -> random.nextInt(100))
                .skip(5)
                .limit(10)
                .forEach(System.out::println);
        System.out.println("---");
        String str = "Hello World";
        String sortedString =
                Arrays.stream(str.split("\\s"))
                .peek(System.out::println)                   // Stream<Sting>
                .map(s -> s.chars().mapToObj(c -> (char) c)) // Stream<Stream<Character>>
                .flatMap(characterStream -> characterStream)
                .peek(System.out::println)                    // Stream<Character>
                .sorted()
                .map(String::valueOf)
                .collect(Collectors.joining());
        System.out.println("sortedString = " + sortedString);
    }
}

interface Encoder<T> {
    String encode(String str, T key);
}

interface Decoder<T> {
    String decode(String str, T key);
}

class Xor implements Encoder<Integer>, Decoder<Integer> {

    @Override
    public String encode(String str, Integer key) {
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] ^= key;
        }
        return new String(bytes, 0, bytes.length, StandardCharsets.UTF_8);
    }

    @Override
    public String decode(String str, Integer key) {
        return encode(str, key);
    }
}
