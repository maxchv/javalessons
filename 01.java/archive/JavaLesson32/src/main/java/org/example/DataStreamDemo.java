package org.example;

import java.io.*;

public class DataStreamDemo {
    public static void main(String[] args) {
        String filename = "cars.bin";
        saveToBinaryFile(filename);
        readFromBinaryFile(filename);
    }

    private static void readFromBinaryFile(String filename) {
        try(InputStream in = new FileInputStream(filename);
            DataInputStream din = new DataInputStream(in)) {
            //int count = din.readInt();
            //for (int i = 0; i < count; i++) {
            while (in.available() > 0) {
                String name = din.readUTF();
                String color = din.readUTF();
                double price = din.readDouble();
                System.out.println("name = " + name + "; color = " + color + "; price = " + price);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void saveToBinaryFile(String filename) {
        try(OutputStream out = new FileOutputStream(filename);
            DataOutputStream dout = new DataOutputStream(out)) {
            //dout.writeInt(2); // количество автомобилей

            dout.writeUTF("Mazda 6"); // 1-й автомобиль
            dout.writeUTF("ORANGE");
            dout.writeDouble(18000);

            dout.writeUTF("Mercedes");// 2-й автомобиль
            dout.writeUTF("WHITE");
            dout.writeDouble(100000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
