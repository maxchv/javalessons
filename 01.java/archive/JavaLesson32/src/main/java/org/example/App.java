package org.example;


import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class App {

    public static final String CARS_TXT = "cars.txt";

    public static void main(String[] args) {
        // 1. Написать приложение для создания и редактирования списка автомобилей
        // 2. try {} with resources
        // 3. DataInputStream, DataOutputStream
        CarShop carShop = new CarShop();
        carShop.loadCars(CARS_TXT);
        System.out.println("-= Welcome to car shop =-");
        Scanner scanner = new Scanner(System.in);
        int menu = 0;
        main:
        while (true) {
            do {
                System.out.print("You can:\n" +
                        "1. Add car\n" +
                        "2. Show all cars\n" +
                        "3. Exit\n" +
                        ">>> ");
                try {
                    menu = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException ex) {
                    System.err.println(ex.getMessage());
                }
            } while (menu < 1 || menu > 3);

            switch (menu) {
                case 1:
                    addCar(carShop);
                    break;
                case 2:
                    showCars(carShop);
                    break;
                case 3:
                    carShop.saveCars(CARS_TXT);
                    break main;
            }
        }
    }

    private static void showCars(CarShop carShop) {
        carShop.find(c -> true)
                .forEach(System.out::println);
    }

    private static void addCar(CarShop carShop) {
        String name;
        Color color;
        BigDecimal price;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Type name: ");
        name = scanner.nextLine();

        System.out.print("Type color " + Color.getAllNamesAsString() + ": ");
        String c = scanner.nextLine();
        color = Color.valueOf(c); // FIXME: handle exception IllegalArgumentException

        System.out.print("Type price: ");
        String p = scanner.nextLine();
        price = new BigDecimal(p); // FIXME: handle exception NumberFormatException

        carShop.add(new Car(name, color, price));
        System.out.println("Car was added successfully");
    }

}

enum Color {
    RED, WHITE, GREEN, BLUE, YELLOW, ORANGE;

    public static String getAllNamesAsString() {
        return Arrays.stream(values())
                .map(Enum::name)
                .collect(Collectors.joining(", ", "[", "]"));
    }
}

class Car {
    public static final String DIVIDER = ";";
    private final String name;
    private final Color color;
    private final BigDecimal price;

    public Car(String name, Color color, BigDecimal price) {
        this.name = name;
        this.color = color;
        this.price = price;
    }

    public byte[] toBytes() {
        String str = name + DIVIDER + color + DIVIDER + price + "\n";
        return str.getBytes(StandardCharsets.UTF_8);
    }

    public static Car parse(String str) {
        String[] parts = str.split(DIVIDER);
        if(parts.length != 3) {
            throw new IllegalArgumentException("Input string should contains three part divided by ';'");
        }
        return new Car(parts[0], Color.valueOf(parts[1]), new BigDecimal(parts[2]));
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", color=" + color +
                ", price=" + price +
                '}';
    }
}

class CarShop {
    private final List<Car> cars = new ArrayList<>();

    public void add(Car car) {
        cars.add(car);
    }

    public List<Car> find(Predicate<Car> carPredicate) {
        return cars.stream()
                .filter(carPredicate)
                .collect(Collectors.toList());
    }

    public void saveCars(String file) {
        try(OutputStream out = new FileOutputStream(file)) {
            for(Car car: cars) {
                out.write(car.toBytes());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadCars(String file) {
        try(InputStream in = new FileInputStream(file)) {
            String allText = "";
            byte[] buffer = new byte[in.available()];
            int len = in.read(buffer);
            if(len != buffer.length) {
                throw new RuntimeException("Read not all file");
            }
            allText = new String(buffer, 0, len, StandardCharsets.UTF_8);
            List<Car> inputCars = Arrays.stream(allText.split("\n"))
                    .filter(s -> s != null && !s.isBlank())
                    .map(Car::parse)
                    .collect(Collectors.toList());
            cars.addAll(inputCars);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class MyStream implements AutoCloseable {

    @Override
    public void close() {
        System.out.println("Close MyStream");
    }
}
