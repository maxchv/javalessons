package org.itstep;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * Hello world!
 */
public class App {
    static Random random = new Random();

    public static void main(String[] args) {
        // 1.  Как создать стрим?
        // Обобщенные стримы
        Stream<Integer> intStream = Stream.of(1, 4, 2, 4, 6, 7, 8, 2, 3);
        Stream<Object> int2Stream = Stream.builder().add(1).add(2).add(3).add(4).build();

        Integer[] arr = {1, 2, 3, 4};
        Stream<Integer> arrStream = Arrays.stream(arr);

        List<Integer> list = List.of(1, 2, 3, 4);
        Stream<Integer> listStream = list.stream();

        // Стримы примитивных данных

        IntStream ints = random.ints(100);
        //ints = IntStream.of();
        DoubleStream doubles = random.doubles();
        LongStream longs = random.longs();

        // 2.  Что можно делать с стримом?
        // 2.1 Конвеерные (не терминальный) методы (возвращают стирм) - ленивые (lazy)
        //     filter() - фильтрует данные,
        //     distinct() - возвращает стрим уникальных элементов,
        //     limit() - ограничивает количество элементов в стриме,
        //     skip() - позволяет пропустить заданное количество элементов в стриме,
        //     map() - преобразовать стрим,
        List<String> names = Stream
                .of(new Student("Вася"), new Student("Маша"), new Student("Рома"))
                .map(student -> student.getName())
                .collect(Collectors.toList());
        System.out.println("names = " + names);
        Stream<Integer> s1 = Stream.of(1);
        Stream<Integer> s2 = Stream.of(2);
        Stream
                .concat(s1, s2)
                .forEach(System.out::println);

        //     peek() - просмотреть стрим (для отладки),
        //     sorted() - сортировка,
        //     mapToXXX() - пробразование к примитивным стримам,
        //     flatMap() вложыенный стрим преобразовать в плоский,
        //     flatMapToXXX()
//        Predicate<Integer> evenPredicate = new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                return integer % 2 == 0;
//            }
//        };
//        evenPredicate = integer -> integer % 2 == 0;
//        Consumer<Integer> consumer = new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        };
        long c = intStream
                    .filter(integer -> integer % 2 == 0)
                    .distinct()
                    .sorted()
                    .peek(integer -> System.out.println(integer))
                    .count();
        System.out.println("c = " + c);

        int i = 10; // примитивный тип данных, хранится в стеке
        Integer ii = i; // объект, хранится в куче. Упаковка (auto boxing) перенесение данных из стека в кучу
        //                              IntStream -> Stream<Integer>

        Optional<Student> optionalStudent = loadStudent();
        optionalStudent.ifPresent(student -> System.out.println(student.getName()));

        // 2.2 Терминальные методы (не возвращаю стримы)
        //     collect() - преобразовать в нужную колекцию
        //     toArray()
        //     forEach() - позволяет перебрать элементы стрима (похож на peek(), но терминальный)
        //     max(), min()
        //     count()
        //     findFirst(), findAny() - найти первый элемент или любой подходящий
        //     anyMatch(), noneMatch(), allMatch() - проверяют соответсвие всего стрима неким условиям
        List<Integer> collect = random.ints(0, 100)
                .limit(10)
                .boxed()
                .collect(Collectors.toList());

        // 3.  Лямбда выражение
        Demo<Integer> demo = item -> System.out.println(item);

        // 4.  Дефолтные и статические методы в интефейсах
        Lst<Integer> lst = new ArrayList<>();
        System.out.println("lst.size() = " + lst.size());

        // 5.  Функциональные интерфейсы
        Predicate<Integer> predicate;
        Consumer<Integer> consumer;
        Supplier<Integer> supplier;
        Function<Integer, String> function;
        BiConsumer<Integer, String> biConsumer;

        // 6.  Класс Optional
        Optional<Integer> optionalMax = ints.boxed().max(Integer::compare);
        optionalMax.ifPresent(integer -> System.out.println("optionalMax.get() = " + integer));

        // 7.  Ссылки на методы
        usingDemo(App::print, "Hello World");

        usingCommand(System.out::println, "Argument");
    }

    static void usingCommand(Command command, String arg) {
        command.execute(arg);
    }

    interface Command {
        void execute(String arg);
    }

    static <T> void print(T data) {
        System.out.println(data);
    }

    static <T> void usingDemo(Demo<T> demo, T item) {
        demo.apply(item);
    }

    interface Lst<T> {
        T get(int i);

        void add(T i);

        default int size() {
            return 0;
        }
    }

    static class ArrayList<T> implements Lst<T> {

        @Override
        public T get(int i) {
            return null;
        }

        @Override
        public void add(T i) {

        }

//        @Override
//        public int size() {
//            return 10;
//        }
    }

    @FunctionalInterface
    interface Demo<T> {
        void apply(T item);

        default void applyAll(T... items) {
            for(T item: items) {
                apply(item);
            }
        }
    }

    static Optional<Student> loadStudent() {
        if(random.nextBoolean()) {
            return Optional.of(new Student("Вася"));
        }
        return Optional.empty();
    }
}

class Student {
    private final String name;

    Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
