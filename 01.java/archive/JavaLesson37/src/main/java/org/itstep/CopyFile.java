package org.itstep;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CopyFile {
    static class CopyFileThread extends Thread {
        private final InputStream in;
        private final OutputStream out;

        CopyFileThread(InputStream in, OutputStream out) {
            this.in = in;
            this.out = out;
        }

        @Override
        public void run() {
            copy();
        }

        private void copy() {
            byte[] buff = new byte[1024];
            int len;
            while (true) {
                try {
                    synchronized (in) {
                        if ((len = in.read(buff)) > 0) {
                            out.write(buff, 0, len);
                        } else {
                            break;
                        }
                        System.out.printf("Thread %s wrote %d bytes\n", getName(), len);
                    }
                    Thread.sleep(10);
                } catch (Exception e) {
                    break;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException {
        final String origFileName = "alice.txt";
        final String copyFileName = origFileName.replace(".txt", "_copy.txt");
        System.out.printf("Copy file %s to %s\n", origFileName, copyFileName);
        String md5SumForOrigFile = getMd5SumForFile(origFileName);
        InputStream in = Files.newInputStream(Path.of(origFileName));
        OutputStream out = Files.newOutputStream(Path.of(copyFileName));
        CopyFileThread[] threads = new CopyFileThread[10];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new CopyFileThread(in, out);
        }
        for (CopyFileThread thread : threads) {
            thread.start();
        }
        for (CopyFileThread thread : threads) {
            thread.join();
        }
        in.close();
        out.close();
        String md5SumForCopyFile = getMd5SumForFile(copyFileName);
        System.out.println("md5 for alice.txt is " + md5SumForOrigFile);
        System.out.println("md5 for alice_copy.txt is " + md5SumForCopyFile);
        if (md5SumForOrigFile.equals(md5SumForCopyFile)) {
            System.out.println("Files are equal");
        } else {
            System.out.println("Files are not equal");
        }
    }

    public static String getMd5SumForFile(String filename) throws IOException, NoSuchAlgorithmException {
        byte[] bytes = Files.readAllBytes(Path.of(filename));
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(bytes);
        StringBuilder sb = new StringBuilder();
        for (byte b : md5.digest()) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString().toUpperCase();
    }
}
