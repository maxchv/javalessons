package org.itstep;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        //demo01();
        Collection<Product> products = new HashSet<>();
        products.add(new Product("Сало", 230));
        products.add(new Product("Сало", 0));
        products.add(new Product("IPhone", 23_000));
        products.add(new Product("IPhone", 0));
        products.add(new Product("TV", 53_000));
        products.add(new Product("TV", 0));
        for(Product p: products) {
            System.out.println(p);
        }
    }

    private static void demo01() {
        // Vector like StringBuffer
        // ArrayList (StringBuilder)
        List<Product> products = new LinkedList<>(); //new ArrayList<>();
        // [,,,,,,,,,,,]
        products.add(new Product("Сало", 230));
        // [1,,,,,,,,,,]
        products.add(new Product("IPhone", 23_000));
        // [1, 2,,,,,,,]
        products.add(new Product("TV", 53_000));
        // [1, 2, 3,,,,]

        System.out.println("Вы продаете сало? " + products.contains(new Product("Сало", 0)));

        products.remove(1);

        /*
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }*/
        for (Product p: products) {
            System.out.println(p);
        }
        // [0, 1->2, 2->3, 3->4]
        long start = System.currentTimeMillis();
        products.add(0, new Product("Мороженное", 15));
        System.out.println("dt: " + (System.currentTimeMillis() - start));

        System.out.println(products.get(0));

        products = new ArrayList<>(products);
    }
}

class Product {
    private static int count;
    private int id;
    private String name;
    private double price;

    public Product(String name, double price) {
        this.id = ++count;
        this.name = name;
        this.price = price;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Product) {
            Product other = (Product)obj;
            return name.equals(other.name);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
