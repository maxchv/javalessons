package org.itstep;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        //demo1();
        //demo2();
        //demo3();

        // Unmodified Collections
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list = Collections.unmodifiableList(list);
        try {
            list.add(4);
        }catch (UnsupportedOperationException ex) {
            ex.printStackTrace();
        }

        // Immutable Collections


    }

    private static void demo3() {
        Map<String, Set<String>> networkMap = new HashMap<>();
        Set<String> fox = null;
        if(!networkMap.containsKey("FOX")) {
            fox = new TreeSet<>();
            networkMap.put("FOX", fox);
        }
        if(networkMap.containsKey("FOX")) {
            fox = networkMap.get("FOX");
        }
        fox.add("The Simpsons");
        System.out.println(networkMap);
    }

    private static void demo2() {
        Map<String, String> dic = new HashMap<>();
        dic.put("hello", "привет");
        dic.put("person", "человек");
        dic.put("java", "язык программирования");

        System.out.println(dic);

        Scanner scanner = new Scanner(System.in);
        String line;
        while(!(line = scanner.nextLine()).equals(".")){
            if(dic.containsKey(line)) {
                System.out.println(dic.get(line));
            } else {
                System.err.println("Такого слова нет в словаре");
            }
        }
    }

    private static void demo1() {
        Set<Person> set = new TreeSet<>((o1, o2) -> o1.getName().compareTo(o2.getName()));
        Scanner scanner = new Scanner(System.in);
        String line;
        System.out.println("Введите набор данных");
        while(!(line = scanner.nextLine()).equals(".")){
            set.add(new Person(line));
        }

        for(Person data: set) {
            System.out.println(data);
        }
    }
}

class Person implements Comparable<Person>  {
    private final static Random rnd = new Random();
    private int id;
    private String name;

    public Person(String name) {
        id = rnd.nextInt();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        return id - o.id;
    }
}