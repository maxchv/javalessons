package org.itstep;

import java.io.IOException;
import java.util.Iterator;

public class BinaryTree<K extends Comparable<K>, V> implements Iterable<K> {



    private static class Node<K, V>{
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private Node<K, V> root;

    public void put(K key, V value) {
        Node<K, V> node = new Node<>(key, value);
        if(root == null) {
            root = node;
        } else {
            put(root, node);
        }
    }

    // 3, 7, 6, 10
    //    5
    //   / \
    //  3   7
    //     / \
    //    6  10
    private void put(Node<K, V> to, Node<K, V> node) {
        if(node.key.compareTo(to.key) < 0) {
            if(to.left == null) {
                to.left = node;
            } else {
                put(to.left, node);
            }
        } else if(node.key.compareTo(to.key) > 0) {
            if(to.right == null) {
                to.right = node;
            } else {
                put(to.right, node);
            }
        }
    }

    public V get(K key) {
        return null;
    }

    @Override
    public Iterator<K> iterator() {
        return null;
    }

    public static void main(String[] args) throws IOException {
        BinaryTree<Integer, String> tree = new BinaryTree<>();
        tree.put(5, "");
        tree.put(3, "");
        tree.put(7, "");
        tree.put(6, "");
        tree.put(10, "");

        for(Integer k: tree) {
            System.out.println(tree.get(k));
        }
//        Files.walkFileTree(Paths.get("."), new FileVisitor<Path>() {
//            @Override
//            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//                return FileVisitResult.CONTINUE;
//            }
//
//            @Override
//            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
//                System.out.println(file);
//                return FileVisitResult.CONTINUE;
//            }
//
//            @Override
//            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
//                return FileVisitResult.CONTINUE;
//            }
//
//            @Override
//            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
//                return FileVisitResult.CONTINUE;
//            }
//        });
    }
}
