-- -----------------------------
-- Atomicity   — Атомарность
--      TCL
-- Consistency — Согласованность
-- Isolation   — Изолированность
-- 		set transaction isolation level <level>;
-- Durability  — Долговечность
-- ------------------------------
use online_shop;
SELECT * FROM orderitems;

INSERT INTO `orderitems`(`OrderID`,`ProductID`,`Quantity`,`UnitPrice`)
VALUES(1, 1, 10, 20);

-- TCL - Transaction Control Language
-- COMMIT, ROLLBACK, BEGIN/START TRANSACTION
use usergallery;

select * from role;

insert into role(id, role) values(1, 'ROLE_ADMIN');

-- set autocommit=1; -- отмена автотранзакций

insert into role(role) values('ROLE_GUEST');

commit;
-- rollback;

start transaction;
delete from role where id=0;
commit;

-- create table test
-- (
-- 	data varchar(255)
-- )engine=memory;
