package org.itstep;

import java.sql.*;

public class App {

    public static final String URL = "jdbc:mysql://localhost/online_shop";
    public static final String USER = "root";
    public static final String PASSWORD = "";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             Statement stmt = conn.createStatement()) {
            //stmt.execute("USE online_shop");
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            conn.setAutoCommit(false); // set autocommit=0;
            try {
                int rows = stmt.executeUpdate("insert into customers(firstname, middlename, lastname, address, email, phone)" +
                        "values('Vasja', 'Vasilievitch', 'Pupkin', 'Here', 'vasja@mail.com', '(012) 123-45-67')");
                conn.commit(); // commit
            } catch (Exception ex) {
                conn.rollback(); // rollback
            }
            conn.setAutoCommit(true); // set autocommit=1

            ResultSet rs = stmt.executeQuery("select firstname, lastname, email, phone from customers");
            ResultSetMetaData metaData = rs.getMetaData();
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                System.out.printf("%30s", metaData.getColumnName(i + 1));
                if (i != metaData.getColumnCount() - 1) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
            while (rs.next()) {
                System.out.printf("%30s | %30s | %30s | %30s\n", rs.getString(1),
                        rs.getString("lastname"), rs.getString("email"),
                        rs.getString("phone"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
