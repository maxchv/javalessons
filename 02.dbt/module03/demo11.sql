use sakila;
explain select * from customer where customer_id = 594; -- last_name = 'hiatt' -- first_name = 'eduardo'
explain select * from customer where last_name = 'hiatt';
explain select * from customer where first_name = 'eduardo';
show indexes from customer;
drop index idx_last_name on customer;
create index idx_first_name on customer(first_name);
alter table customer
add index (last_name);

desc customer;
show tables;

drop table if exists tbl;
create table tbl
(
	data varchar(100),
	index (data) using hash
) engine=memory;
show indexes from tbl;

-- TCL (Transaction Control Language): START TRANSACTION, COMMIT, ROLLBACK

-- DCL (Data Control Language) GRANT, REVOKE
-- set password for current user
set password=password('qwerty');
-- create new user with password 'vasja'
create user 'vasja'@'localhost' identified by 'vasja';

drop user 'max'@'localhost';
use academy;
grant all privileges on academy.students to 'max'@'localhost' identified by 'max'; -- with grant options;
-- give access to read data from database academy
grant select on academy.groups to 'max'@'localhost';
revoke delete on academy.students from 'max'@'localhost';
show grants;
revoke all on movies.movies from 'vasja'@'localhost';
grant all on movies.movies to 'max'@'localhost' with grant option;
grant all on sakila.film to 'vasja'@'localhost';

use mysql;
show tables;
select * from tables_priv;