create database if not exists movies default charset utf8;

use movies;

create table if not exists genres
(
   id int primary key auto_increment not null,
   name varchar(50) not null unique
);

create table if not exists actors
(
   id int primary key auto_increment not null,
   first_name varchar(50) not null,
   last_name varchar(50) not null,
   nationality varchar(50),
   birth_date date
);

create table if not exists directors
(
   id int primary key auto_increment not null,
   first_name varchar(50) not null,
   last_name varchar(50) not null,
   nationality varchar(50),
   birth_date date
);

create table if not exists movies
(
   id int primary key auto_increment not null,
   title varchar(50) not null,
   release_year int,
   rating int,
   plot text,
   length int
);

create table if not exists movies_genres
(
   movie_id int not null,
   genre_id int not null,
   primary key(movie_id, genre_id),
   constraint movies_genres_fk
   foreign key(movie_id)
   references movies(id),
   foreign key(genre_id)
   references genres(id)
   on delete cascade
);

create table if not exists movies_actors
(
   movie_id int not null,
   actor_id int not null,
   primary key(movie_id, actor_id),
   constraint movies_actors_fk
   foreign key(movie_id)
   references movies(id),
   foreign key(actor_id)
   references actors(id)
   on delete cascade
);

insert into genres(name)
values ('Action'), ('Adventure'), ('Comedy'), ('Drama'),
	   ('Fantasy'), ('Horror'), ('Sci-fi'), ('Thriller'),
       ('Biography'), ('Romance'), ('History'), ('Crime'),
       ('Sport');

-- select * from genres;

insert into actors
values
(1, 'Tom', 'Hardy', 'English', '1977-09-15'),
(2, 'Ji-cheol', 'Gong', 'South Korean', '1979-07-10'),
(3, 'Daniel Jacob', 'Radcliffe', 'English', '1989-07-23'),
(4, 'Robert John', 'Downey Jr.', 'American', '1965-04-04'),
(5, 'Benjamin Thomas', 'Barnes', 'English', '1981-08-20'),
(6, 'Seung-ryong', 'Ryu', 'South Korean', '1970-11-29'),
(7, 'Andrew Russell', 'Garfield', 'English/American', '1983-08-20'),
(8, 'Edward John David', 'Redmayne', 'English', '1982-01-06'),
(9, 'Thomas Stanley', 'Holland', 'English', '1996-06-01'),
(10, 'Robert Brydon', 'Jones', 'Welsh', '1965-05-03');

-- select * from actors;

insert into directors
values
(1, 'Andrew Clement', 'Serkis', 'English', '1964-04-20'),
(2, 'Sang-ho', 'Yeon', 'South Korean', '1978-01-01'),
(3, 'David', 'Yates', 'English', '1963-10-08'),
(4, 'Anthony', 'Russo', 'American', '1970-02-03'),
(5, 'Oliver', 'Parker', 'English', '1960-09-06');

-- select * from directors;

alter table movies
add column director_id int not null,
add constraint movies_directors_fk
foreign key(director_id) references directors(id);

insert into movies
values
(1, 'Venom: Let There Be Carnage', 2021, 6, 'Eddie Brock attempts to reignite
 his career by interviewing serial killer Cletus Kasady, who becomes the host 
 of the symbiote Carnage and escapes prison after a failed execution.', 97, 1),
   
(2, 'Train to Busan', 2016, 7, 'While a zombie virus breaks out in South Korea,
 passengers struggle to survive on the train from Seoul to Busan.', 118, 2),
 
(3, 'Harry Potter and the Deathly Hallows – Part 2', 2011, 8, 'Harry, Ron,
 and Hermione search for Voldemort`s remaining Horcruxes in their effort to 
 destroy the Dark Lord as the final battle rages on at Hogwarts.', 130, 3),
   
(4, 'Avengers: Endgame', 2019, 8, 'The universe is in ruins. With the help
 of remaining allies, the Avengers assemble once more in order to reverse 
 Thanos` actions and restore balance to the universe.', 181, 4),
   
(5, 'Dorian Gray', 2009, 5, 'A corrupt young man somehow keeps his youthful
 beauty eternally, but a special painting gradually reveals his inner ugliness 
 to all.', 112, 5),
 
 (6, 'Psychokinesis', 2018, 6, 'After drinking water from a mountain spring, a 
 bank security guard gains telekinetic superpowers, which he must use to save his 
 estranged daughter from an evil construction company, as a superhero.', 101, 2),
 
 (7, 'Breathe', 2017, 6, 'The inspiring true love story of Robin (Andrew Garfield) 
 and Diana Cavendish (Claire Foy), an adventurous couple who refuse to give up in 
 the face of a devastating disease.', 117, 1),
 
 (8, 'Fantastic Beasts and Where to Find Them', 2016, 8, 'The adventures of writer 
 Newt Scamander in New York`s secret community of witches and wizards seventy 
 years before Harry Potter reads his book in school.', 133, 3),
 
 (9, 'Cherry', 2021, 5, 'Cherry drifts from college dropout to army medic in Iraq. 
 But after returning from the war with PTSD, his life spirals into drugs and 
 crime as he struggles to find his place in the world.', 141, 4),
 
 (10, 'Swimming with Men', 2018, 4, 'A man who is suffering a mid-life crisis 
 finds new meaning in his life as part of an all-male, middle-aged, amateur 
 synchronised swimming team.', 96, 5);

-- select * from movies;

insert into movies_genres
values
(1, 1), (1, 2), (1, 3), (1, 5), (1, 7),
(2, 1), (2, 6), (2, 8),
(3, 2), (3, 5),
(4, 1), (4, 2), (4, 5), (4, 7),
(5, 4), (5, 6), (5, 8),
(6, 1), (6, 3), (6, 5),
(7, 4), (7, 9), (7, 10), (7, 11),
(8, 1), (8, 2), (8, 5),
(9, 4), (9, 12),
(10, 3), (10, 4), (10, 13);

-- select * from movies_genres;

insert into movies_actors
values 
(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), 
(6, 6), (7, 7), (8, 8), (9, 9), (10, 10);

-- select * from movies_actors;

-- 1. Выбрать все фильмы и отсортировать по году выпуска в порядке убывания (новые фильмы раньше)
select title, release_year from movies order by release_year desc, title;

-- 2. Выбрать все фильмы и выпущенные в прошлом году отсортированные по рейтингу (в порядке убывания)
select title, rating from movies where release_year = 2021 order by rating desc, title;

-- 3. Выбрать все фильмы по заданному жанру (комедия, боевик, ...)
select title, name
from movies, movies_genres, genres
where genres.id = genre_id and movies.id = movie_id 
and name like 'Fantasy';

-- 4. Выбрать все фильмы по заданному имени и фамилии актера 
select title, first_name, last_name
from movies, movies_actors, actors
where actors.id = actor_id and movies.id = movie_id 
and first_name like 'Ji-cheol%' and last_name like '%Gong';

-- 5. Выбрать все фильмы по заданному имени и фамилии продюсера
select title, first_name, last_name
from movies, directors
where directors.id = director_id
and first_name like 'Sang-ho%' and last_name like '%Yeon';

-- 6. Отобразить информацию по фильмам с названием жанра, именем, фамилией и национальностью продюсера
select title, name, first_name, last_name, nationality
from movies, genres, movies_genres, directors
where genres.id = genre_id and movies.id = movie_id and directors.id = director_id;