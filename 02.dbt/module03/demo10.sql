use academy;

-- turn off auto commit
set autocommit = 1;
INSERT INTO `academy`.`students`
(
`first_name`,
`last_name`,
`email`,
`group_id`,
`birth_date`)
VALUES
(
'Roma',
'Romanov',
'roma2@mail.com',
1,
'2000-01-01');
commit;
begin work; -- implicit transaction
delete from students where first_name='Roma' and last_name='Romanov' and id > 0;
commit;
-- rollback;
select * from students;
select * from groups;

-- Atomacity 
begin work;
	begin work;
		INSERT INTO `students`(`first_name`, `last_name`, `email`, `group_id`, `birth_date`)
		VALUES ('Vova4', 'Vova4', 'vova4@mail.com', 1, '2000-01-01');	
	rollback;
    INSERT INTO `students`(`first_name`, `last_name`, `email`, `group_id`, `birth_date`)
	VALUES ('Roma', 'Romanov', 'roma3@mail.com', 1000, '2000-01-01');
commit;

select groups.name, group_concat(concat(first_name, ' ', last_name) separator ', ') from groups
left join students on group_id = groups.id
group by groups.name
