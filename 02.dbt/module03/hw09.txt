Доска с процедурами и циклами: https://jamboard.google.com/d/1wgUozjmZL12DAk9hwk973NJ10OxEt2ZVVK9ERmdXa60/edit?usp=sharing
Доска с триггерами и курсорами: https://jamboard.google.com/d/1gnif3DeGEBNerRXGJL0yM_a-Wzx_DoZxCvYsOHQyeJc/edit?usp=sharing

Есть следующие таблицы в базе данных телефонной компании:

CUSTOMER (Id, FirstName, LastName, PhoneNum, PlanId)
PRICINGPLAN (Id, ConnectionFee, PricePerSecond )
PHONECALL (Id, StartCall, CalledNum, Seconds, CustomerId)
BILL (Id, Month, Year, Amount, CustomerId)

Диаграмма: https://drawsql.app/maxchv/diagrams/hw09-cursors

Задание 1:
Запретить удаление и обновление данных в таблице PHONECALL - только вставка

Задание 2:
Напишите триггер, который автоматически обновляет счет (amount) за звонки в таблице Bill
в случае добавления записи в таблицу PHONECALL
В таблице Bill будет только одна запись для каждого клиента на каждый месяц