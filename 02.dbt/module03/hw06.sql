-- 4. Вывести самый фильм с самым высоким и самым низким рейтингом для каждого актера
select a.first_name, a.last_name, m.title, m.rating
from actors a
join movies_actors ma on a.id = ma.actor_id
join movies m on m.id = ma.movie_id
order by last_name, first_name;

select a.first_name, a.last_name, mm.title, mm.rating from movies mm
join movies_actors mma on mm.id = mma.movie_id
join actors a on a.id = mma.actor_id
where mm.rating = (select min(m.rating) mx
from movies_actors ma
join movies m on m.id = ma.movie_id
where ma.actor_id = mma.actor_id
group by ma.actor_id)
order by a.last_name, a.first_name;

with ratings as (select actor_id, min(m.rating) mn 
				from movies_actors ma
				join movies m on m.id = ma.movie_id
				group by ma.actor_id),
	 min_ratings as 
				(select a.first_name, a.last_name, mm.title, mm.rating from movies mm
					join movies_actors mma 
						on mm.id = mma.movie_id
					join actors a 
						on a.id = mma.actor_id
					where mm.rating = (select mn from ratings r where r.actor_id = mma.actor_id)
					order by a.last_name, a.first_name)
select * from min_ratings;

-- Представление - именованный запрос
-- 1. Для упрощения написания SQL запросов
-- 2. Безопасность
-- 3. Обеспечение совместимости с клиентами при изменении структуры бд

create view movie_view
as
select * from movies;

select * from movie_view;
insert into movie_view(title, release_year, rating, plot, length, director_id)
values ('title', 2022, 10, 'simple plot', 100, 1);
update movie_view set title='New title' where id=11;
delete from movie_view where id=11;

create view movies_all
as
select a.first_name 'Actor first name', a.last_name 'Actor last name', m.title 'Title', m.rating 'Rating', 
g.name 'Ganre', d.first_name 'Director first name', d.last_name 'Actor director name'
from actors a
join movies_actors ma on a.id = ma.actor_id
join movies m on m.id = ma.movie_id
join movies_genres mg on m.id = mg.movie_id
join genres g on mg.genre_id = g.id
join directors d on m.director_id = d.id;

select * from movies_all;

create view title_rating_view
as
select Title, Rating from movies_all;

select * from title_rating_view;