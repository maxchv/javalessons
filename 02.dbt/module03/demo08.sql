use academy;

create function full_name(first_name varchar(100), last_name varchar(100))
returns varchar(200)
return concat(first_name, ' ', last_name);

drop procedure if exists show_students_info_proc;
create procedure show_students_info_proc()
select full_name(first_name, last_name) 'Full Name' , email 'Email' from students;

-- вызываем процедуру
call show_students_info_proc();

select version();
delimiter $
select version()$
delimiter ;

--  Задача: Если число делится на 3, то вернуть foo. Если на 5, то bar. Если на оба, то foobar
delimiter $
create function foo_bar(num int)
returns varchar(6)
begin
	if num % 3 = 0 and num % 5 = 0 then
		return 'foobar';
	elseif num % 5 = 0 then
		return 'bar';
	else
		return 'foo';
	end if;
end$
delimiter ;

select id 'Id', foo_bar(id) 'Foo or Bar', monthname(curdate()) from students;

-- переменные вне процедур и функций
set @now = curdate();
select count(*) into @cnt from students;
select @cnt, @now;

create procedure proc_random(out num float)
set num = rand();

call proc_random(@res);
select @res;

create procedure prod(in multiplicator int, inout num int)
select multiplicator * num into num;

set @n = 10;
call prod(2, @n);
select @n;

drop procedure if exists proc_var;
delimiter $
create procedure proc_var()
begin
	declare cnt int; 
	declare age float default 0;
	declare str varchar(100) default '';
	select count(*) into cnt from students;
    select avg(datediff(curdate(), birth_date))/365 into age from students;
    select cnt, age, str;
end$
delimiter ;

call proc_var();
drop function if exists season;
delimiter $
create function season(d date)
returns varchar(10)
begin
	declare num int;
    set num = month(d);
	return case
		when num in (12, 1, 2) then 'Winter'
		when num between 3 and 5 then 'Spring'
		when num between 6 and 8 then 'Summer'
		else 'Fall'
	end;
end$
delimiter ;

select season(now());

select first_name, last_name, 
	case year(birth_date)
		when 2000 then 'Millenial'
    end
 from students;
