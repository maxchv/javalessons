-- select (DML|DQL)
/*
	SELECT [DISTINCT] <col1>, <col2>, ... 
		FROM <table1>, <table2>, ...
		[WHERE <condition>]
        [GROUP BY <expression> [HAVING <condition>]]
        [ORDER BY <col1>, <col2>,,,]
        [LIMIT <rows>] [OFFSET <offset>]        
*/
use online_shop;
select * from customers;

SELECT o.CustomerId, FirstName, LastName, OrderDate 
   FROM orders o, customers c 
   WHERE o.CustomerID=c.CustomerID;

CREATE VIEW CusomerOrders
as   
SELECT o.CustomerId Id, FirstName, LastName, OrderDate 
   FROM orders o 
   JOIN customers c 
   USING(CustomerID);
   -- ON   o.CustomerId=c.CustomerId;