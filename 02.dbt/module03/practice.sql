create database if not exists practice default charset utf8;

use practice;

create table if not exists students
(
   id int primary key auto_increment not null,
   first_name varchar(50) not null,
   last_name varchar(50) not null,
   group_id int
);

create table if not exists `groups`
(
   id int primary key auto_increment not null,
   name varchar(50) not null unique
);

alter table students
add constraint students_groups_no_action_fk
   foreign key (group_id) references `groups`(id)
   on delete no action;

insert into `groups`
values
(1, 'Java12'), 
(2, 'Java10');

insert into students
values
(1, 'John', 'Smith', 1),
(2, 'Mary', 'Jane', 2),
(3, 'Peter', 'Parker', 2);

alter table students
-- drop constraint students_groups_cascade_fk,
add constraint students_groups_set_null_fk
   foreign key (group_id) references `groups`(id)
   on delete set null;
select * from groups;
select * from students;
delete from groups where id=1;

select students.id, first_name, last_name, name 
from students, groups
where group_id = groups.id;

select * from students where group_id is null;
select * from students where group_id is not null;

