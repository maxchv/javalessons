use academy;
show triggers;
select * from logging;

-- триггер перед добавлением
delimiter $
create trigger after_students_insert
after insert on students
for each row
begin
	declare first_name varchar(50);
    declare last_name varchar(50);
    declare email varchar(50);
    declare birth_date date;
    declare group_id int;
    
    set first_name = NEW.first_name;
    set last_name = NEW.last_name;
    set email = NEW.email;
    set birth_date = NEW.birth_date;
    set group_id = NEW.group_id;
	
    insert into logging(`table`, event, data)
    values('students', 
		   'insert', 
           concat(first_name, ' ', last_name, 
				  ', ', email, 
                  ', ', cast(birth_date as varchar(20)), 
                  ', ', cast(group_id as varchar(10))));
end$
delimiter ;

insert into students(first_name, last_name, email, birth_date, group_id)
values ('Саша', 'Растеряша', 'alex@mail.com', '2000-01-01', 1);

-- проверить, что возраст студента не менее 18 лет
/*create table studentsd
(
	birth_date date check(birth_date > now())
);*/
drop trigger if exists check_age_before_student;
delimiter $
create trigger check_age_before_student
before insert on students
for each row
begin
	declare age int;
    set age = datediff(curdate(), new.birth_date)/365;
    if age < 18 then
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Student shoul be at least 18 years old';
    end if;
end$
delimiter ;

select * from students;

insert into students(first_name, last_name, email, birth_date, group_id)
values ('Машыш', 'Смешиш', 'malish2@mail.com', '2022-01-01', 1);

-- Циклы. Задача: найти арифметическую прогрессию чисел в диапазоне от a до b
-- While
delimiter $
create procedure proc_sum_while(a int, b int, out result int)
begin
	set result = 0;
	while a <= b do
		set result = result + a;
        set a = a + 1;
    end while;
end$
delimiter ;
call proc_sum_while(1, 10, @num);
select @num;

-- Repeat
drop procedure if exists proc_sum_repeat;
delimiter $
create procedure proc_sum_repeat(a int, b int, out result int)
begin
	set result = 0;
    repeat
		set result = result + a;
        set a = a + 1;
    until a > b
    end repeat;
end$
delimiter ;
call proc_sum_repeat(1, 10, @num);
select @num;

-- Loop
drop procedure if exists proc_sum_loop;
delimiter $
create procedure proc_sum_loop(a int, b int, out result int)
begin
	set result = 0;
    lbl: loop
		if a > b then
			leave lbl;
        end if;
		set result = result + a;
        set a = a + 1;
    end loop;
end$
delimiter ;
call proc_sum_loop(1, 10, @num);
select @num;

-- Курсоры
drop procedure if exists average_age;
delimiter $
create procedure average_age(out result decimal(4,1))
begin
	declare age date;
    declare i int default 0;
	declare break bit default 0;
	declare c cursor for 
		select birth_date from students where birth_date is not null;
    declare continue handler for not found
		set break = 1;
	set result = 0;
	open c;
	lbl: loop		
        fetch c into age;
        if break = 1 then 
			leave lbl;
		end if;
        set result = result + datediff(curdate(), age)/365;
        set i = i + 1;
    end loop;
    close c;
    set result = result / i;
end$
delimiter ;

call average_age(@age);
select @age;