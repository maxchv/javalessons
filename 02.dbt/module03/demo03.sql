create database if not exists todolist
default charset utf8;

use todolist;

create table if not exists users
(
   id int primary key auto_increment,
   login varchar(30) not null unique,
   password varchar(50) not null,
   avatar blob
);

create table if not exists tasks
(
   id int primary key auto_increment,
   title varchar(100) not null,
   description text,
   start datetime,
   deadline datetime,
   user_id int,
   constraint tasks_users_fk 
   foreign key (user_id) references users(id)
);

alter table tasks
modify column user_id int not null;

insert into users(login, password)
values ('admin', 'qwerty'),
       ('user', '12345'),
       ('vajsa', 'pupkin');
       
select * from users;

insert into tasks(title, user_id)
values ('Make beckup database', 1),
       ('Make homework', 2);
       
insert into tasks(title, user_id)
values ('Clear temp directories', 1),
       ('Block advertisement', 1);
select * from tasks;

/*insert into tasks(title, user_id)
values ('Kill Bill', 4);*/

delete from users where id=1;
select * from users;

create table if not exists status
(
	id int primary key auto_increment,
    name varchar(10) not null unique
);

insert into status(name)
values ('new'),
	   ('in progress'),
       ('done'),
       ('postponed'),
       ('archived');

alter table tasks
add column status_id int not null default(1),
add constraint tasks_status_fk
foreign key(status_id) references status(id);

create table if not exists tags
(
	id int primary key auto_increment,
    name varchar(50) not null unique
);

create table if not exists tasks_tags
(
    task_id int not null,
    tag_id int not null,
    primary key(task_id, tag_id)
);    

alter table tasks_tags
add constraint tasks_tags_tags_fk
foreign key(tag_id) references tags(id),
add constraint tasks_tags_tasks_fk
foreign key(task_id) references tasks(id);

insert into tags(name)
values('java'), ('html'), ('css'), ('it\'s');

insert into tasks_tags(task_id, tag_id)
values (1, 1), (1, 2), (2, 1);

select * from tasks_tags;

select title, description, start, deadline, name
from tasks, tags, tasks_tags
where tasks.id = tasks_tags.task_id 
     and tags.id = tasks_tags.tag_id;

