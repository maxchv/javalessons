use todolist;
select * from tasks;
select * from users;

select * from tasks_tags where task_id=1;

-- удаление ограничения на внешний ключ
alter table tasks_tags
drop constraint tasks_tags_tasks_cascade_fk;

-- добавление ограниченя с каскадным удалением
alter table tasks_tags
	add constraint tasks_tags_tasks_cascade_fk
	foreign key(task_id) references tasks(id)
	on delete cascade
    on update cascade;

select * from tasks_tags;    
select * from tasks;
delete from tasks where id=1;
update tasks set id=1 where id=2;