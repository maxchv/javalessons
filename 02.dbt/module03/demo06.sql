use todolist;
select * from tasks;
select * from status;
update tasks set status_id = 3 where id=7;

select t.status_id Id, s.name Status, count(*) `Tasks count` 
from tasks t, status s
where t.status_id = s.id
group by t.status_id
having count(*) > 1;

select name from status
union
select name from tags
union all
select name from tags;

-- Cross Join (Декартовое произведение)
select * from tasks, status;
select * from tasks cross join status;
select * from tasks inner join status;
select * from tasks join status;

-- Inner Join
select * from tasks t
join status s
on t.status_id = s.id;
/*
select * from tasks t, status s
where t.status_id = s.id;
*/

-- Left Outer Join
select s.name Status, t.title Task
from status s
left outer join tasks t
on t.status_id = s.id;

use academy;

select s.first_name `First name`, s.last_name `Last name`, g.name `Group`
from students s
left join groups g
on s.group_id = g.id
union
select s.first_name `First name`, s.last_name `Last name`, g.name `Group`
from students s
right join groups g
on s.group_id = g.id;