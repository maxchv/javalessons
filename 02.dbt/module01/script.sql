-- SQL - Structured Query Language
show databases; /* отобразить все базы данных */
use academy; -- выбрать базу данных для которой будем выполнять запросы
show tables; -- отобразить список таблиц в базе данных academy

-- DDL - Data Definition Language
-- Язык объявления данных: создание (изменение, удаление) 
--                         бд, таблиц, представлений, процедур, функций
-- Команды: CREATE, ALTER, DROP
-- Создание базы данных
create database if not exists carshop default charset utf8;
-- выбираем бд carshop
use carshop;
-- Создание таблиц 
create table if not exists cars
(
	model varchar(50),
    price decimal(15, 3),
    description text
);

-- изменим таблицу - добавим поле id
alter table cars add column id int;

alter table cars
add constraint primary key(id);

-- удалим таблицу/базу данных
/* 
drop table cars;
drop database carshop;
*/

-- DML - Data Maniplate Language
-- Изменение данных (CRUD)
-- * CREATE - добавление
-- * READ - считывание данных
-- * UPDATE - обновление
-- * DELETE - удаление данных
-- Команды: INSERT, UPDATE, DELETE

insert into cars(id, model, price, description)
values (1, 'BMW X5', 100000.0, 'Клевое авто');

update cars set price=80000, description='Авто со скидкой' where id=1;

insert into cars(id, model, price, description)
values (2, 'Жигули', 1000.0, 'Клевое авто');

delete from cars where id=2;

-- DQL - Data Query Language
-- Получение данных из базы
-- Команда: SELECT
select id, model, price from cars;
