-- -------------------------------
-- Data Definition Language (DDL)
-- -------------------------------
-- CREATE - создание объекта (например, таблицы, процедуры, функции, представления, триггера и т.д.) базы данных
-- ALTER - изменения объекта базы данных
-- DROP - команда удаления объекта базы данных

-- SQL - регистронезависимый язык запросов в базу данных

-- Cоздания базы данных
CREATE DATABASE IF NOT EXISTS academy default charset utf8;

-- Удаление базы данных
-- DROP DATABASE IF EXISTS academy;

USE academy;

-- Создание таблицы базы данных
-- CONSTRAINT - ограничения на данные в колонках
-- 		1. NOT NULL- не пустое
--      2. UNIQUE  - уникальное занчение
--      3. DEFAULT - значение по умолчанию
--      4. PRIMARY KEY - первичный ключ
--      5. FOREIGN KEY - внешний ключ
--      6. INDEX - индексное поле
-- https://dev.mysql.com/doc/refman/8.0/en/create-table.html
CREATE TABLE IF NOT EXISTS students
(
	id         INT         PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(30) NOT NULL,
    last_name  VARCHAR(30) NOT NULL
);

-- https://dev.mysql.com/doc/refman/8.0/en/alter-table.html
ALTER TABLE students
ADD COLUMN email VARCHAR(50) NOT NULL UNIQUE;

ALTER TABLE students
ADD COLUMN group_id INT NOT NULL;

CREATE TABLE IF NOT EXISTS `groups`
(
	id 		INT 		PRIMARY KEY	AUTO_INCREMENT,
    `name`	VARCHAR(50) NOT NULL UNIQUE
);

ALTER TABLE students
ADD CONSTRAINT students_groups_fk
FOREIGN KEY (group_id) REFERENCES `groups`(id);

