-- Функции: агрегатные, не агрегатные
-- sum(), avg(), count(), max(), min()
-- SELECT count(*) FROM online_shop.customers;

-- Создание собственных функций
/*
CREATE FUNCTION `full_name` (first_name varchar(255), last_name  varchar(255))
RETURNS varchar(255)
RETURN concat(first_name, last_name);
*/

/*select full_name(FirstName, LastName) from customers 
	where FirstName is not null and LastName is not null;*/

-- Процедуры
/*
USE `online_shop`;
DROP procedure IF EXISTS `new_procedure`;

DELIMITER $$
USE `online_shop`$$
CREATE PROCEDURE `new_procedure` (in category_name varchar(255), out _id int)
BEGIN
	insert into productcategories(productcategoryname) values(category_name);
    set _id = last_insert_id();
END$$

DELIMITER ;
*/
select * from productcategories;
call new_category('test', @id);
select @id;

set @a = 1;

delimiter $
create function foo(b int)
returns int
begin
declare a int default 0;
set a = 1;
return a + b;
end$
delimiter ;

-- Права доступа
-- DCL: GRANT, REVOKE
-- CREATE USER
-- create user `movie_user`@`localhost` identified by 'qwerty'; 
-- выдача прав 
grant all on `movies`.* to `movie_user`@`localhost`;
grant insert, update, delete on `movies`.`genres` to `movie_user`@`localhost`;
grant select on `movies`.`genres` to `movie_user`@`%`;
-- отнимание прав доступа
revoke delete on `movies`.genres from `movie_user`@`localhost`;


