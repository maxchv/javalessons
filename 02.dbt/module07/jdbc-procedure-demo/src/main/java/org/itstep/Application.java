package org.itstep;

import java.sql.*;
import java.util.Scanner;

public class Application {

    public static final String URL = "jdbc:mysql://localhost/online_shop";
    public static final String USER = "root";
    public static final String PASSWORD = "";

    public static void main(String[] args) {
        String sql = "select * from movies limit ";
        Scanner scanner = new Scanner(System.in);
        System.out.println("How much? ");
        String count = scanner.nextLine();
        System.out.println(sql + count);
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD)){

//            PreparedStatement ps = conn.prepareStatement("select * from movies limit ?");
//            ps.setInt(1, 5);

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select full_name(FirstName, LastName) from customers \n" +
                    "\twhere FirstName is not null and LastName is not null;");
            while(rs.next()) {
                System.out.println(rs.getString(1));
            }

            System.out.println("Call procedure");
            CallableStatement callableStatement = conn.prepareCall("{call new_category(?, ?)}");
            callableStatement.setString(1, "Test category");
            callableStatement.registerOutParameter(2, Types.INTEGER);
            callableStatement.executeQuery();
            System.out.println("Created with id: " + callableStatement.getInt(2));

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
