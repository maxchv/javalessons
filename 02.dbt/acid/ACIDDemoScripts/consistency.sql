-- Consistency - ����������, ����������� ������ ����������� ���������� �, ��� �����, 
-- ����������� ���� ����������, ��������� ��������������� ���� ������. 

TRUNCATE TABLE acid.dbo.account;

INSERT INTO acid.dbo.account(name, amount) 
	VALUES (N'����', 1000),
	       (N'����', 2000);

SELECT 
	name '��������', 
	amount '�����'
FROM acid.dbo.account;

UPDATE acid.dbo.account
SET amount =
	CASE name
		WHEN N'����' THEN amount - 1500
		WHEN N'����' THEN amount + 1500
	END
WHERE name IN (N'����', N'����');

SELECT 
	name '��������', 
	amount '�����'
FROM acid.dbo.account;