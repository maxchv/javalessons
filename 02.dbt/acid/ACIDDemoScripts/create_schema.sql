IF DB_ID('acid') IS NOT NULL
	DROP DATABASE acid;

CREATE DATABASE acid;

IF OBJECT_ID('acid.dbo.account') IS NOT NULL
	DROP TABLE acid.dbo.account;
GO

CREATE TABLE acid.dbo.account
(
	id int primary key identity,
	name nvarchar(255) not null unique,
	amount money not null check(amount > 0),
	comment text
)
GO

INSERT INTO acid.dbo.account(name, amount) 
	VALUES (N'����', 1000),
	       (N'����', 2000);

