-- Isolation � �� ����� ���������� ���������� ������������ ���������� �� ������ ��������� ������� �� � ���������.
-- ���������������� �������

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT 
	name '��������', 
	amount '�����'
FROM acid.dbo.account;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

SELECT 
	name '��������', 
	amount '�����'
FROM acid.dbo.account;