drop database if exists carshop;

create database carshop char set utf8;

use carshop;

create table manufacture
(
	id int primary key auto_increment,
    name varchar(100) not null unique
);

insert into manufacture(name) values('BMW'), ('Mercedes'), ('Volvo');
insert into manufacture values(4, 'Mazda');

-- select * from manufacture order by id;

create table clients 
(
	id int primary key auto_increment,
	first_name varchar(50) not null,
    last_name varchar(50) not null,
    phone varchar(20),
    unique(first_name, last_name)
);

create table cars
(
	id int primary key auto_increment,
    model varchar(100) not null,
    price decimal(15,3) not null,
    manufacture_id int,
    constraint cars_manufacture_fk 
         foreign key (manufacture_id) references manufacture(id)
             on update set null on delete cascade
);

create table orders
(
	id int primary key auto_increment,
	client_id int not null,
    car_id int not null,
    stamp timestamp,
    constraint orders_clients_fk foreign key(client_id) references clients(id),
    constraint orders_car_fk foreign key (car_id) references cars(id)
);

insert into cars(model, price, manufacture_id)
   values('X5', 35000, 1),
         ('3', 7000, 4);
         
-- select * from cars;

insert into cars(model, price, manufacture_id)
   values('Fantom', 3500000, 100);
   
insert into clients(first_name, last_name, phone)
 values('Вася','Пупкин','+380991234567'),
       ('Маша','Ефросинина','+380997654321'),
       ('Билл','Гейтс','+380979874512'),
       ('Дональд','Трамп','+380661234567');
       
select * from clients;
select * from cars;

insert into orders(client_id, car_id)
	values(1, 2), (4, 1), (3, 2);
    
select * from orders;
