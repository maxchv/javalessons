use academy;

-- alter table students
-- drop constraint students_groups_fk;

-- alter table students
-- modify first_name varchar(100) not null;

-- alter table students
-- add column birth_date date;
insert into students(id, first_name, last_name, email, birth_date)
values (1, 'Миша', 'Ростокин', 'rost@mail.com', '2000-01-01');

update students
set email='masha@mail.com'
where id=2;

delete from students
where id=1;

select * from students;