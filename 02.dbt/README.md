# Database Theory

**Course duration**: 16 double-classes

## Course objective

Introduction to the database management system (DBMS). Teach the
student SQL (structured query language); the principles of normalization;
working with stored procedures, triggers, views, and user-defined
functions. Gain theoretical and practical knowledge about DBMS.

Upon completion of the course, the attendee will:

* have a good understanding of SQL;
* be able to create multi-table queries;
* understand the principles of operation of sub-queries and aggregate
functions;
* be able to carry out the database normalization;
* use the stored procedures, triggers, views, and user-defined functions.

Upon completion of this course, the student submits a practical task
and takes a theoretical exam on course materials. For admission to the
examination, all home and practical tasks must be submitted. Practical
task should cover a maximum of material from different sections of the
course.

## Topic Plan

1. [Introduction to the database theory](module01) (2 double-classes)
2. [SELECT, INSERT, UPDATE, and DELETE queries](module02) (2 double-classes)
3. [Multi-table databases](module03) (2 double-classes)
4. [Aggregation functions](module04) (2 double-classes)
5. [Joinings](module05) (2 double-classes)
6. [Views, stored procedures, triggers](moudle06) (4 double-classes)
7. [Exam](module07) (2 double-classes)

### Useful links

* [Введение в SQL](https://compscicenter.ru/courses/data-bases/2017-autumn/classes/3254/)