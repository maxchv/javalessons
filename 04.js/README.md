# Development of Client-Side Scripts Using JavaScript

Course duration: 16 double-classes

## Course objective

To teach the student to develop client-side scripts using JavaScript.

To teach them to pick appropriate mechanisms and structures to solve a particular problem.

__Upon the completion of the course, the student will:__

* master basic structures of JavaScript, such as variables, conditionals, loops, strings, arrays, functions, etc.;
* get acquainted with OOP and its basic terms;
* be able to handle errors;
* be knowledgeable in the concepts of event, event handler;
* create handler functions of various events;
* understand differences between BOM and DOM;
* be able to interact with objects from BOM and DOM;
* be knowledgeable in subtle details of implementing client-side scripts for various browsers;
* master the principles of creating forms and analyzing user data with regular expressions;
* be able to store user data using cookie mechanism;
* understand subtle details of applying HTML5 in relation to JavaScript;
* be able to serialize and parse data using JSON;
* master principles of creating asynchronous requests using Ajax.

Upon the completion of this course, the student submits all practical
tasks of the course. Grades for the subject are issued based on all
submitted tasks.

## Topic Plan

1. [Introduction to JavaScript](module01) (2 double-classes)
2. [Object. Arrays. Array object. Strings. String object. Date object. Math object. Introduction to the object-oriented programming (OOP)](module02) (2 double-classes)
3. [Processing events](module03) (2 double-classes)
4. [Browser Object Model. Document Object Model](module04) (2 double-classes)
5. [Forms](module05) (2 double-classes)
6. [Form validity verification. The use of Cookie](module06) (2 double-classes)
7. [Canvas drawing, support of media](module07) (2 double-classes)
8. [JSON, Ajax](module08) (2 double-classes)

## Usefull links

* [Все что нужно про ES6](https://reactwarriors.com/es6)