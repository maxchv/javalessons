QUnit.test( "Задание 1. Клонирование простого массива", function( assert ) {
	var arr = [1, 2, 4, 0];
	var cpy = array_clone(arr);
	arr[0] = 100;
	arr[1] = 200;
	assert.deepEqual(cpy, [1,2,4,0] , "Должна быть копия массива!" );
});

QUnit.test( "Задание 1. Клонирование вложенного массива", function( assert ) {
	var arr = [1, 2, [4, 0]];
	var cpy = array_clone(arr);
	arr[2][0] = 100;
	arr[2][1] = 200;
	assert.deepEqual(cpy, [1, 2, [4, 0]] , "Вложенный массив тоже должен быть скопирован!" );
});

QUnit.test( "Задание 2. Сумма квадратов чисел", function( assert ) {	
	assert.deepEqual(summ_kv(1,2,3,4), 30 , "Должна быть сумма квадратов!" );
});

QUnit.test( "Задание 3. Перемешивание массива", function( assert ) {	
	assert.notDeepEqual(array_shuffle([1,2,3,4]), [1,2,3,4] , "Массив должен быть перемешан!" );
});

QUnit.test( "Задание 4. Уникальные элементы массива", function( assert ) {	
	assert.deepEqual(unique([7, 9, 0, -2]), [7, 9, 0, -2] , "Должны быть только элементы массива!" );
	assert.deepEqual(unique([7, 7, 0, -2]), [0, -2] , "Должны быть только элементы массива!" );
	assert.deepEqual(unique([7, 9, 9, -2]), [7, -2] , "Должны быть только элементы массива!" );
	assert.deepEqual(unique([1, 1, 1, 1]), [] , "Должны быть только элементы массива!" );
});