window.x = 10;
console.log(x);
console.dir(window.document); // DOM
// BOM
window.addEventListener('load', () => {
    console.log('load window');
    const forward = document.getElementById('forward');
    forward.addEventListener('click', e => {
        e.preventDefault();
        // history.forward();
        // история серфинга
        history.go(1);
    });

    const btn = document.querySelector('button');
    btn.addEventListener('click', () => {
        location = 'http://google.com';
        location.replace("http://itstep.dp.ua");
    });

    function myMap(long, lat) {
        const position = {
            lat: lat,
            lng: long
        };
        const map = new google.maps.Map(
            document.getElementById("googleMap"), {
                center: position,
                zoom: 25
            }
        );
        const marker = new google.maps.Marker({position: position, map: map})
    }
    navigator.geolocation.getCurrentPosition((position) => {
        console.dir(position);
        myMap(position.coords.longitude, position.coords.latitude);
    });

    const b = document.querySelector('[type=button]');
    b.addEventListener('click', (e) => {
       const win = open('http://itstep.dp.ua', 'Page', 'width=800,height=600,resizable=0,scrollbars=0,status=0,location=0');
    });
});

// window.addEventListener('unload', () => {
//    //console.log("unload window");
//     alert('unload');
// });
// Экран - screen
for(let prop in screen) {
    console.log(`${prop}: ${screen[prop]}`);
}
console.log(screen.orientation.type);

// Location - работа с url
for(let prop in location) {
    console.log(`${prop}: ${location[prop]}`);
}

console.log(navigator.userAgent);






