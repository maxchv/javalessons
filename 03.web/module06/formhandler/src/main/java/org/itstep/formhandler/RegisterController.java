package org.itstep.formhandler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegisterController {

    @GetMapping
    public String index() {
        System.out.println("GET");
        return "index";
    }

    @PostMapping
    public String index(HttpServletRequest request) {
        System.out.println("POST");
        return "index";
    }

}
