package org.itstep.formhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormhandlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FormhandlerApplication.class, args);
    }

}
