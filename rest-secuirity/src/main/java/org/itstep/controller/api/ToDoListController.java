package org.itstep.controller.api;

import lombok.RequiredArgsConstructor;
import org.itstep.entity.ToDoItem;
import org.itstep.repository.ToDoItemRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.OPTIONS})
@RestController
@RequestMapping("/api/todolist")
@RequiredArgsConstructor
public class ToDoListController {

    private final ToDoItemRepository toDoItemRepository;

    @GetMapping
    public List<ToDoItem> list() {
        return toDoItemRepository.findAll();
    }

    @PostMapping
    public ToDoItem save(@RequestBody ToDoItem toDoItem) {
        toDoItemRepository.save(toDoItem);
        return  toDoItem;
    }

}
