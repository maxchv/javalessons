package org.itstep.config;

import org.itstep.filter.BeforeFilterDemo;
import org.itstep.filter.JWTTokenGeneratorFilter;
import org.itstep.filter.JWTTokenValidatorFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import java.time.Duration;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${frontend}")
    private String allowedOrigin;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("user").authorities("user");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/**").authenticated()
                .anyRequest().permitAll()
                .and()
                    .httpBasic()
//                .and()
//                    .formLogin()
                .and()
                    .addFilterBefore(new BeforeFilterDemo(), BasicAuthenticationFilter.class)
                    .addFilterBefore(new JWTTokenValidatorFilter(), BasicAuthenticationFilter.class)
                    .addFilterAfter(new JWTTokenGeneratorFilter(), BasicAuthenticationFilter.class)
                    .csrf().disable()
                    .cors().configurationSource(request -> {
                        CorsConfiguration config = new CorsConfiguration();
                        config.addAllowedOrigin(allowedOrigin);
                        config.addAllowedHeader("*");
                        config.addAllowedMethod("*");
                        config.setMaxAge(Duration.ofHours(1));
                        return config;
                    });
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
