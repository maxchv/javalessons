package org.itstep;

import lombok.RequiredArgsConstructor;
import org.itstep.entity.ToDoItem;
import org.itstep.repository.ToDoItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class InitDatabase implements CommandLineRunner {
    private final ToDoItemRepository toDoItemRepository;

    @Override
    public void run(String... args) throws Exception {
        toDoItemRepository.saveAll(List.of(
                new ToDoItem("First task"),
                new ToDoItem("Second task"),
                new ToDoItem("Third task")
        ));
    }
}
